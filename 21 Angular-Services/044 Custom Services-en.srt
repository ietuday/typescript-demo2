1
00:00:16,560 --> 00:00:24,080
Services in Angola will first begin with understanding the role and importance of a service.

2
00:00:24,090 --> 00:00:32,050
How does it complement the component then we are going to build our own custom service and inject the

3
00:00:32,050 --> 00:00:39,450
same into the component followed by that we are going to see how we can make use of the built in his

4
00:00:39,450 --> 00:00:47,890
DTP service with the help of promise an observable pattern and then we will see how we can make use

5
00:00:47,900 --> 00:00:57,340
of that in service to make its GDP GET request to a remote service of the EPA using less people call

6
00:00:57,910 --> 00:01:03,040
and get the response of the same in a service to be used in a company.

7
00:01:03,520 --> 00:01:03,790
Yes.

8
00:01:03,790 --> 00:01:05,990
Let us understand a little more in detail.

9
00:01:06,340 --> 00:01:09,280
So first thing why do we need a service.

10
00:01:09,500 --> 00:01:15,490
So we pretty well understand that the core of Angola is all in component.

11
00:01:15,730 --> 00:01:22,270
It is the component that is going to provide all the functionality required for the all the data required

12
00:01:22,270 --> 00:01:26,630
for the view which is responsible for presentation of the data.

13
00:01:27,400 --> 00:01:35,790
But it is highly recommended according to the solid principle simpers Single Responsibility Principle

14
00:01:36,310 --> 00:01:37,630
in solid principles.

15
00:01:37,740 --> 00:01:42,280
One of the principles is Single Responsibility Principle.

16
00:01:42,360 --> 00:01:49,020
It is recommended that one single class should not be performing multiple functionalities it should

17
00:01:49,020 --> 00:01:57,900
be constrained or restricted to perform only one functionality keeping this design pattern in mind.

18
00:01:58,350 --> 00:02:07,750
It is recommended to keep the components clean and make them focused only upon supporting the view any

19
00:02:07,840 --> 00:02:10,380
additional business logic required.

20
00:02:10,500 --> 00:02:16,560
It is recommended to move it outside the company and that is what precisely we are going to do in our

21
00:02:16,560 --> 00:02:21,120
service now by moving the code from component into service.

22
00:02:21,120 --> 00:02:25,050
It also becomes pretty easy to test the component how.

23
00:02:25,320 --> 00:02:33,240
Yes we can have the actual service performing the main business logic and we can also have a mock service

24
00:02:33,270 --> 00:02:40,700
if required component can be tested by injecting either a mock service or the actual service in real

25
00:02:40,700 --> 00:02:41,380
time.

26
00:02:41,800 --> 00:02:49,290
In this project it will be more that is injected and in real execution the main service component can

27
00:02:49,290 --> 00:02:52,230
be injected into service can be injected.

28
00:02:52,740 --> 00:03:02,140
So it is very very important that in every project we meet the components even then and or restrict

29
00:03:02,140 --> 00:03:11,850
them to only the functionality and speed which is required for supporting the view or computations.

30
00:03:11,850 --> 00:03:17,890
For example I want to make a trip to the server and from server I would like to add some data.

31
00:03:18,240 --> 00:03:20,940
That means I want to make it less full call.

32
00:03:20,940 --> 00:03:23,560
I will not put up for writing that in a component.

33
00:03:23,580 --> 00:03:29,850
I would prefer moving that into a service service should make it a simple call to the US or what fits

34
00:03:29,850 --> 00:03:32,770
the data and present it to the component.

35
00:03:33,060 --> 00:03:39,900
Likewise if there is any kind of complex business logic computation I prefer moving that also into a

36
00:03:39,900 --> 00:03:41,460
service.

37
00:03:41,460 --> 00:03:49,380
So yes we can say that service is going to play the role of a kind of business object and component

38
00:03:49,380 --> 00:03:51,880
will be responsible for using the service.

39
00:03:52,320 --> 00:03:56,260
Making the data then available to the view.

40
00:03:56,610 --> 00:04:04,180
So let us first begin with how we can actually implement a custom service so have already created a

41
00:04:04,180 --> 00:04:05,340
sample project.

42
00:04:05,410 --> 00:04:13,480
Basically copied the basic project with the seed folder copied into it and here I would like to add

43
00:04:13,570 --> 00:04:20,500
all the required constantly for demonstrating how we can make use of competition with the help of a

44
00:04:20,500 --> 00:04:27,370
service provide some functionality to it and very importantly how can a service be injected into the

45
00:04:27,370 --> 00:04:28,720
component.

46
00:04:28,900 --> 00:04:37,050
So first I would like to have my model plus alcoholic does employee simple and right don't beat us a

47
00:04:37,100 --> 00:04:37,780
lot.

48
00:04:37,820 --> 00:04:44,330
We want an employee not be as I would like to export a simple clause called an employee and we know

49
00:04:44,330 --> 00:04:54,420
that employee will have some of the employees will have some Neame employees have some salary number

50
00:04:55,520 --> 00:04:58,150
and maybe we can also have some day of.

51
00:04:58,820 --> 00:05:01,020
So we have basically covered different data types.

52
00:05:01,090 --> 00:05:04,480
Strings numbers date and maybe require bullion.

53
00:05:04,520 --> 00:05:10,010
You cannot Kaspar the requirement then I like the constructor which can help in initializing all this

54
00:05:10,100 --> 00:05:12,000
ID number.

55
00:05:13,550 --> 00:05:23,660
Name string salary which is again in aboe and they don't join the Jews date.

56
00:05:24,630 --> 00:05:25,740
And here we like this.

57
00:05:25,780 --> 00:05:33,590
Why is it called Whateley does not name the name.

58
00:05:34,170 --> 00:05:35,150
Not Doxology.

59
00:05:35,180 --> 00:05:36,150
The will do.

60
00:05:36,180 --> 00:05:39,070
Salary does not.

61
00:05:39,090 --> 00:05:42,020
They both join in equal to that of time.

62
00:05:42,590 --> 00:05:46,590
So basic employee clause is really now on top of this employee class.

63
00:05:46,620 --> 00:05:52,850
I would like to employ up employee service but what is this employees are we supposed to do with what

64
00:05:52,860 --> 00:05:58,790
functionality they work for performing operations on employee like insert update the lead.

65
00:05:58,830 --> 00:06:01,400
Or maybe we are told it is still operations we want.

66
00:06:01,800 --> 00:06:11,110
So I get a new script class I call it does employee service dog.

67
00:06:11,210 --> 00:06:19,330
He is the last name again which I have to export flosser employee service

68
00:06:22,130 --> 00:06:23,980
not an employee service.

69
00:06:23,990 --> 00:06:31,380
I want certain things to be important first of course I have to import is the employee plus which I'm

70
00:06:31,430 --> 00:06:33,080
going to manage here.

71
00:06:33,300 --> 00:06:38,360
So from employee and also I would like to import.

72
00:06:38,360 --> 00:06:42,620
Here is the decorator indexable

73
00:06:49,090 --> 00:06:51,290
then angle up or

74
00:06:55,090 --> 00:06:58,060
so here and make employee service as indictable.

75
00:06:58,060 --> 00:07:05,650
Plus it's a set of service we are going to inject this into component of what it claims to be injectable

76
00:07:05,770 --> 00:07:11,370
injectable into component must be declared with the greater injectable.

77
00:07:11,380 --> 00:07:14,580
So I have employee service in employee service.

78
00:07:14,620 --> 00:07:21,010
I'm going to provide a function call to get employees and let us say this is supposed to be done at

79
00:07:21,250 --> 00:07:26,790
a rate of employers like that might provide additional methodes also.

80
00:07:27,210 --> 00:07:30,160
But let us see some implementation here.

81
00:07:30,280 --> 00:07:31,980
For what purpose.

82
00:07:32,080 --> 00:07:36,810
I will put some static data.

83
00:07:37,300 --> 00:07:39,190
I would have an added into it.

84
00:07:39,250 --> 00:07:44,980
I will put all the objects statical on that employee object.

85
00:07:44,980 --> 00:07:46,090
I would like to read them

86
00:07:49,300 --> 00:07:55,530
for once a year some new employee in the constructor can provide some data.

87
00:07:56,020 --> 00:08:09,170
Let's say one employee Jambi one we'll call it some name and then salary living doesn't come on.

88
00:08:09,220 --> 00:08:13,610
Did all join some new date.

89
00:08:13,860 --> 00:08:15,200
In 2000.

90
00:08:16,310 --> 00:08:17,520
17.

91
00:08:17,560 --> 00:08:23,160
Someone do basically means marks and then some date.

92
00:08:23,550 --> 00:08:24,920
Let's say.

93
00:08:25,970 --> 00:08:33,200
Like that I would like to have multiple employees.

94
00:08:33,430 --> 00:08:37,100
It's called Mario Cuomo.

95
00:08:39,190 --> 00:08:39,700
Call the

96
00:08:51,730 --> 00:08:54,230
candy and everything gets indented.

97
00:09:11,940 --> 00:09:13,320
This time pro-whaling

98
00:09:22,000 --> 00:09:23,760
something.

99
00:09:24,530 --> 00:09:34,200
So does is going to be written through a service mentor get employees and service employee.

100
00:09:34,250 --> 00:09:40,580
I mean in place of this is what I would like to know make use of a company.

101
00:09:41,110 --> 00:09:45,030
So I would add no to this.

102
00:09:45,420 --> 00:09:46,660
Fine by.

103
00:09:49,410 --> 00:09:57,150
Employee Cumberland is now what we have to do here.

104
00:09:57,790 --> 00:09:58,240
Yes.

105
00:09:58,240 --> 00:10:00,980
First thing we have to lay the components on top.

106
00:10:01,000 --> 00:10:15,160
They should provide component and this is from the aid angle or slice called then we have board

107
00:10:19,350 --> 00:10:26,110
we don't import employ which is an employee we all like to import

108
00:10:31,100 --> 00:10:32,150
employee.

109
00:10:32,150 --> 00:10:42,970
So that is an Loizeaux and we think that the decorator component

110
00:10:46,000 --> 00:10:50,280
of very important thing we have deployed here the Selecta

111
00:10:58,560 --> 00:11:02,270
employees.

112
00:11:02,770 --> 00:11:15,630
We need to give them a little template which is let us say in employed or Steimle fine and then we will

113
00:11:15,640 --> 00:11:24,540
have very important while those are what is the significance of this providers.

114
00:11:24,600 --> 00:11:32,330
This is where I would like to provide the employee service glass which I'm going to inject at inject

115
00:11:32,330 --> 00:11:38,690
into this component and then I'll have your exposed gloss

116
00:11:41,540 --> 00:11:46,410
Oh I'll collect as employe come on and

117
00:11:51,990 --> 00:11:58,090
OK call my is missing is on the right employee component class and what should we do.

118
00:11:58,110 --> 00:12:00,420
And then pay component class.

119
00:12:00,420 --> 00:12:01,850
Pretty simple.

120
00:12:01,860 --> 00:12:09,450
I would like to get a list of all employees and that data to the employees not extremal fine.

121
00:12:09,960 --> 00:12:16,070
So here I'll say I want employees which is nothing but employee.

122
00:12:17,070 --> 00:12:18,460
How are we going to get that.

123
00:12:18,600 --> 00:12:22,820
As I said earlier we are going to index indexed

124
00:12:26,840 --> 00:12:32,590
employees sort of this employee this

125
00:12:36,170 --> 00:12:38,820
into the component.

126
00:12:39,980 --> 00:12:44,480
And if you want them closer is itself to become a member of the employee component class so that it

127
00:12:44,480 --> 00:12:45,870
can be used in other places.

128
00:12:45,890 --> 00:12:48,640
Also you can live here.

129
00:12:49,140 --> 00:12:56,450
So employer service will be injected into the employee component because employee services indictable.

130
00:12:56,780 --> 00:13:02,230
And we have employees or is it a wider.

131
00:13:02,370 --> 00:13:12,180
So here I say this DOD employees is equal to simply call them employees out of this door to get employees

132
00:13:13,900 --> 00:13:21,150
I think better because it does get employees were here also at Anthony because he was running an Eddie

133
00:13:23,900 --> 00:13:35,080
and little so long can we was just employees in training of an employee's daughter team and yes that's

134
00:13:35,080 --> 00:13:36,280
what we will do.

135
00:13:36,640 --> 00:13:43,990
So I to a new team or find employees not headed to Yemen.

136
00:13:44,380 --> 00:13:45,070
What should we do.

137
00:13:45,090 --> 00:13:47,970
And it is not a AGM and so forth.

138
00:13:48,030 --> 00:13:49,820
Obviously I will vote for

139
00:13:52,440 --> 00:14:01,640
and table will have the be or will have the first will give the Haitink's ID

140
00:14:07,750 --> 00:14:08,600
name

141
00:14:12,620 --> 00:14:13,520
salary

142
00:14:17,400 --> 00:14:18,360
date Bode

143
00:14:21,910 --> 00:14:26,540
followed by this he or we would like to pull that off.

144
00:14:26,580 --> 00:14:31,660
But this 6N has to be repeated repeated for every employee.

145
00:14:31,800 --> 00:14:46,250
So to repeat yes we know that we use star energy phone is to let them be off employees.

146
00:14:47,310 --> 00:14:59,700
So what will we do is then we have to provide intercalation MP ID staring into a police dog named you

147
00:14:59,990 --> 00:15:06,590
be not silent and this will be you know being taught

148
00:15:09,300 --> 00:15:13,290
did I have to be a big deal all the time.

149
00:15:13,330 --> 00:15:16,000
So this should not be about the child

150
00:15:20,370 --> 00:15:23,490
yeah we probably want to get some style for borders.

151
00:15:28,220 --> 00:15:35,510
Here only we can put some in and for Damon and Deely

152
00:15:41,070 --> 00:15:49,750
next thing we all want to help the Border one big solid Let's get black

153
00:15:56,360 --> 00:15:58,540
and I hope everything is fine of course.

154
00:15:58,550 --> 00:16:06,060
No we have to make changes to more Dhume.

155
00:16:06,160 --> 00:16:14,530
We want to use employee component and employ component comes from

156
00:16:22,300 --> 00:16:24,720
employee component.

157
00:16:24,790 --> 00:16:35,660
And of course what do you have given employees this I have to use the Rube Goldberg component.

158
00:16:35,710 --> 00:16:37,220
I can say employee

159
00:16:40,080 --> 00:16:41,090
Didi's

160
00:16:45,470 --> 00:16:48,140
and we'll get the data

161
00:16:53,610 --> 00:16:54,480
and that's it

162
00:17:00,640 --> 00:17:04,450
served well let's execute control fire.

163
00:17:04,590 --> 00:17:10,250
So very basic application what is very important is we have to understand.

164
00:17:10,490 --> 00:17:17,500
You have never loved something is wrong or not slice should not be done just slice

165
00:17:27,260 --> 00:17:30,630
the slice it also not it doesn't add up

166
00:17:37,810 --> 00:17:42,490
at the Nasdaq controller Feiffer execution.

167
00:17:45,740 --> 00:17:48,050
And of course as usual Something went wrong.

168
00:17:48,200 --> 00:17:49,220
That's de-bug.

169
00:17:49,580 --> 00:17:57,130
But it's too to figure out not followed should be employed.

170
00:17:57,130 --> 00:18:03,350
Daughter in law because employee's daughter ex-chairman is in the same folder.

171
00:18:03,730 --> 00:18:06,460
Yes they can actually give more.

172
00:18:06,840 --> 00:18:07,360
Id

173
00:18:12,500 --> 00:18:14,340
more Buel.

174
00:18:15,100 --> 00:18:17,080
And let's see now come on

175
00:18:25,560 --> 00:18:26,570
you both.

176
00:18:26,700 --> 00:18:32,100
Oh lordy Tom I think it's all kid's problem uppercase lowercase problem.

177
00:18:34,510 --> 00:18:35,370
TMI.

178
00:18:35,650 --> 00:18:43,020
Yup ID name salary and date of joy.

179
00:18:43,040 --> 00:18:46,630
I would like to pro-white Pied de

180
00:19:01,670 --> 00:19:07,150
medium.

181
00:19:08,610 --> 00:19:10,330
All the readers.

182
00:19:11,330 --> 00:19:14,850
So what is important is nothing great actually.

183
00:19:15,920 --> 00:19:24,200
I wrote a sort of is no what I'm saying this is sort of this Wadud April wait here to me this sort of

184
00:19:24,200 --> 00:19:27,410
as just a glass.

185
00:19:27,560 --> 00:19:29,340
That is one of the one learning here.

186
00:19:29,570 --> 00:19:34,690
I just get a decorator for a custom blossomy table.

187
00:19:34,940 --> 00:19:36,710
It's just declasse.

188
00:19:36,710 --> 00:19:38,460
Nothing special about this glass.

189
00:19:38,720 --> 00:19:43,010
Of course you can provide any kind of functionality in that glass as part of that business requirement

190
00:19:43,660 --> 00:19:52,320
but important is we have provided a decorator called injectable And second thing we have done is here

191
00:19:52,450 --> 00:19:56,020
we have injected the employees service.

192
00:19:56,050 --> 00:20:01,370
We have mentioned it does it all wind up and we have injected the same in the constructor.

193
00:20:03,500 --> 00:20:09,100
And that's all by any chance if the glass has not mentioned as injectable.

194
00:20:09,430 --> 00:20:12,670
Yes here we can use the decorator index.

195
00:20:12,880 --> 00:20:20,170
We have learned this in dependency in the dependence injection chapter our external object can be injected

196
00:20:20,190 --> 00:20:24,960
into all content contained or dependent object.

197
00:20:24,970 --> 00:20:28,000
Of course this example can be for that extended.

198
00:20:28,090 --> 00:20:35,770
You can add more methods you can add in so to employ update employer delete employee get employee by

199
00:20:35,770 --> 00:20:39,990
id and so on and keep providing the required functionality.

200
00:20:41,070 --> 00:20:43,320
But that is not the main target here.

201
00:20:43,350 --> 00:20:49,600
Something like that we will see in later example that we are going to perform operations.

202
00:20:49,800 --> 00:20:58,920
But right now I just wanted to focus upon how simple it is to remove the code from component and Pucelle

203
00:20:58,980 --> 00:21:03,670
in service thats all it does.

204
00:21:03,840 --> 00:21:04,380
Thank you.

