1
00:00:16,730 --> 00:00:22,900
In the previous example I have demonstrated how we can make use of a service in a component.

2
00:00:23,160 --> 00:00:29,640
Basically we injected a service into a company and there are many situations where we would like to

3
00:00:29,700 --> 00:00:32,770
inject a service into a service.

4
00:00:32,790 --> 00:00:35,560
So in this example that is what I want to demonstrate.

5
00:00:35,580 --> 00:00:39,200
We want to use a service inside another set of.

6
00:00:39,380 --> 00:00:45,650
So for this I go back to the same product which we created in the previous session and here I would

7
00:00:45,650 --> 00:00:54,140
like to our longer service a service which is pretty common and I want to do some kind of logging through

8
00:00:54,140 --> 00:00:54,720
it.

9
00:00:55,160 --> 00:01:00,790
So it doesn't longer God sort as DOD see us.

10
00:01:01,370 --> 00:01:03,320
So I have another ServiceNow.

11
00:01:03,970 --> 00:01:04,330
Yes.

12
00:01:04,340 --> 00:01:06,960
First let's get this out of this.

13
00:01:07,430 --> 00:01:17,210
So I make this service also as indexable from Adelaide angle or Sly's called

14
00:01:19,720 --> 00:01:30,400
Gloss Lauber And of course this is injectable.

15
00:01:30,650 --> 00:01:32,570
And what should be there in the other service.

16
00:01:32,780 --> 00:01:35,700
I would like to last brings.

17
00:01:35,900 --> 00:01:46,380
So I will harp string and by for let's say this is empty there's nothing to look for.

18
00:01:49,930 --> 00:01:57,480
Nothing here only we will provide one method called as log which is going to get as message ring and

19
00:01:57,480 --> 00:02:00,840
of course that spring is going to be added to

20
00:02:06,380 --> 00:02:14,540
the log logs and and maybe if required for them of us you can say.

21
00:02:14,830 --> 00:02:17,040
So don't load the message

22
00:02:21,170 --> 00:02:26,230
and that can be it is and this also is missing.

23
00:02:27,550 --> 00:02:29,740
And it can be no return if required.

24
00:02:29,860 --> 00:02:38,090
Maybe in log.

25
00:02:38,810 --> 00:02:40,420
So I would like to get to

26
00:03:06,700 --> 00:03:15,180
that said so I would like to control all the love message of suffering and only one message which we

27
00:03:15,180 --> 00:03:19,180
are logging is also being put into the console this doc

28
00:03:31,650 --> 00:03:33,400
smart loving Glenn.

29
00:03:33,790 --> 00:03:34,410
OK.

30
00:03:42,720 --> 00:03:51,550
So this is a service I'll call it a or longer service but it's recommended not compulsory the a longer

31
00:03:51,550 --> 00:03:58,510
sort of service I would like to use in the employee's service which I have written here so wholly implosive

32
00:03:58,590 --> 00:04:00,100
is get the reference.

33
00:04:00,430 --> 00:04:06,890
Yes I'm going to write the constructor and construct up Alsip.

34
00:04:06,910 --> 00:04:14,660
But I had log longer service.

35
00:04:15,170 --> 00:04:18,680
So I don't know whether it was due in part to Section

36
00:04:27,960 --> 00:04:29,310
good enough.

37
00:04:30,090 --> 00:04:36,490
And now this is logwood is now a member because we have used the key word privately.

38
00:04:36,930 --> 00:04:39,080
So can we use this in the method here.

39
00:04:40,260 --> 00:04:52,500
Yes log on and up log some messages or something like felting employees and

40
00:04:55,730 --> 00:05:08,060
as usual a common mistake like no point is which is longer service mention as indictable.

41
00:05:08,160 --> 00:05:16,710
Yes we have done injectable here but Army indicating it is indictable are injecting any it for it to

42
00:05:16,710 --> 00:05:18,790
be injected injected.

43
00:05:18,990 --> 00:05:22,250
We need to market as a wider.

44
00:05:22,500 --> 00:05:28,140
So when the market does provide little something like logger's out of this I would like to use it in

45
00:05:28,140 --> 00:05:36,940
multiple components so we can we want to use it in multiple places within the more boom.

46
00:05:37,050 --> 00:05:41,390
So I will market as a wider at the more level.

47
00:05:41,830 --> 00:05:47,990
So your providers and providers are providing longer service

48
00:05:50,770 --> 00:05:51,940
which our market

49
00:06:01,780 --> 00:06:11,000
that's so advantage of mentioning in the provider section is hot anywhere within that we can inject

50
00:06:11,240 --> 00:06:15,770
the longer small which we are doing in another service.

51
00:06:15,960 --> 00:06:20,210
November in the service we don't have any decoding them.

52
00:06:20,360 --> 00:06:25,180
When I wrote this this class does not have a decorator even though it does injectable.

53
00:06:25,340 --> 00:06:29,670
There's no property called providers for this injectable.

54
00:06:29,800 --> 00:06:37,320
If you are doing a component for component we have this but we don't have for service.

55
00:06:37,690 --> 00:06:44,320
So right now we can't inject employees or was only in this component not anywhere else but because we

56
00:06:44,380 --> 00:06:51,310
mentioned loggers servicing the providers property of indie module.

57
00:06:51,380 --> 00:06:54,360
We can now use it globally anywhere in the project.

58
00:06:54,370 --> 00:07:01,650
Again indirect luggers this no doesn't run the project

59
00:07:04,300 --> 00:07:05,170
controller Fyffe

60
00:07:10,870 --> 00:07:11,960
as well.

61
00:07:14,560 --> 00:07:16,320
15 employees.

62
00:07:16,410 --> 00:07:17,930
Would it might it for us.

63
00:07:18,370 --> 00:07:19,330
I'll be getting that.

64
00:07:19,390 --> 00:07:20,350
Letting employees

65
00:07:23,530 --> 00:07:26,350
we're calling only 1 matter.

66
00:07:26,590 --> 00:07:29,890
We don't have multiple methodes to go and demonstrate.

67
00:07:29,890 --> 00:07:36,970
The other matter that we have done but is also involved there are multiple sodomizes multiple messages

68
00:07:37,020 --> 00:07:41,240
are dead and at once we would like to print all of them.

69
00:07:41,320 --> 00:07:47,340
So the only point which you have to remember is if a service has to use another service and I just look

70
00:07:47,430 --> 00:07:57,850
at the module level in India more decorate that said otherwise you have to make the class indictable

71
00:07:58,300 --> 00:08:03,180
and injected into the other service through a consumer.

72
00:08:03,500 --> 00:08:03,950
Thank you.

