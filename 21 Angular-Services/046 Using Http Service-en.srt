1
00:00:16,790 --> 00:00:18,730
It's to keep the service.

2
00:00:18,810 --> 00:00:26,040
One of the very important and most commonly used angle or service yes it is a built in service that

3
00:00:26,040 --> 00:00:28,730
is part of the library.

4
00:00:28,830 --> 00:00:31,980
So what is the purpose of this particular service.

5
00:00:32,100 --> 00:00:38,940
See any real time project and application is going to highlight certain things on server that is the

6
00:00:39,010 --> 00:00:45,930
server and certain things online that is the browser that when someone application is meant to start

7
00:00:45,940 --> 00:00:52,180
with features and plain features we are pretty well understood that all that we want on plain sight

8
00:00:52,200 --> 00:01:00,810
we are able to achieve using angular but only plain said will not walk many times.

9
00:01:00,860 --> 00:01:08,430
We would like to invoke certain functionality on the server so that we can get some dynamic old and.

10
00:01:08,790 --> 00:01:17,310
For example when I'm building employee components in Angola I would like to invoke the EPA call.

11
00:01:17,490 --> 00:01:23,430
That means I would like to invoke certain set of the same functionality and get employed details from

12
00:01:23,610 --> 00:01:29,350
that server which probably would get it from database.

13
00:01:29,520 --> 00:01:32,830
So we need to have some supporting functionality also.

14
00:01:33,300 --> 00:01:45,310
So it is the service which is going to be used by the client for making it more a call to the EPA.

15
00:01:45,930 --> 00:01:56,120
Yes I repeat it's GTP service is going to be used by our I will call in and we'll up for remotely calling

16
00:01:56,330 --> 00:02:05,630
up that service installed on the server which can be a bit EBIZ in darknet or it can be a DSP or BHB

17
00:02:05,750 --> 00:02:06,040
page.

18
00:02:06,050 --> 00:02:08,620
Also executing on server.

19
00:02:09,200 --> 00:02:15,050
So in this particular example I want to demonstrate how we can make use of its GDP service which is

20
00:02:15,050 --> 00:02:17,340
part of its GDP.

21
00:02:18,260 --> 00:02:24,680
So for this to look and is if we don't get the publication because Ansaru are saying we want some or

22
00:02:24,680 --> 00:02:25,300
not.

23
00:02:25,820 --> 00:02:28,700
So I'll create a new dot net a publication

24
00:02:31,320 --> 00:02:32,830
called does its duty

25
00:02:35,610 --> 00:02:41,690
service them all up your is your target application.

26
00:02:41,790 --> 00:02:45,530
Click on OK.

27
00:02:46,940 --> 00:02:48,840
We want to be a project.

28
00:02:49,240 --> 00:02:50,240
OK.

29
00:02:50,720 --> 00:02:53,880
It's going to create the empty project template into it.

30
00:02:53,930 --> 00:02:59,910
I'll have to copy the seed project the content from this project.

31
00:03:00,050 --> 00:03:05,560
So the main files from the project are Diest linked back into dog days.

32
00:03:06,280 --> 00:03:09,610
Biggest conflict Doherty's and simple.

33
00:03:10,220 --> 00:03:13,690
I'll copy this for things into my web application.

34
00:03:15,510 --> 00:03:18,490
Best to do it now.

35
00:03:18,490 --> 00:03:27,850
Right click on package toadyism and see at least two packages packages D'Argenton until these packages.

36
00:03:27,910 --> 00:03:30,160
This is going to download the latest

37
00:03:32,520 --> 00:03:40,290
package which is required for executing a project which is required for developing and executing a lot

38
00:03:40,300 --> 00:03:42,010
of obligation.

39
00:03:42,330 --> 00:03:51,750
So all this you can see as it is doing it in the old can see where the progress is being displayed.

40
00:03:51,750 --> 00:03:54,580
It is downloading it is going to give certain warnings also.

41
00:03:55,540 --> 00:03:58,660
All that we can ignore it and it's going to take while.

42
00:03:59,010 --> 00:04:06,730
So in the meantime I'll pause the video and then resume the complete download is completed approximately

43
00:04:06,730 --> 00:04:10,300
100 M-B of content to be downloaded into your project.

44
00:04:11,150 --> 00:04:13,530
That is the reason it is going to take some time.

45
00:04:14,540 --> 00:04:17,720
So let's say it's all completed.

46
00:04:17,720 --> 00:04:24,900
No all the all the required finds have been downloaded especially selected has been added.

47
00:04:25,400 --> 00:04:30,810
But before we actually run this we will have to expand the source of all the uncertainty and just fill

48
00:04:30,810 --> 00:04:34,570
up to make specially indexed Nottage to human.

49
00:04:34,630 --> 00:04:37,670
We love to give the relevant part.

50
00:04:37,910 --> 00:04:40,090
Especially for no more use.

51
00:04:40,100 --> 00:04:48,400
This is in the root directory of our project so give that root directory.

52
00:04:48,410 --> 00:04:51,680
We also have the basic box

53
00:04:54,520 --> 00:05:00,190
base which is a sarcy because that is what is good for us.

54
00:05:02,130 --> 00:05:04,990
Also one more point would have to make a team.

55
00:05:08,330 --> 00:05:12,930
There is this conflict got Jason on here we should.

56
00:05:13,100 --> 00:05:17,570
It generally takes TX.

57
00:05:18,060 --> 00:05:25,530
That said let's execute and see those first so I create a project.

58
00:05:25,870 --> 00:05:30,210
Remember the same thing is what I have actually done in my first example also.

59
00:05:30,790 --> 00:05:37,070
And all the times thereafter I've been using the base project copy and making changes.

60
00:05:37,270 --> 00:05:43,830
But now we have a new project which is the darknet MVC.

61
00:05:43,830 --> 00:05:45,400
The EPA project.

62
00:05:45,650 --> 00:05:49,960
So here I would like to add a controller control something like

63
00:05:53,880 --> 00:06:01,180
that maybe a controller.

64
00:06:01,250 --> 00:06:05,960
We don't have a model class but we can create one if needed.

65
00:06:07,420 --> 00:06:10,490
I do that but literacy does come first.

66
00:06:12,660 --> 00:06:21,790
In this sarcy slice next door is Jim.

67
00:06:21,980 --> 00:06:24,470
No it does not become something to feed.

68
00:06:28,370 --> 00:06:34,820
Despondent with $4.40.

69
00:06:35,040 --> 00:06:36,940
One more change.

70
00:06:36,950 --> 00:06:41,980
We love to make systems is here also we have no bottoms.

71
00:06:46,380 --> 00:06:48,090
Yet it should be fine.

72
00:06:58,230 --> 00:06:58,350
And

73
00:07:03,240 --> 00:07:03,840
one more.

74
00:07:03,880 --> 00:07:07,990
Or is that OK.

75
00:07:10,160 --> 00:07:16,310
You will have to put slice or slice in and it's not up to you.

76
00:07:16,590 --> 00:07:27,490
I just put a slice of authority or let off of looks like you need it and the C S S also is in style.

77
00:07:27,720 --> 00:07:36,910
This will start to take that I probably told you that rather I don't see that fight so you can actually

78
00:07:36,910 --> 00:07:37,690
come in.

79
00:07:37,840 --> 00:07:40,580
No it's not that rodent

80
00:07:43,650 --> 00:07:45,450
and not me should get everything right.

81
00:07:46,560 --> 00:07:47,080
I don't know

82
00:07:51,830 --> 00:08:02,580
it should be dead or actually in the same way that statement is better than style thought he here.

83
00:08:02,930 --> 00:08:06,100
So just leave a note on.

84
00:08:06,170 --> 00:08:18,060
We should get tangled up in blue column along with a good So this project isn't any know what your post.

85
00:08:18,080 --> 00:08:25,320
I'm going to add a control the control

86
00:08:28,710 --> 00:08:36,320
employing EPA control of this employee EPA controller.

87
00:08:36,770 --> 00:08:43,700
We would like to provide the functionality for returning a list of employees maybe for that we would

88
00:08:43,710 --> 00:08:48,080
require to access database but for the most part.

89
00:08:48,120 --> 00:08:51,080
I'll just put here a class employee

90
00:08:53,760 --> 00:09:02,510
and of course the public class it has public in need of ID

91
00:09:06,420 --> 00:09:23,560
get and set public string name get it public does the mother than good and said Keep it simple that

92
00:09:23,560 --> 00:09:31,830
said and get in get people to read don't I numerable all employed.

93
00:09:32,440 --> 00:09:36,570
So we have a new list of employees.

94
00:09:38,260 --> 00:09:43,580
And here we simply are new employee with irony.

95
00:09:43,790 --> 00:09:51,550
Let's a call to one name equal to do some ENP one and some salary

96
00:09:57,140 --> 00:09:57,650
like that.

97
00:09:57,650 --> 00:10:00,630
Some employees like to read an oral

98
00:10:13,010 --> 00:10:16,540
two three four five

99
00:10:21,020 --> 00:10:22,010
some salary

100
00:10:26,490 --> 00:10:28,040
some.

101
00:10:29,430 --> 00:10:32,890
So the bottom line is pretty simple.

102
00:10:32,980 --> 00:10:33,430
Go

103
00:10:36,490 --> 00:10:42,310
this all client is going to get at it of employees.

104
00:10:43,140 --> 00:10:50,770
And this array of employees does employee objects are what I would like to display in my view.

105
00:10:51,760 --> 00:11:01,410
So for us able to see and hear under the app we will begin with I think let us say

106
00:11:04,720 --> 00:11:14,660
or types could find employee class or just Employee class is equal to that of the Employee class which

107
00:11:14,660 --> 00:11:16,040
we have.

108
00:11:16,680 --> 00:11:29,600
So same thing but it is in its current we'll say export class employing and simply say the number of

109
00:11:34,850 --> 00:11:38,150
name string

110
00:11:40,630 --> 00:11:41,420
and Saladin's

111
00:11:44,020 --> 00:11:44,510
number

112
00:11:52,180 --> 00:11:55,370
that a simple employee ID name and some the

113
00:11:58,770 --> 00:12:02,230
basically a mapping class we have the same class.

114
00:12:02,240 --> 00:12:03,700
We have a leader on that line.

115
00:12:04,620 --> 00:12:08,560
No I'm going to create employee component

116
00:12:14,540 --> 00:12:15,370
employed or

117
00:12:18,280 --> 00:12:31,190
Cumberland or is what we want in the company and on top we'll see it in bold.

118
00:12:36,690 --> 00:12:39,870
And with our slides code.

119
00:12:41,110 --> 00:12:56,700
And then I would like to import employee from employee and I would like to ride that one in.

120
00:12:56,850 --> 00:13:06,820
The important thing we have to provide is Selecta college does employees I would like to display all

121
00:13:06,820 --> 00:13:19,670
the employees and that template you are doing an alcoholic does employed or did to him because it is

122
00:13:20,210 --> 00:13:26,590
a little but more you will it more doable.

123
00:13:26,730 --> 00:13:32,530
I mean and then when he had explored class

124
00:13:37,010 --> 00:13:39,460
and law he complimented

125
00:13:49,320 --> 00:13:57,920
what we want to do here in this employee company and I'll try to get a reference to a service robotically

126
00:13:58,000 --> 00:14:00,600
making use of service here.

127
00:14:01,090 --> 00:14:04,300
I would call the project employee service

128
00:14:12,440 --> 00:14:17,730
an employee service will be responsible for getting me the details of employees.

129
00:14:17,730 --> 00:14:22,920
So first it is a indictable clause so important.

130
00:14:25,070 --> 00:14:28,950
Indexable from

131
00:14:34,720 --> 00:14:41,090
Sly's called and then I ate here at

132
00:14:43,880 --> 00:14:45,220
and double

133
00:14:47,880 --> 00:14:55,380
X. or Clauss employees sort of its.

134
00:14:55,610 --> 00:15:00,710
Now what does that employee service we want to get a list of all employees to get

135
00:15:03,630 --> 00:15:05,840
employees.

136
00:15:06,050 --> 00:15:07,520
Now what does that does.

137
00:15:07,740 --> 00:15:13,540
Let us say we want an area of employees here but

138
00:15:25,570 --> 00:15:30,160
what if I have to return an array of employees I should write here the functionality which creates an

139
00:15:30,160 --> 00:15:30,830
employee.

140
00:15:31,320 --> 00:15:37,620
Not those things I would like to get from this article and that is where I would like to use this DTP

141
00:15:37,620 --> 00:15:44,740
service so post-up could use its GDP services import heads to be

142
00:15:47,610 --> 00:15:54,550
from Adelaide Angola Sly's TTP

143
00:15:57,280 --> 00:16:05,140
and we want this matter to be done and observable not employ it but it should be synchronously called

144
00:16:05,690 --> 00:16:15,040
whenever in matter should be a synchronously call it and it should be observable of generic what is

145
00:16:15,040 --> 00:16:15,880
observable.

146
00:16:15,880 --> 00:16:22,390
Again a class which I would like to see done that is in our state is so important.

147
00:16:25,080 --> 00:16:32,890
Observable from Audix does last

148
00:16:35,890 --> 00:16:46,940
hoping that so now we have to approach implementation says that this object is written but how do we

149
00:16:46,940 --> 00:16:48,570
get that object to it.

150
00:16:49,070 --> 00:16:52,530
What Hodel get into the heads deep in debt.

151
00:16:52,580 --> 00:16:54,130
In this particular class.

152
00:16:54,440 --> 00:17:04,840
So to inject what we need to construct up and here is a crime and its TTP which is of its GDP.

153
00:17:04,850 --> 00:17:08,360
So can I use that it's too deep you know because they are all private.

154
00:17:08,390 --> 00:17:09,790
This becomes a member.

155
00:17:10,160 --> 00:17:19,430
So now I'll say this Daut it's GTP top get is the matter we would like to invoke and get we would like

156
00:17:19,430 --> 00:17:22,760
to pass the audit of the EPA.

157
00:17:24,330 --> 00:17:36,960
That is in the current project around EPA Sly's employee EPA that it does give this automatically it

158
00:17:36,950 --> 00:17:41,820
is going to invoke get better in employ EPA control because of this.

159
00:17:41,820 --> 00:17:42,970
Get him.

160
00:17:43,170 --> 00:17:45,060
Now what do you get.

161
00:17:45,060 --> 00:17:47,070
What is this going to get done.

162
00:17:47,070 --> 00:17:49,370
This is going to be done some observable.

163
00:17:49,470 --> 00:17:58,700
As you can see that it's done in an observable all response not on observable presumptions.

164
00:17:58,800 --> 00:18:08,960
I wanted dot map I'll get the response object there is map.

165
00:18:08,960 --> 00:18:09,960
We'll see.

166
00:18:09,960 --> 00:18:11,760
At have the librarian.

167
00:18:11,920 --> 00:18:20,790
But when we get the response what we should do this response right now is in springform and that spring

168
00:18:20,820 --> 00:18:23,140
we would like to convert to Islam.

169
00:18:23,350 --> 00:18:26,830
So it was drop an.

170
00:18:27,020 --> 00:18:28,740
So what is math.

171
00:18:28,960 --> 00:18:30,310
We'll do that.

172
00:18:30,310 --> 00:18:34,170
But finally we will say we don't all hear

173
00:18:41,780 --> 00:18:42,430
so here.

174
00:18:42,470 --> 00:18:44,950
All right.

175
00:18:46,090 --> 00:18:47,520
Onix.

176
00:18:47,650 --> 00:18:54,770
Does slides and operations operate.

177
00:18:55,260 --> 00:18:56,340
Does not

178
00:18:59,950 --> 00:19:03,240
and of course response also is part of it.

179
00:19:03,250 --> 00:19:10,320
Did he use that looking to get rid of a bracket.

180
00:19:10,340 --> 00:19:10,620
Yes.

181
00:19:10,650 --> 00:19:12,020
Because is up I don't mean to

182
00:19:15,570 --> 00:19:19,190
and that's that.

183
00:19:19,200 --> 00:19:26,020
So instead of putting hard coded data like we did in the previous example we are not getting the beat

184
00:19:26,020 --> 00:19:30,510
up from sort of a but we're just methodical.

185
00:19:30,510 --> 00:19:38,480
This matter I would like to call in might employ some of this and to what degree is observable.

186
00:19:38,890 --> 00:19:43,660
So let's go to employee service employees service

187
00:19:47,150 --> 00:19:47,740
setting.

188
00:19:47,810 --> 00:19:50,120
I want to call this my third component.

189
00:19:50,330 --> 00:19:52,080
It's already an employee service.

190
00:19:52,220 --> 00:20:01,490
We want to call this an employee component employee component to get injected employee service.

191
00:20:01,520 --> 00:20:03,400
So first right here.

192
00:20:03,550 --> 00:20:06,520
CUOMO throw away tools.

193
00:20:07,900 --> 00:20:14,910
And give your employee service or various employee service it is a clause in

194
00:20:20,570 --> 00:20:27,280
lies employed so that is harder to get that through system.

195
00:20:30,440 --> 00:20:40,750
But I have it you know it's Ambrois sort of this no.

196
00:20:40,840 --> 00:20:48,040
Once we have the object of this plus we don't like to make you of all the employees and call get employees

197
00:20:48,700 --> 00:20:52,750
and get employees is going to treat an employee eddied.

198
00:20:53,020 --> 00:20:56,970
So I'd say employees of employee and

199
00:20:59,600 --> 00:21:06,560
so at least ideally actually it is recommended to have your aunt in and

200
00:21:10,360 --> 00:21:15,250
we will implement here on it.

201
00:21:16,360 --> 00:21:26,640
And TAWS We all right in the on in and what we want to do and deal on it we want to initialize this

202
00:21:26,750 --> 00:21:31,600
dot employees whole like that.

203
00:21:31,600 --> 00:21:34,840
Now let's make a call to employee service

204
00:21:37,410 --> 00:21:43,800
rather distraught employees at his dot employees and what employees will do.

205
00:21:43,900 --> 00:21:45,410
Get me an observable.

206
00:21:45,410 --> 00:22:03,630
All employees that is ENP has that eum me as a Xining does not implies that it so I'm making up gold

207
00:22:03,720 --> 00:22:07,940
to get employees because good employees return type is observable.

208
00:22:08,040 --> 00:22:13,190
This becomes an asynchronous call something is it all

209
00:22:16,570 --> 00:22:19,870
so we have to get employees don't subscribe.

210
00:22:20,020 --> 00:22:22,660
Very important to subscribe.

211
00:22:23,250 --> 00:22:29,430
Good employees is a method which we want to call good employees returns what good employees returns

212
00:22:29,520 --> 00:22:32,840
observable data observable.

213
00:22:32,840 --> 00:22:34,750
We are subscribing.

214
00:22:34,830 --> 00:22:35,580
That means what.

215
00:22:35,650 --> 00:22:41,070
When it will get employees right and this becomes an asynchronous call when we will get employees rate

216
00:22:41,080 --> 00:22:47,800
on that time we are going to get employees and employees object and that employees object we are going

217
00:22:47,820 --> 00:22:55,180
to disrupt employees once again that not employees no Kineret employee that it's Tiemann.

218
00:22:55,440 --> 00:22:59,880
So you're only a new item.

219
00:22:59,890 --> 00:23:03,600
It stimulates employed its treatment.

220
00:23:03,770 --> 00:23:06,110
Well I would like to do is play the data

221
00:23:09,680 --> 00:23:16,900
do your duty which will have any names.

222
00:23:17,410 --> 00:23:20,580
But we are getting word of employee.

223
00:23:21,070 --> 00:23:24,430
So what are those first tier headings.

224
00:23:26,810 --> 00:23:29,840
Your second name are going to begin with.

225
00:23:29,840 --> 00:23:33,210
Id name Salan

226
00:23:38,760 --> 00:23:43,550
and say in the head please read the

227
00:23:53,280 --> 00:24:06,840
here or should I do well first want to repeat this for rebuilding will start and people will let them

228
00:24:06,900 --> 00:24:10,500
be off employees.

229
00:24:10,770 --> 00:24:15,630
And yet I'll give you the.

230
00:24:16,080 --> 00:24:16,670
Id

231
00:24:21,080 --> 00:24:22,310
dord name

232
00:24:25,760 --> 00:24:30,210
ENP not suddenly.

233
00:24:30,450 --> 00:24:33,780
So it's Juman violence taken care of in the component

234
00:24:36,710 --> 00:24:43,600
which is getting it from employees employees is getting initialized Hjelle because of the subscriber

235
00:24:44,480 --> 00:24:48,170
and get employees the matter in service or it injected here.

236
00:24:51,330 --> 00:25:02,060
And so that is then using his TTP to make a service call mapping the response so that we get data an

237
00:25:02,080 --> 00:25:03,830
object from the spring.

238
00:25:03,830 --> 00:25:10,190
It should be converted to Islam and very important in model.

239
00:25:10,220 --> 00:25:18,350
We love to include your TTP model so here is a poll.

240
00:25:18,870 --> 00:25:27,010
It's going to be more from the raid angle or slice its GDP.

241
00:25:27,050 --> 00:25:37,350
This is very important and we are using employee component and that is in

242
00:25:40,550 --> 00:25:48,100
employee company and for our daughters last employee combo it

243
00:25:52,370 --> 00:25:53,500
and finally

244
00:25:56,080 --> 00:25:58,580
exiguous the project.

245
00:25:58,990 --> 00:26:00,960
Chances are some that something might go wrong.

246
00:26:01,140 --> 00:26:12,270
Lexi Yes one important thing about employee component has a selector employees that I should do the

247
00:26:12,360 --> 00:26:20,710
room on and so this only does employee details.

248
00:26:22,770 --> 00:26:23,090
And

249
00:26:25,650 --> 00:26:31,640
employees thats it not.

250
00:26:31,710 --> 00:26:32,480
Once again

251
00:26:36,520 --> 00:26:45,010
we should send a request to slash Sourcery slash index Orphic first time we got the perfect job and

252
00:26:45,010 --> 00:26:50,060
we contested what I would strongly recommend to do is put a point everywhere.

253
00:26:50,200 --> 00:26:51,990
I would like to put a break point here.

254
00:26:53,890 --> 00:26:54,260
Dot

255
00:26:57,280 --> 00:26:59,530
I would like to put a break point in component

256
00:27:02,140 --> 00:27:04,080
and run in the book.

257
00:27:07,540 --> 00:27:13,840
You can actually put a breakpoint in the EPA also.

258
00:27:14,100 --> 00:27:16,100
So what is happening overall here.

259
00:27:17,320 --> 00:27:18,080
Cumberland

260
00:27:27,130 --> 00:27:32,420
component is sending a request to service.

261
00:27:32,540 --> 00:27:34,040
How did we get the service.

262
00:27:34,270 --> 00:27:40,660
Its a customer service which is injected into my component and the injection is succeeding here because

263
00:27:40,660 --> 00:27:45,490
it is mentioned in the provider.

264
00:27:45,550 --> 00:27:53,570
So from here and say continue it goes into the service service return type is observable.

265
00:27:53,740 --> 00:27:57,920
That's quite on service we were able to observe.

266
00:27:58,140 --> 00:28:10,300
I mean subscriber so you're not a remote server so in control this matter is going to execute when it

267
00:28:10,300 --> 00:28:10,920
returns.

268
00:28:10,960 --> 00:28:11,340
Right.

269
00:28:11,380 --> 00:28:17,920
This is starting but the same thing can be replaced with appropriate code area code or entity framework

270
00:28:17,980 --> 00:28:23,340
or to get the data from database that all into an array and redundancy.

271
00:28:23,590 --> 00:28:29,500
We can do that probably will do that in a sample project which we are going to do with the end of this

272
00:28:29,500 --> 00:28:30,330
course.

273
00:28:30,420 --> 00:28:37,900
So like this I repeat once again component is calling the functionality of employee components calling

274
00:28:37,900 --> 00:28:45,940
them functionality of employee service employee services using data service that is injected here to

275
00:28:45,940 --> 00:28:54,560
make a call to the remote API the VPN function returns the data which goes back to the service and in

276
00:28:54,560 --> 00:28:56,340
service we are having IMAP.

277
00:28:56,380 --> 00:29:04,920
That means whatever response we get is in string format and that string actually you can press step

278
00:29:05,400 --> 00:29:10,130
or you see that response.

279
00:29:11,990 --> 00:29:12,710
OK.

280
00:29:13,320 --> 00:29:16,620
We have to get into it a little difficult to debug.

281
00:29:17,030 --> 00:29:23,170
And here we get the employee object.

282
00:29:23,420 --> 00:29:29,390
And once we have the employee object the same thing is getting to the client if we want to have proper

283
00:29:29,390 --> 00:29:35,840
debugging and all unable to make this a method rather than single a single statement.

284
00:29:35,940 --> 00:29:37,130
Does it matter.

285
00:29:37,610 --> 00:29:44,110
And then probably you can put a break point same thing in employee services.

286
00:29:44,180 --> 00:29:49,250
This can be a matter log it is simple map

287
00:29:53,280 --> 00:29:54,340
still we can write

288
00:30:03,790 --> 00:30:07,440
no it not broke yet book it's working.

289
00:30:07,740 --> 00:30:09,560
So here doesn't part of it.

290
00:30:09,570 --> 00:30:12,600
But no the book

291
00:30:17,860 --> 00:30:18,250
Exit

292
00:30:34,740 --> 00:30:37,560
lies sauciest plus index

293
00:30:44,280 --> 00:30:45,050
sticking type

294
00:30:50,540 --> 00:30:52,880
of the breakpoint will not be had.

295
00:30:53,360 --> 00:30:59,400
That's why there's something you can actually use control don't go overboard or stuff like that otherwise.

296
00:31:03,320 --> 00:31:06,820
Ideally I want to break that so that I can demonstrate

297
00:31:17,730 --> 00:31:22,980
looks like mostly about something went wrong.

298
00:31:24,850 --> 00:31:29,660
Anyways I do try on your own debugging by putting breakpoints.

299
00:31:29,660 --> 00:31:30,170
It should work.

300
00:31:30,170 --> 00:31:36,200
Something should have gone wrong in my editor but otherwise these are the steps you're supposed to make

301
00:31:36,200 --> 00:31:42,360
use of making a call to a remote API using its DTP service.

302
00:31:42,470 --> 00:31:47,920
In this example I've demonstrated only again but in later example I'm going to demonstrate.

303
00:31:47,990 --> 00:31:55,310
Insert update delete almost all the fun stuff that is we are going to provide the custom component and

304
00:31:55,600 --> 00:31:56,610
the FBI.

305
00:31:56,960 --> 00:31:57,610
Thank you.

