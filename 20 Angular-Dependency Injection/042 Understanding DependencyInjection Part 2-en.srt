1
00:00:16,560 --> 00:00:23,370
Understanding dependency injection in Angola framework as I said earlier in the previous session we

2
00:00:23,370 --> 00:00:30,180
have to get some support from framework so that we do not have to write something like a factory.

3
00:00:30,450 --> 00:00:38,060
We want Trimbach to provide us a factory so to implement in Angola.

4
00:00:38,250 --> 00:00:45,360
There are three main things which we have to consider in dicto provider and dependency the other three

5
00:00:45,360 --> 00:00:51,570
main things which we need to understand in Angola before we actually understand how it is angler's supporting

6
00:00:51,990 --> 00:01:00,720
Dependency Injection indirect object is used to expose it and create instances of dependencies.

7
00:01:00,720 --> 00:01:01,780
Very interesting.

8
00:01:02,480 --> 00:01:11,630
Injector is responsible for exposing objects to the EPA and those is that EPA will take care of creating

9
00:01:11,630 --> 00:01:20,850
the instance of dependencies to provider provider tells the injector how to create instance of the dependency

10
00:01:21,410 --> 00:01:27,540
the injector is responsible for creating what Holzschuh did create should that same object be shared

11
00:01:27,540 --> 00:01:33,690
across multiple other objects or for every body a new object has to be created.

12
00:01:33,760 --> 00:01:36,090
Which type of object has to be created.

13
00:01:36,090 --> 00:01:40,790
What what is the information we need for creating an appropriate object.

14
00:01:40,890 --> 00:01:49,660
All that people give through provider provider takes two can and maps to a factory to create instances.

15
00:01:49,800 --> 00:01:57,390
So what we have seen in the real such as factory that job vacancy is not taken in by the provider provider

16
00:01:57,390 --> 00:02:03,650
is our factory and the fact that we have to register tokens and maps.

17
00:02:03,780 --> 00:02:06,170
And finally we have co-dependancy.

18
00:02:06,290 --> 00:02:14,610
It is the type that an object should be created within the class we can see which we have to register

19
00:02:14,900 --> 00:02:17,180
whose objects have to be created.

20
00:02:17,190 --> 00:02:26,220
So yes very interesting dependency classes are registered with provider and instantiate that are needed

21
00:02:26,400 --> 00:02:27,520
to index.

22
00:02:27,720 --> 00:02:32,270
So these three play together a very important role in Angola.

23
00:02:32,940 --> 00:02:42,120
So using indet we are going to achieve angular dependency indiction by using reflective reflective injector

24
00:02:42,210 --> 00:02:49,050
time so reflective in the fact the glass Makenzie and that is present in the core library of angular

25
00:02:49,980 --> 00:03:00,420
reflective inductor has already matured and that matter is going to dig in to gain and return dependency

26
00:03:00,510 --> 00:03:10,860
or list of dependencies injector does not return the class but the instance using the new keyboard so

27
00:03:11,010 --> 00:03:19,460
it is going to be clear instance for us and what is the input or then what are kids going to create

28
00:03:19,460 --> 00:03:21,030
a dependency.

29
00:03:21,290 --> 00:03:26,690
That means it is going to create the instance of the dependency object and return to us.

30
00:03:26,690 --> 00:03:28,170
How is this achieved.

31
00:03:28,370 --> 00:03:31,110
You see it's a small example again.

32
00:03:31,200 --> 00:03:35,090
And non-executable example only for concept.

33
00:03:35,150 --> 00:03:43,240
We have to first import a reflective injector and that's in liability in reflective Indic.

34
00:03:43,270 --> 00:03:50,170
We have got a method called last resort and create a very important matter resolve and create.

35
00:03:50,170 --> 00:03:59,520
We can see it is like a factory function and that is responsible for creating an index of.

36
00:03:59,590 --> 00:04:06,820
And these are the list of basically we get said this is the provider to the provider we are registering

37
00:04:06,820 --> 00:04:10,690
no student address and subject.

38
00:04:10,690 --> 00:04:18,190
So we are passing to result and create and getting indexed.

39
00:04:18,300 --> 00:04:25,770
The provider is having three classes or three dependencies that is to student address and subject to

40
00:04:25,770 --> 00:04:26,910
what is taught in here.

41
00:04:27,000 --> 00:04:28,240
We can say it is a token.

42
00:04:28,320 --> 00:04:38,400
These three are tokens so these tokens nothing but class names are possible as a way to resolve and

43
00:04:38,400 --> 00:04:43,670
create an instant we are getting indexed.

44
00:04:44,080 --> 00:04:49,070
And later this index can use can be used for getting an object.

45
00:04:49,090 --> 00:04:53,570
So when I say indirect or get of student that token is registered here.

46
00:04:53,830 --> 00:04:59,100
So this indicator will take care of creating a student object and giving it to us.

47
00:04:59,150 --> 00:05:02,470
Likewise if needed I can write that

48
00:05:05,660 --> 00:05:13,490
maybe address is equal to inject or not get and simply pass the address book on.

49
00:05:14,180 --> 00:05:25,660
Likewise we can like what subjects is equal to inject or God get subjects so whatever tokens are registered.

50
00:05:26,090 --> 00:05:30,570
All the instances of this can be created to interact.

51
00:05:30,920 --> 00:05:38,360
And how do we get in that through reflective indirect or not resolve and create matter resolve and create

52
00:05:38,360 --> 00:05:48,090
matter is creating a provider which is responsible for having a list of classes as toucans.

53
00:05:48,110 --> 00:05:54,550
So basically this is the core API What is the core if they reflect the Indicum.

54
00:05:54,620 --> 00:06:04,030
No no the problem is how the injector knows which dependency needs to be created in order for instantiating

55
00:06:04,030 --> 00:06:05,540
the student.

56
00:06:05,700 --> 00:06:12,390
Suppose I'm writing a student class Hoelzer inject that know that it has to create address and subject

57
00:06:14,250 --> 00:06:22,500
to do this we have to import Indic from the angle of framework and apply it as a decorator at the rate

58
00:06:22,570 --> 00:06:25,680
index come to the constructor parameters.

59
00:06:25,690 --> 00:06:27,780
Basically this is the syntax.

60
00:06:27,780 --> 00:06:37,500
Imagine for a while now this is all student plus so installed and plus let's say the student class in

61
00:06:37,510 --> 00:06:38,620
student class.

62
00:06:38,890 --> 00:06:47,810
We try to construct though and will simply mention address and subject what you want it automatically

63
00:06:47,900 --> 00:06:54,920
Address object will be created and subject object will be created and given to us because of this decorator's

64
00:06:56,790 --> 00:07:01,010
so simply imagine some vet student is registered.

65
00:07:01,010 --> 00:07:08,680
Also as you can there if I create an object of student totally great object something like this indirect

66
00:07:08,750 --> 00:07:10,520
or don't get stolen.

67
00:07:10,920 --> 00:07:17,910
So automatically the constructor of student is going to be called in with object of address an object

68
00:07:17,910 --> 00:07:20,060
of subject and given to us.

69
00:07:20,310 --> 00:07:22,930
So we will not leave that excluding object.

70
00:07:22,940 --> 00:07:28,830
We will not directly create an object or subject object we will use indirectly for creating a student

71
00:07:28,830 --> 00:07:29,780
like that.

72
00:07:29,970 --> 00:07:37,550
Internally it will do this and create objects and pass them over here based on what tokens we have register

73
00:07:38,590 --> 00:07:39,760
So data is very important.

74
00:07:39,760 --> 00:07:41,800
The whole thing has to work together.

75
00:07:42,220 --> 00:07:49,550
Some that we haven't mentioned deflective inject up on through reflective Indic we are registering the

76
00:07:49,680 --> 00:07:56,940
organs so that these tokens can be used anywhere for creating an object of appropriate type.

77
00:07:57,060 --> 00:08:04,340
But what is the order in which they have to be created between when the student object is created at

78
00:08:04,350 --> 00:08:07,220
that time we walk adolescent's objects to be created.

79
00:08:07,560 --> 00:08:13,490
For that let's say we have a student class in student Plus we'll have to provide it.

80
00:08:13,510 --> 00:08:18,330
Decorator caller's address on a decorator for a subject.

81
00:08:19,170 --> 00:08:25,500
Towards then the student object is created automatically it is going to create for us to address and

82
00:08:25,500 --> 00:08:28,110
subject Trimbach is going to do that for us.

83
00:08:28,110 --> 00:08:29,700
We don't have to do it explicitly.

84
00:08:29,700 --> 00:08:36,080
That is the whole beauty of the thing or I believe it is very clear to you either.

85
00:08:36,390 --> 00:08:42,530
But anyways when we take a live example it will be much more clear that we are going to do it.

86
00:08:42,960 --> 00:08:50,520
Right now my interest is only on focusing about what is reflective in desktop and how reflective injector

87
00:08:50,610 --> 00:08:58,770
is used for registering the guns and getting those injector and how indirect that can be used for creating

88
00:08:58,930 --> 00:09:06,230
objects and to mentioned the order of creating the objects how we can make use of the Indic decorator.

89
00:09:06,660 --> 00:09:16,450
There also we are supposed to provide the token no dependency causing multiple calls to same index up

90
00:09:17,020 --> 00:09:21,790
for the same token will return the same instance.

91
00:09:21,850 --> 00:09:24,930
That means every time you say injector don't get.

92
00:09:24,930 --> 00:09:27,070
See here we are getting injected.

93
00:09:27,390 --> 00:09:33,980
Now once we get the index up or injector refuses to then and again later some time again if you say

94
00:09:34,020 --> 00:09:40,870
to them remember both will get the same object it will not be different objects same store and object

95
00:09:40,870 --> 00:09:42,480
is going to come and come here.

96
00:09:42,670 --> 00:09:49,600
That's why if you do a comparison of student and student one we are going to get as a result to but

97
00:09:49,720 --> 00:09:51,340
very important.

98
00:09:51,340 --> 00:09:54,030
Suppose we have two different injectors.

99
00:09:54,030 --> 00:10:02,180
Here are two different indicators two different indicators on two different indicators of prices today

100
00:10:02,240 --> 00:10:06,120
and both are going to return different student objects.

101
00:10:06,250 --> 00:10:07,470
So they will not be the same.

102
00:10:07,510 --> 00:10:09,960
Hence when comparing We will get false.

103
00:10:10,390 --> 00:10:14,570
So we can say that every indicator is a separate factor.

104
00:10:15,010 --> 00:10:20,970
This indicator is in fact the index actually the same object we are going to get.

105
00:10:20,980 --> 00:10:28,390
Every time we give to control whereas if there are two factories the factories is going to get us different

106
00:10:28,390 --> 00:10:31,410
objects every time for the same token.

107
00:10:31,410 --> 00:10:37,240
Token is saying but because factories are different we are going to get different optics.

108
00:10:37,240 --> 00:10:39,450
This is very very very important.

109
00:10:39,910 --> 00:10:43,190
So basically it all depends on how you are creating it.

110
00:10:43,240 --> 00:10:45,900
Provider.

111
00:10:46,780 --> 00:10:47,960
Child injector.

112
00:10:48,250 --> 00:10:55,260
Many times we know that in Angola we have got a parent component and we have a child component parent

113
00:10:55,360 --> 00:11:02,380
component will be responsible for creating child component and content from time component is embedded

114
00:11:02,380 --> 00:11:04,020
into the parent component.

115
00:11:04,030 --> 00:11:07,050
This is what we have been seeing from beginning of our goals.

116
00:11:08,090 --> 00:11:13,590
So now suppose injectors can have one or more child Indicus.

117
00:11:13,760 --> 00:11:21,520
Imagine if this is done by the parent component and if child component is also having it's all indirect.

118
00:11:21,990 --> 00:11:31,830
There are two different kinds of in one case we are getting the Indic in the parent and child is also

119
00:11:31,830 --> 00:11:42,950
having its own provider resolve and create child in such cases the object created by the parent an object

120
00:11:42,950 --> 00:11:46,100
created by the child will be different.

121
00:11:46,130 --> 00:11:48,560
Very important.

122
00:11:48,560 --> 00:11:57,230
The parent has got one Reflektor there is one indicator and two parent injected we are having a separate

123
00:11:57,680 --> 00:11:59,350
list of tokens.

124
00:11:59,840 --> 00:12:03,280
So many providers are there with two providers.

125
00:12:03,380 --> 00:12:12,400
One is parent and the other is child so in such case imperent we will have a separate object and in

126
00:12:12,400 --> 00:12:17,860
total we will have a separate object because there are two different indicators having their respective

127
00:12:17,900 --> 00:12:24,010
providers went in jacked up and tied in that instance as our dear friend.

128
00:12:24,130 --> 00:12:29,540
If both are resolving for the same provider we are intended to.

129
00:12:29,550 --> 00:12:37,600
And type indicators are seem if the child is not configured for any provider see this particular case

130
00:12:38,050 --> 00:12:40,120
provider is not given in the child.

131
00:12:40,630 --> 00:12:47,860
So because providers not given in the child did index references are see no and indirect reference or

132
00:12:47,860 --> 00:12:52,060
subtheme obviously this and this is going to get me the same object.

133
00:12:52,690 --> 00:12:56,800
Precisely that is what you have to understand very clearly here.

134
00:12:57,380 --> 00:13:06,030
If inductor is same seem indirect but will get the same object on the Doogan different injectors we

135
00:13:06,030 --> 00:13:09,380
get with different objects of the same token.

136
00:13:09,400 --> 00:13:12,010
Now the relationship is parent and child.

137
00:13:12,270 --> 00:13:17,590
So here we are getting the parent and dicto and two parent in that we are creating a child in the act.

138
00:13:18,020 --> 00:13:20,050
Now is child injectors same as parent.

139
00:13:20,070 --> 00:13:20,540
No.

140
00:13:20,550 --> 00:13:21,170
Why.

141
00:13:21,180 --> 00:13:25,280
Because a provider is given separately here.

142
00:13:25,530 --> 00:13:31,410
But in this particular case parent and child both are safe injectors because pro-white is not explicitly

143
00:13:31,410 --> 00:13:34,320
configured.

144
00:13:34,350 --> 00:13:36,220
There is no provider provided here.

145
00:13:36,430 --> 00:13:42,370
So obviously the indicators seem to have been indirect or something not created by bit and is as it

146
00:13:42,370 --> 00:13:49,990
is what we are going to get to also are trying to object and parent object we can say will be seen this

147
00:13:49,990 --> 00:13:57,190
concept really becomes useful when we have got indexed for child component as well as index the parent

148
00:13:57,190 --> 00:14:02,990
company parent company object dependency object and type Conconi dependency object.

149
00:14:03,130 --> 00:14:09,420
If you want to be same don't give in to the provider if you want them to be different in time.

150
00:14:09,430 --> 00:14:13,410
Also you are supposed to provided provider always the provider.

151
00:14:13,420 --> 00:14:18,990
Given that is what we have to understand with a live example when we date later.

152
00:14:19,600 --> 00:14:29,140
So let us know understand more about pro-white provider is an object which describes a can and configuration

153
00:14:29,170 --> 00:14:32,680
of hope to create the associated dependency.

154
00:14:32,710 --> 00:14:34,280
Very interesting.

155
00:14:34,420 --> 00:14:40,080
What is creating the dependency indicator but where is the information about how it is created.

156
00:14:40,120 --> 00:14:48,280
Provider so are reflective indicators not resolved in square brackets the writing actually like this.

157
00:14:48,430 --> 00:14:57,780
That is nothing but in a way that actually so this syntax is a socket of the syntax

158
00:15:01,990 --> 00:15:10,060
that Indic does index is actually a short hunt syntax off so will provide and use class because it is

159
00:15:10,060 --> 00:15:10,720
student.

160
00:15:10,750 --> 00:15:14,880
Same thing is used for pro-white anti-anything is useful use plus also.

161
00:15:15,190 --> 00:15:15,680
Likewise.

162
00:15:15,680 --> 00:15:19,880
Address provide use class provide used.

163
00:15:20,070 --> 00:15:27,020
Basically means what if the token requester is to then create an object of student.

164
00:15:27,250 --> 00:15:30,560
If the token use this address create an object of interest.

165
00:15:30,790 --> 00:15:35,560
If the target use this object create an object of subject object later.

166
00:15:35,560 --> 00:15:37,720
I already explained to Indicum.

167
00:15:38,080 --> 00:15:44,740
This is Talk on an object it also seem to have the token and class name or theme.

168
00:15:44,740 --> 00:15:50,060
This syntax works perfectly but what is the token and class names are different.

169
00:15:50,080 --> 00:15:53,440
We have got various options here so there are two properties here.

170
00:15:53,440 --> 00:15:57,170
One is a white and the other is useless.

171
00:15:57,540 --> 00:16:08,420
Provided this is the token nado can can be either the train or a type or instance.

172
00:16:08,470 --> 00:16:10,510
Anything can be given right.

173
00:16:10,510 --> 00:16:11,830
No it is a class that is.

174
00:16:11,830 --> 00:16:17,390
It is a type but instead we can give that the class also.

175
00:16:17,680 --> 00:16:24,360
I mean the string name also can be given and that instance can also be given.

176
00:16:24,400 --> 00:16:27,110
Of course we all understand a little later more in detail.

177
00:16:27,130 --> 00:16:31,980
But right now you'll see the reflector does not resolve.

178
00:16:32,260 --> 00:16:37,300
Now we are giving interrupt one token just the class name.

179
00:16:37,420 --> 00:16:47,700
We are seeing a white student and senior student so later when we write something like inductor don't

180
00:16:47,700 --> 00:16:49,660
get answers to.

181
00:16:49,670 --> 00:16:53,100
And what are we going to get.

182
00:16:53,160 --> 00:16:59,040
We are going to actually get object of seniors to read.

183
00:16:59,070 --> 00:17:00,570
Suppose we write like this.

184
00:17:03,070 --> 00:17:08,590
What we will not get is object of student but instead we will get objects of senior students and obviously

185
00:17:08,590 --> 00:17:12,950
that will be valid only if a student is inherited from a student.

186
00:17:13,000 --> 00:17:19,660
So in case you want to have different Duken and class name then this syntax is useful.

187
00:17:19,660 --> 00:17:25,810
Otherwise the socket syntax meant we just mentioned the class name is sufficient because token and class

188
00:17:25,810 --> 00:17:33,210
will be the same in such case use class is what the dependency type that we want to actually create

189
00:17:33,210 --> 00:17:41,420
the instance for so very interesting how we are creating a provider.

190
00:17:41,430 --> 00:17:42,710
This is the shortcut.

191
00:17:42,960 --> 00:17:49,860
But in case we want the token and the type to be different whose class instance is created then we will

192
00:17:49,860 --> 00:17:52,450
have the compulsory use this syntax.

193
00:17:52,590 --> 00:17:55,190
So every provider has two properties.

194
00:17:55,320 --> 00:17:57,880
One is used class and provide.

195
00:17:57,910 --> 00:18:02,180
Now let us understand what are the different ways of giving Dogon.

196
00:18:02,270 --> 00:18:10,770
Are they already set and can be given either as string type or instance so when you have spring you'll

197
00:18:10,770 --> 00:18:12,610
simply give string here.

198
00:18:13,900 --> 00:18:18,550
STUDENT I like this.

199
00:18:18,570 --> 00:18:23,410
So what we're going to do now no no at the time of creating the instance.

200
00:18:23,410 --> 00:18:25,170
Also we will say

201
00:18:29,160 --> 00:18:31,990
off student in.

202
00:18:32,210 --> 00:18:34,670
That's it.

203
00:18:34,790 --> 00:18:37,550
Type means we have all of these things we just give the

204
00:18:41,620 --> 00:18:50,820
syntax.

205
00:18:51,510 --> 00:18:57,300
So if your provider is playing you have to mend some strings to create the object of student class.

206
00:18:57,740 --> 00:19:04,290
If the provider is a type you are to give the time for creating an object of student class index and

207
00:19:04,290 --> 00:19:04,940
dog.

208
00:19:05,310 --> 00:19:13,230
And we also have the third option where we create an object of injection talk explicitly where we have

209
00:19:13,230 --> 00:19:18,040
to specify what we are going to give as the token.

210
00:19:18,060 --> 00:19:21,960
So here we are seeing token is mentioned that's just plain.

211
00:19:22,100 --> 00:19:26,180
So when we mention talking a string that C is what we can put away.

212
00:19:26,750 --> 00:19:31,000
So when I create an object I can say student

213
00:19:37,790 --> 00:19:43,290
is equal to I can say them don't get off.

214
00:19:43,310 --> 00:19:47,280
See that's it.

215
00:19:47,280 --> 00:19:49,930
So this is going to get me a student of no.

216
00:19:50,390 --> 00:19:58,360
So based on how did you actually register you were to can it is registered as a class name or is we

217
00:19:58,520 --> 00:20:00,520
heard as class type.

218
00:20:00,630 --> 00:20:07,290
Or is it registered as indicator to an object the index that is supposed to have that parameter passed

219
00:20:07,560 --> 00:20:10,720
to the get method for getting an object of that type.

220
00:20:10,890 --> 00:20:11,840
The dependency on

221
00:20:14,780 --> 00:20:19,140
Likewise we have the facility to even mention dependencies.

222
00:20:19,140 --> 00:20:26,190
So in all the three cases right now I've used use class only but not necessarily we should use use class.

223
00:20:26,560 --> 00:20:28,980
We have our other other alternatives.

224
00:20:29,100 --> 00:20:30,050
Use class.

225
00:20:30,190 --> 00:20:32,440
Use existing use value.

226
00:20:32,440 --> 00:20:33,570
Use factoring.

227
00:20:33,580 --> 00:20:37,270
We have four options for that dependency.

228
00:20:37,510 --> 00:20:44,050
So we are already understood use class basically means that we want to make use of the class so here

229
00:20:44,070 --> 00:20:52,420
that we already know that ISim when we just mentioned the can without explicitly creating a provider

230
00:20:52,420 --> 00:20:57,420
object it indirectly creates a white student and use class student.

231
00:20:57,440 --> 00:21:03,970
This we understand what is use existing use existing is for multiple tokens.

232
00:21:03,970 --> 00:21:06,940
We want to create objects of same type.

233
00:21:07,020 --> 00:21:13,310
So here you see it is not a good example but what I'm trying to demonstrate is here.

234
00:21:13,680 --> 00:21:18,480
If I write something like that one object one

235
00:21:21,800 --> 00:21:26,630
is equal to injector.

236
00:21:26,710 --> 00:21:27,630
I think it's all relative.

237
00:21:27,640 --> 00:21:29,310
Yeah it's all real.

238
00:21:29,800 --> 00:21:37,080
If I tried to read it in in-depth or get Tournon on same index that if I say get address get to all

239
00:21:37,170 --> 00:21:42,360
the three are actually going to get missed wouldn't object on the do it's not a good example.

240
00:21:42,790 --> 00:21:48,460
But just to understand what they are we have given as type you have to use existing.

241
00:21:48,460 --> 00:21:54,510
So already some existing object of students is created the same object is what we are going to get.

242
00:21:54,520 --> 00:22:02,920
It is not going to use a different time that isn't interesting you in this example all that you can

243
00:22:02,910 --> 00:22:09,930
read on at the same instant of time to learn because all the tokens are mapping to the existing dependency.

244
00:22:10,000 --> 00:22:17,800
But remember some students would already be mentioned in other words this will not look and then we

245
00:22:17,800 --> 00:22:19,430
have news value.

246
00:22:19,810 --> 00:22:31,180
So if I give something like we are going to get this one if I say in or dot get it be a key we will

247
00:22:31,180 --> 00:22:33,280
get API x y z.

248
00:22:33,430 --> 00:22:37,240
The value is going to get directly as a constant.

249
00:22:37,240 --> 00:22:42,180
We can either get a single value or we can get up a list of values.

250
00:22:42,580 --> 00:22:44,510
It's our choice what we want to put here.

251
00:22:46,570 --> 00:22:53,110
When the user wants to configure the application with some constant then we toss this can to either

252
00:22:53,110 --> 00:23:00,090
a single value or list of keys with but single you can talk on this one only.

253
00:23:00,100 --> 00:23:15,120
In either case I'm going to write what say something like value is playing is equal to Indic does not

254
00:23:15,140 --> 00:23:17,270
get an idea.

255
00:23:17,370 --> 00:23:26,210
If the be are going to get the value of that as well we are going to get as much

256
00:23:29,530 --> 00:23:30,300
some variable

257
00:23:34,920 --> 00:23:42,000
so this value is nothing but if the 8 x y z Likewise here we can write the same thing

258
00:23:46,520 --> 00:23:49,810
but here what is the value of that.

259
00:23:49,850 --> 00:23:53,060
It's not going to be spring but it is going to be this object

260
00:23:57,850 --> 00:24:00,530
some object so anything.

261
00:24:01,050 --> 00:24:09,430
And this object will go and so on that object we can further say don't even DOT get VALDER Well not

262
00:24:09,430 --> 00:24:11,200
get to play that we can actually

263
00:24:13,990 --> 00:24:21,480
when we want to make an object to be immutable then we have to use Free's what is immutable not modifiable.

264
00:24:21,550 --> 00:24:25,880
So in such case we are going to get to object by this.

265
00:24:26,020 --> 00:24:29,510
And then after we cannot make changes to the object

266
00:24:32,830 --> 00:24:35,990
object Godfreys is also possible.

267
00:24:36,130 --> 00:24:43,480
And finally we have got use factor we use factor that gives us the advantage where we can decide what

268
00:24:43,480 --> 00:24:45,940
object to be created this or that.

269
00:24:45,970 --> 00:24:47,780
Of course right now this is a constant.

270
00:24:47,890 --> 00:24:50,200
But that itself can be a variable.

271
00:24:50,200 --> 00:24:59,500
So when I said inject don't get ag based on this condition we are going to get this object or this object.

272
00:24:59,560 --> 00:25:01,280
So that is used actually.

273
00:25:01,300 --> 00:25:03,620
So these are the different possible tokens.

274
00:25:04,720 --> 00:25:06,810
I mean dependencies.

275
00:25:06,850 --> 00:25:10,420
So let me review once again everything from beginning.

276
00:25:10,480 --> 00:25:11,770
We all started with

277
00:25:14,970 --> 00:25:22,620
three things are required in Angola inject up an index that is responsible for letting us create the

278
00:25:22,710 --> 00:25:30,950
instance of dependencies is used to expose a piece and create instance of dependencies that is the important

279
00:25:30,950 --> 00:25:42,770
part of it to provider provider is going to create tells us in the details the injector how the instance

280
00:25:42,830 --> 00:25:44,930
of dependencies should be created.

281
00:25:45,350 --> 00:25:55,450
And we have seen that in that we have got Dogon and maps how it Dogon is mapped to the.

282
00:25:55,500 --> 00:26:01,070
And finally we have dependency which is the object which is actually instantiated.

283
00:26:02,070 --> 00:26:04,110
How do we get dependency in Angola.

284
00:26:04,170 --> 00:26:12,900
We have to use a reflective injector plus reflective injector has resolve and created to resolve and

285
00:26:12,900 --> 00:26:16,120
create we are supposed to pause for a wider.

286
00:26:16,560 --> 00:26:24,920
So there are actually are three providers of response to you.

287
00:26:25,670 --> 00:26:33,480
Each provider is supposed to be used as you can and from that token corresponding class object is created.

288
00:26:33,500 --> 00:26:41,890
This is the basic syntax with the help of inject the and interestingly we have learned here how dependency

289
00:26:41,890 --> 00:26:47,190
can be minsan with the help of Indic decoded to indic. the decorator.

290
00:26:47,530 --> 00:26:54,060
So when you're creating student objects using How are we going to create an object like this.

291
00:26:54,300 --> 00:26:57,690
Internally it is going to create objects of address and subject.

292
00:26:57,690 --> 00:27:10,330
Also for us because all these are provided to provide the then we have understood how multiple indexers

293
00:27:11,660 --> 00:27:14,680
can get us objects of same type injectors.

294
00:27:14,720 --> 00:27:22,670
So the multiple calls on ended up are getting the same object because injected is same but if inductors

295
00:27:22,730 --> 00:27:29,450
are different we will get different objects though the token is seen because the providers are mentioned

296
00:27:29,450 --> 00:27:30,480
separately here.

297
00:27:31,390 --> 00:27:37,800
Both are indicators both are at the same level but Indicus can be injected also.

298
00:27:38,130 --> 00:27:45,100
So we have got a parent go on to that minute and in detail we are resolving and creating a tile in the

299
00:27:46,440 --> 00:27:54,810
if the parent index and tell index providers are different their objects will be different though that

300
00:27:54,830 --> 00:27:56,170
vocalising.

301
00:27:56,570 --> 00:28:04,050
But if the parent indicator has provided and that the child indicator does not have any providers configured

302
00:28:04,710 --> 00:28:09,490
then in such case we are going to get the same object for the child as well.

303
00:28:09,610 --> 00:28:17,460
When this concept is very useful when we are going to have a parent component having a set of provider

304
00:28:17,730 --> 00:28:24,420
and time component also having a provider list in such case different objects will be created for them

305
00:28:24,550 --> 00:28:25,470
to work on.

306
00:28:25,560 --> 00:28:34,260
But if the child does not have a provider different objects sorry if the child has the provider list

307
00:28:34,560 --> 00:28:40,760
different object will be created but if the child does not have the provider list the same object will

308
00:28:40,760 --> 00:28:45,750
be used for parent and child then exploring provider.

309
00:28:45,960 --> 00:28:48,540
We know we have provider it has two properties.

310
00:28:48,560 --> 00:28:56,060
One is white and use cloth is basically for the token and use class is for mentioning that dependency.

311
00:28:56,330 --> 00:28:57,780
We know what we are providing to.

312
00:28:57,810 --> 00:29:04,220
Can we have different options we can provide a token as a spring then when spring is used the object

313
00:29:04,220 --> 00:29:09,540
will be created to provide a token as a type for that type object will be created.

314
00:29:09,810 --> 00:29:17,190
And we can provide it talken itself as an object of index and token index and token is a generic class

315
00:29:17,550 --> 00:29:28,050
based on which we can create for instance and finally we have also seen how we can mention the dependencies

316
00:29:28,590 --> 00:29:31,410
for ways of mentioning that dependencies use.

317
00:29:31,410 --> 00:29:37,730
Plus of course the talken and the use plus can be either theme or different.

318
00:29:37,830 --> 00:29:44,320
So if talken is mentioned with different classes get is going to create object of a different class

319
00:29:44,320 --> 00:29:45,270
for us.

320
00:29:45,280 --> 00:29:54,120
So this is a sort of syntax for this and then we have got use existing use existing is supposed to be

321
00:29:54,120 --> 00:30:00,340
used when we want to use the same object for multiple tokens though the tokens are different.

322
00:30:00,480 --> 00:30:03,320
We still want to have the same object.

323
00:30:03,390 --> 00:30:11,900
And finally we can also have use value that means for a given key we can directly get the van which

324
00:30:11,900 --> 00:30:18,930
can be either a single string or can be something like a list of keys or we can call it a dictionary.

325
00:30:18,950 --> 00:30:23,380
So based on our requirement we can specify what is need.

326
00:30:23,540 --> 00:30:25,910
And finally we have the freezed method.

327
00:30:26,120 --> 00:30:31,790
We don't use it so frequently but these are generally used when we want to get immutable object.

328
00:30:31,880 --> 00:30:39,890
That means we cannot modify the properties of it and we can also make use of use factory use factory

329
00:30:40,040 --> 00:30:47,800
based on some condition can be done either of the objects for a given key or token.

330
00:30:47,810 --> 00:30:49,180
So I believe this is going.

331
00:30:49,220 --> 00:30:59,680
This has given you completely clear picture of how all injector is instantiated with the help of provided

332
00:30:59,690 --> 00:31:06,060
what is the role of provider in provider what is the role of toker and dependency.

333
00:31:06,290 --> 00:31:11,730
And of course whole injector of token to create instances of dependency.

334
00:31:12,110 --> 00:31:12,960
Thank you.

