1
00:00:17,030 --> 00:00:24,400
Dependency Injection a very important feature knowledge is used in projects especially in object oriented

2
00:00:24,410 --> 00:00:25,900
application development.

3
00:00:26,710 --> 00:00:32,200
So first we will invest our time in understanding what is dependency injection and then we will see

4
00:00:32,200 --> 00:00:38,980
how it is dependency index and implement implemented in and will put Imbolc how are we going to implement

5
00:00:38,980 --> 00:00:43,030
that in various components which we are going to write.

6
00:00:43,150 --> 00:00:50,860
But before that let us understand first what is dependency injection see in any kind of object oriented

7
00:00:50,860 --> 00:00:51,680
world.

8
00:00:52,090 --> 00:00:58,950
It is all objects communicating with each other but to communicate with each other somebody will have

9
00:00:58,950 --> 00:01:00,470
to create the object.

10
00:01:00,930 --> 00:01:06,780
Yes it is one object which is going to create the other object and the other object will further create

11
00:01:06,780 --> 00:01:11,180
more objects like the many objects we keep getting created.

12
00:01:11,250 --> 00:01:17,550
They are delighted and they are destroyed and it's all objects communicating with each other by invoking

13
00:01:17,550 --> 00:01:21,640
the amateurs and setting and getting properties.

14
00:01:21,830 --> 00:01:26,080
So what is this dependency injection dependency injection.

15
00:01:26,100 --> 00:01:28,500
Is a software design pattern.

16
00:01:28,610 --> 00:01:29,490
Very important.

17
00:01:29,510 --> 00:01:40,430
It is a software design pattern in which a class receives its dependencies from an external source rather

18
00:01:40,430 --> 00:01:41,960
than creating them itself.

19
00:01:41,960 --> 00:01:48,050
That means the class or one object is not directly creating the other object.

20
00:01:48,160 --> 00:01:53,830
Somebody else will create that object and give a reference to this object so that they can communicate

21
00:01:53,830 --> 00:01:56,430
with each other say for example.

22
00:01:56,430 --> 00:01:58,500
Now imagine we have an object.

23
00:01:59,060 --> 00:02:06,330
And if object is creating object B because it wants to access some of the functionality in Object B

24
00:02:07,020 --> 00:02:12,660
we can say that object B is independent and object is dependent.

25
00:02:12,660 --> 00:02:20,650
That means it is dependent on B on because it is directly creating an object of B.

26
00:02:20,760 --> 00:02:25,370
We say that A and B are tightly coupled to each other.

27
00:02:25,590 --> 00:02:27,850
And this is not a good design pattern.

28
00:02:27,950 --> 00:02:35,010
Now why it is not good design pattern because it is directly dependent on be any kind of thing just

29
00:02:35,040 --> 00:02:41,260
in B can break it unless the team is reflected in it also.

30
00:02:41,650 --> 00:02:43,710
So we want no loose coupling.

31
00:02:43,740 --> 00:02:51,700
That means even if I make just to be even if I upgrade B I should not break it.

32
00:02:52,050 --> 00:03:00,620
And that is what we can achieve using dependency index and bottom so a dependency index and in Angola

33
00:03:00,680 --> 00:03:06,990
has many great things you will have in Decatur Rwanda and dependency.

34
00:03:07,100 --> 00:03:08,920
We will talk more about this later.

35
00:03:08,930 --> 00:03:10,080
What is there in Angola.

36
00:03:10,160 --> 00:03:11,880
Independence in.

37
00:03:11,930 --> 00:03:14,000
What is indicator what is provided.

38
00:03:14,000 --> 00:03:15,060
What is dependency.

39
00:03:15,080 --> 00:03:17,030
That is not our concern right now.

40
00:03:17,390 --> 00:03:26,480
But you see as an example we have got here a clause called us address we are called a subject and we

41
00:03:26,480 --> 00:03:35,510
have class student and student here is directly creating an object of address and subject subjects rather

42
00:03:36,200 --> 00:03:39,440
into subjects we are pushing some subject that is fine.

43
00:03:39,500 --> 00:03:42,270
Of course this is only a dummy example.

44
00:03:42,290 --> 00:03:44,300
It's not something realistic.

45
00:03:44,310 --> 00:03:53,360
But yes what we really understand here is student is directly dependent on address and subject and that

46
00:03:53,360 --> 00:03:56,220
is the major concern that is the drawback.

47
00:03:56,240 --> 00:03:57,170
So what are the drawbacks.

48
00:03:57,170 --> 00:04:03,810
One is inflexible student is directly dependent on address and subject class and is thus inflexible.

49
00:04:03,980 --> 00:04:06,560
What is the inflexibility by turns.

50
00:04:06,600 --> 00:04:08,990
Imagine no later for some reason.

51
00:04:08,990 --> 00:04:16,880
If I addressed a constructor with paramita that means I'm replacing the default constructor with a constructor.

52
00:04:17,000 --> 00:04:18,400
Paramita.

53
00:04:18,410 --> 00:04:19,480
Will this work then.

54
00:04:19,500 --> 00:04:23,150
No I love to make changes to student also.

55
00:04:23,330 --> 00:04:26,040
That's what we call it that's inflexibility.

56
00:04:26,150 --> 00:04:27,390
That is what is said here.

57
00:04:27,410 --> 00:04:32,010
If a let's get involved and its constructor requires a part of me dope.

58
00:04:32,180 --> 00:04:40,990
Storing is broken because the de-couple student is gonna be creating endless do all the work on this.

59
00:04:41,190 --> 00:04:46,780
Right now we will have to make changes to this tool class and that is not a good design.

60
00:04:46,830 --> 00:04:54,030
Hard to test not is hard to test so it is always recommended that we use more data for testing rather

61
00:04:54,030 --> 00:04:55,340
than real data.

62
00:04:55,710 --> 00:05:01,260
So I would like to probably have something like pre-alert address cluster print and the mock address

63
00:05:01,260 --> 00:05:02,220
class supplied.

64
00:05:02,490 --> 00:05:05,600
But what is the problem here.

65
00:05:05,790 --> 00:05:09,320
Storing is directly creating the real address class itself.

66
00:05:09,600 --> 00:05:14,550
So I don't have a facility to provide the mock address the mock was in.

67
00:05:14,550 --> 00:05:20,130
We cannot provide it so it is going to become difficult for testing because probably the address class

68
00:05:20,340 --> 00:05:25,110
is eventually making some changes in the live data and I don't want that.

69
00:05:25,110 --> 00:05:27,340
So that is one of the main problem.

70
00:05:27,510 --> 00:05:31,040
The main plus cannot share objects with other classes.

71
00:05:31,050 --> 00:05:35,400
Why not check because it is creating its own its own.

72
00:05:35,880 --> 00:05:41,960
It is clearly independent object itself in the constructor so sharing of the object is also not possible.

73
00:05:42,390 --> 00:05:48,810
And of course there is no way if you look at student it reflects that a student is dependent on address

74
00:05:48,900 --> 00:05:53,260
and subject because the constructor is also not having any parameter.

75
00:05:53,430 --> 00:05:55,290
So there is dependency here.

76
00:05:55,360 --> 00:05:59,760
There are dependencies from outside of the student.

77
00:05:59,790 --> 00:06:03,510
So these are the major problem that sort of dependencies.

78
00:06:03,960 --> 00:06:06,900
We have no control over students capabilities.

79
00:06:07,260 --> 00:06:11,370
We can control the dependencies a class become difficult to test.

80
00:06:11,630 --> 00:06:21,260
So all because of the tight coupling the class becomes inflexible hard to test cannot share the dependent

81
00:06:21,270 --> 00:06:25,980
objects and it is hiding the dependency.

82
00:06:25,980 --> 00:06:33,360
Also how can we make this whole thing easy to make the whole code easy or to reuse.

83
00:06:33,360 --> 00:06:39,730
We can have a constructor taking as paramita up an address and subject.

84
00:06:39,780 --> 00:06:45,490
So when we have the constructor taking as part of it address and subject is that one it's somebody who

85
00:06:45,490 --> 00:06:52,080
is creating student will have the opportunity to create an object of address and subject and pass it

86
00:06:52,140 --> 00:06:53,050
over here.

87
00:06:53,520 --> 00:07:01,010
So yes they can really create object of the real address and real subject or even if other ways put

88
00:07:01,050 --> 00:07:08,040
more testing they can write the class in protect from address inherited from object and in return objects

89
00:07:08,040 --> 00:07:09,870
can be patched.

90
00:07:09,870 --> 00:07:18,160
So basically we are overcoming the problems that is inflexibility and hard to test.

91
00:07:18,210 --> 00:07:24,510
Now that we have done D-cup on all the dependency from the student class to create an instance of student

92
00:07:24,510 --> 00:07:25,950
we need to create as follows.

93
00:07:25,950 --> 00:07:28,210
That means we will first grade it.

94
00:07:28,220 --> 00:07:35,450
This object will then create the student object and these objects will pass it to the student or advantage

95
00:07:35,450 --> 00:07:42,470
of the Explained or we can have a mock address object which is extending address Mock's objects object

96
00:07:42,860 --> 00:07:46,390
extending subject and interop parsing the ordinal object.

97
00:07:46,410 --> 00:07:54,280
The real address and real students we can pass the mark was often the definition of the address and

98
00:07:54,700 --> 00:07:58,750
the dependencies are decoupled from the Student class itself.

99
00:07:59,620 --> 00:08:05,700
That means now storing is not directly dependent on address and subjects.

100
00:08:05,920 --> 00:08:12,880
We have the opportunity of passing either objects of type address and student only or the inherited

101
00:08:12,880 --> 00:08:14,360
classes from it.

102
00:08:14,890 --> 00:08:21,140
So it becomes much flexible for testing but what is the drawback of this approach.

103
00:08:21,190 --> 00:08:27,670
The consumer of the student that means the one who is creating an object of class Studen.

104
00:08:28,060 --> 00:08:34,600
There is an need for explicitly creating different object that means I have to write code and create

105
00:08:34,600 --> 00:08:36,300
dependent objects.

106
00:08:36,450 --> 00:08:41,340
The student can set its problem at the consumer expense.

107
00:08:41,500 --> 00:08:44,540
So what a student class was really doing.

108
00:08:44,700 --> 00:08:46,890
Now we are supposed to do that.

109
00:08:46,900 --> 00:08:49,280
This is a basic drawback of this problem.

110
00:08:49,360 --> 00:09:00,270
Otherwise yes we have really decoupled the student from address on subjects this is supposed to be mock

111
00:09:00,270 --> 00:09:01,170
subjects

112
00:09:05,160 --> 00:09:07,750
then we have got another route.

113
00:09:07,860 --> 00:09:09,480
There is a factory pattern.

114
00:09:09,900 --> 00:09:13,950
So why should the consumer take the expense for that.

115
00:09:13,950 --> 00:09:17,850
We can have the factory battle the factory approach.

116
00:09:17,850 --> 00:09:23,880
We need something that takes care of assembling all parts of the student without needing to explicitly

117
00:09:23,880 --> 00:09:26,330
create dependent objects.

118
00:09:26,460 --> 00:09:34,540
That means we will have something called the Student factory storing factory we create student maybe

119
00:09:34,540 --> 00:09:36,020
something like I'll put here.

120
00:09:36,040 --> 00:09:39,980
The student is going to be created by a student factory.

121
00:09:40,070 --> 00:09:44,910
We will have a dress which will take care of creating that object.

122
00:09:44,930 --> 00:09:50,150
We will create subject which will take care of grading the subjects object.

123
00:09:50,170 --> 00:09:52,450
So this is affect the class.

124
00:09:52,610 --> 00:09:57,860
And in fact the class we are providing all the mentors maybe actually speaking we can make these mentors

125
00:09:57,890 --> 00:10:04,880
as static so that data on the factory class itself can create student using grade address grade subjects

126
00:10:05,420 --> 00:10:08,910
and they take it off creating up appropriate objects.

127
00:10:09,080 --> 00:10:15,830
So like this I can have a real student factory I can have a mock student factory mock student factory

128
00:10:15,830 --> 00:10:19,230
will be used for creating mock objects and so on.

129
00:10:19,920 --> 00:10:27,200
So it is creating and creating subject with just these things.

130
00:10:27,390 --> 00:10:33,940
Likewise I would have mocked studen which can create market dress and mock mock subject and thats how

131
00:10:34,030 --> 00:10:35,070
this thing can be done.

132
00:10:36,040 --> 00:10:39,900
So basically the factory more to the factory model.

133
00:10:39,930 --> 00:10:46,510
What we have used you have the factory approach to an extent has resolved my problem with the consumer

134
00:10:46,510 --> 00:10:50,950
doesn't have to worry about creating the object explicitly.

135
00:10:51,220 --> 00:10:57,230
The metters will take care of that doing them and these matters are internally used for creating Storen.

136
00:10:57,430 --> 00:11:02,850
So I would simply say in fact we don't create it and that takes care of everything for us.

137
00:11:04,340 --> 00:11:05,950
But this also has a problem.

138
00:11:05,960 --> 00:11:07,130
What is that.

139
00:11:07,350 --> 00:11:10,200
As the number of objects keep growing.

140
00:11:12,950 --> 00:11:20,860
Factory is going to become huge interdependency will become very complicated to manage it.

141
00:11:20,890 --> 00:11:24,810
Right now we are only seeing only three classes what in real project.

142
00:11:24,820 --> 00:11:31,460
Maybe we will have 100 classes in such kind of cases be writing a factory is going to make things much

143
00:11:31,540 --> 00:11:33,450
complicated.

144
00:11:33,550 --> 00:11:35,210
So what is a better approach.

145
00:11:35,230 --> 00:11:43,570
What would be the best is if we could simply list the things we want to build without having to define

146
00:11:43,690 --> 00:11:50,590
which dependency gets injected into what basically we want the magic word actually we want to ease up

147
00:11:50,580 --> 00:11:55,730
our job and we want to delegate creation of objects to the framework.

148
00:11:55,810 --> 00:12:00,450
I want to only say that class he is interested in.

149
00:12:00,460 --> 00:12:05,290
B Hey somebody played me an object of B and give it all I want to use it.

150
00:12:05,440 --> 00:12:06,950
That is what we want.

151
00:12:07,420 --> 00:12:13,180
That means that somebody is nothing but the framework underlying framework which is responsible for

152
00:12:13,180 --> 00:12:15,820
creating the object and giving us the reference.

153
00:12:15,940 --> 00:12:21,030
And we dont actually make it onto colors corresponding objects functionality.

154
00:12:21,460 --> 00:12:22,970
That is what we want.

155
00:12:23,270 --> 00:12:31,990
But heres what we have understood clearly is what is dependency index Dovi do not have a perfect solution

156
00:12:32,350 --> 00:12:37,810
and we are going to have that when we see the API and Goulart how it is implemented.

157
00:12:38,020 --> 00:12:42,970
But we clearly understood at this point of time what is dependency index.

158
00:12:43,060 --> 00:12:49,500
So we will see eventually that in Angola for him but there is something called an index for what we

159
00:12:49,500 --> 00:12:56,760
are supposed to do is what classes have to be automatically instantiated and given to dependent we should

160
00:12:56,900 --> 00:13:02,590
disturb those classes with the injector and Indic or a period of time will take care of creating the

161
00:13:02,590 --> 00:13:07,360
instance of them as and when needed and give it to that dependent object.

162
00:13:07,440 --> 00:13:12,340
How is all this achieved in luck is what we will go in the next session.

163
00:13:12,360 --> 00:13:19,770
So I believe at this point of time youre very clear about what is the problem with tight coupling basically

164
00:13:19,770 --> 00:13:26,140
inflexible and difficult to d'Este unable to share objects and dependencies.

165
00:13:26,610 --> 00:13:32,560
All these are basic problems to an extent we can solve the problem by making use of the construct of

166
00:13:33,150 --> 00:13:34,110
getting the reference.

167
00:13:34,140 --> 00:13:35,750
But what is the problem here.

168
00:13:35,910 --> 00:13:42,660
The consumer of the Student class is not burdened with creating those objects explicitly to solve this

169
00:13:42,660 --> 00:13:43,250
problem.

170
00:13:43,260 --> 00:13:47,330
We move on to a packed model where we have factory.

171
00:13:47,580 --> 00:13:53,580
So you will have multiple networks and each of these matters will be responsible for creating the object

172
00:13:53,850 --> 00:13:55,590
of a specific type.

173
00:13:55,890 --> 00:14:02,400
But as the factory becomes bigger and bigger with more and more objects to be instantiated it will create

174
00:14:02,400 --> 00:14:10,110
something like a spider web of a spider web is very complicated from where it starts where it ends.

175
00:14:10,110 --> 00:14:10,970
We will not know.

176
00:14:11,070 --> 00:14:15,440
So that kind of thing would happen and becomes very difficult for a developer to manage.

177
00:14:15,810 --> 00:14:24,030
So we won't know something within the framework that I would tell the framework framework the data classes

178
00:14:24,030 --> 00:14:29,940
you remember and I would want these classes to be instantiated when I was creating independent object

179
00:14:30,000 --> 00:14:32,240
automatically for me.

180
00:14:32,280 --> 00:14:33,440
How is this done.

181
00:14:33,450 --> 00:14:34,980
We will see in the next session.

182
00:14:34,980 --> 00:14:35,730
Thank you.

