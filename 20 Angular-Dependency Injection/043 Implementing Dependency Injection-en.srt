1
00:00:15,990 --> 00:00:23,820
Implementing dependency injection in Angola in the previous session we have clearly understood that

2
00:00:23,820 --> 00:00:26,960
dependency injection concept in Angola.

3
00:00:27,360 --> 00:00:28,710
Basically we did that.

4
00:00:28,920 --> 00:00:34,200
Typically we want to understand how is all this implemented practically.

5
00:00:34,710 --> 00:00:37,680
So there are two things we have to understand here.

6
00:00:37,680 --> 00:00:46,700
One is when we are writing a custom class how our dependency objects going to be created to how or how

7
00:00:46,700 --> 00:00:50,180
will a component create dependency or object.

8
00:00:50,810 --> 00:00:53,510
So let us see this whole thing practically.

9
00:00:53,540 --> 00:00:57,140
So I have created a project dependency on the Chindamo.

10
00:00:57,710 --> 00:01:07,220
And here I would like to add me in my app folder let's say a new typescript file al-Qaida student student

11
00:01:07,380 --> 00:01:09,480
doc daz.

12
00:01:10,380 --> 00:01:12,100
Now installing darbies.

13
00:01:12,230 --> 00:01:14,330
We would like to have a class address

14
00:01:17,180 --> 00:01:19,800
the.

15
00:01:19,950 --> 00:01:29,710
I would like to have a class for students that is also the on the subject.

16
00:01:29,710 --> 00:01:30,100
First

17
00:01:35,800 --> 00:01:39,080
for them purpose we are just creating the skeleton for the time being.

18
00:01:39,220 --> 00:01:43,070
And of course we want student to have address

19
00:01:48,020 --> 00:01:49,370
on subject

20
00:01:55,470 --> 00:02:02,060
so my requirement now is without I explicitly creating objects of address and student.

21
00:02:02,620 --> 00:02:07,090
I mean addressin subjects we want to create object of students.

22
00:02:07,420 --> 00:02:09,760
So there are two ways of doing this.

23
00:02:09,940 --> 00:02:16,980
One is we use index decorator or we use injectables.

24
00:02:17,050 --> 00:02:18,810
The two options are available.

25
00:02:19,310 --> 00:02:28,670
So right now in syntax when we write a student class in Student class we simply pro-white inject off

26
00:02:28,750 --> 00:02:30,660
so and so that's it.

27
00:02:30,670 --> 00:02:31,320
So here

28
00:02:33,980 --> 00:02:40,590
first of course all these classes we want to use it outside so we'll mention them as export

29
00:02:43,860 --> 00:02:44,420
here.

30
00:02:44,460 --> 00:02:51,830
On top we will say in order we want to import indebt

31
00:02:55,560 --> 00:02:56,700
pitys in

32
00:03:02,880 --> 00:03:03,270
or

33
00:03:08,700 --> 00:03:11,010
none here when I write constructor

34
00:03:17,170 --> 00:03:22,330
constructor generally we want to help address

35
00:03:25,500 --> 00:03:27,240
and subjects

36
00:03:31,160 --> 00:03:39,390
and it does not address any call to the local variable address and does not subject to the call to the

37
00:03:39,460 --> 00:03:41,310
local variable subjects.

38
00:03:45,320 --> 00:03:50,350
And thereafter we can make use of this address and object the way we want the rest of the methods.

39
00:03:50,540 --> 00:03:59,090
I'm not so keen about it but where does it mention that students is going to have address and subject

40
00:03:59,150 --> 00:04:01,980
automatically instantiated.

41
00:04:02,000 --> 00:04:05,420
That is where I'm going to provide this decoded

42
00:04:09,130 --> 00:04:17,220
doesn't yet give it up for a dress and yet get rid of both subjects.

43
00:04:17,970 --> 00:04:22,440
So now whenever I want I can create objects of student.

44
00:04:23,160 --> 00:04:30,080
And that in turn will automatically create for me object of address and subject.

45
00:04:30,210 --> 00:04:35,560
Of course some the student itself also should be declared as indictable.

46
00:04:36,030 --> 00:04:37,190
So this is one syntax.

47
00:04:37,200 --> 00:04:38,490
What we have.

48
00:04:39,380 --> 00:04:44,440
But how are we going to create the instance of student sunbed.

49
00:04:44,470 --> 00:04:54,130
We have to register this address subject and students as tokens then only we can create the objects.

50
00:04:54,290 --> 00:04:56,800
This we get achieved in two ways.

51
00:04:56,980 --> 00:05:02,440
Right now this is Option 1 decorating constructor with Indic.

52
00:05:02,470 --> 00:05:11,090
That's what I've done to option 2 is declared the class as indictable.

53
00:05:11,090 --> 00:05:12,500
That's it.

54
00:05:12,500 --> 00:05:19,580
If you declare the class as indictable automatically an object of this class will be created wherever

55
00:05:19,640 --> 00:05:21,210
it is required.

56
00:05:23,240 --> 00:05:29,750
So well object of student is created internally it is going to create object of address and subject

57
00:05:29,750 --> 00:05:31,100
for us.

58
00:05:31,100 --> 00:05:36,290
But how are we going to create an object of thought and some very great object of student also for that

59
00:05:36,560 --> 00:05:45,290
we have to create student as it can look to create it too can we use this EPA which we have discussed

60
00:05:45,330 --> 00:05:45,880
earlier.

61
00:05:46,270 --> 00:05:48,930
So why not go to a component.

62
00:05:49,180 --> 00:05:53,920
My project or go to our component and clean up component.

63
00:05:53,930 --> 00:05:55,530
We will write this line.

64
00:05:56,970 --> 00:06:02,090
Reliable index of for which I have to use

65
00:06:05,270 --> 00:06:07,460
deflective Indic.

66
00:06:07,660 --> 00:06:10,990
And here we should put away students subject.

67
00:06:11,000 --> 00:06:12,310
Nowhere are these classes

68
00:06:15,570 --> 00:06:16,640
employed.

69
00:06:19,330 --> 00:06:28,660
Student dress and subjects from Dart's last Enderby's

70
00:06:36,760 --> 00:06:41,930
because you're not writing in a matter output let's say in the constructor.

71
00:06:51,950 --> 00:06:52,540
That's it

72
00:06:55,680 --> 00:06:59,210
so student.

73
00:06:59,620 --> 00:07:03,640
Student We don't want students.

74
00:07:03,750 --> 00:07:04,370
Student

75
00:07:09,110 --> 00:07:19,500
No we can declare it no so that Indico is equal to reflective injected not resolve.

76
00:07:19,840 --> 00:07:28,190
And we are providing three providers does not store of course for that I declare a variable of paper

77
00:07:28,190 --> 00:07:29,280
students.

78
00:07:29,810 --> 00:07:33,090
So now can I use student anywhere in my template.

79
00:07:33,120 --> 00:07:34,360
Yes we can do that.

80
00:07:34,530 --> 00:07:36,940
That all we will see in subsequent chapters.

81
00:07:37,040 --> 00:07:43,260
Right now in this particular chapter of dependency indiction I just would put a break point here and

82
00:07:43,260 --> 00:07:50,550
show you that proper object offers to read and corresponding address and subject objects are created

83
00:07:50,550 --> 00:07:51,780
for us.

84
00:07:51,780 --> 00:07:53,430
So lets run this now.

85
00:08:03,610 --> 00:08:04,550
It breaks.

86
00:08:06,630 --> 00:08:13,770
And you'll see that we have student created Elbrus and subject.

87
00:08:13,830 --> 00:08:19,200
Of course there is no data but if you have data all that will become visible You will not know that

88
00:08:19,200 --> 00:08:26,190
is important otherwise it would all be null and now just put them up for us.

89
00:08:26,280 --> 00:08:33,880
I will go to this and remove Indic and then remove anything.

90
00:08:33,990 --> 00:08:36,750
So this has become an ordinary class no.

91
00:08:36,900 --> 00:08:42,790
And now you will notice that if I run we are going to get an error.

92
00:08:42,950 --> 00:08:43,780
It will not look

93
00:08:48,550 --> 00:08:50,940
see it has failed.

94
00:08:51,250 --> 00:09:01,190
So press F 12 and you see there are errors cannot resolve all parameters for student to create an object

95
00:09:01,190 --> 00:09:07,100
of plus two that it now needs address and subjects ought to be created.

96
00:09:07,250 --> 00:09:10,830
But at present subjects are not mentioned as indictable.

97
00:09:12,650 --> 00:09:15,570
So we need this instead.

98
00:09:15,620 --> 00:09:22,520
What is the matter if you don't mention indirect or alternative I said at the class level we can make

99
00:09:22,520 --> 00:09:27,120
mention of the rate indictable.

100
00:09:27,130 --> 00:09:28,190
That's it.

101
00:09:28,660 --> 00:09:32,580
So no we need injectable.

102
00:09:32,660 --> 00:09:38,700
Same thing aliments and foot this also for the songs.

103
00:09:39,320 --> 00:09:40,620
That is the second element.

104
00:09:40,640 --> 00:09:46,400
Now again it is going to work perfectly Laufer construct we don't have to put a direct indirect so stop

105
00:09:46,400 --> 00:09:51,270
debugging run once again got some head of

106
00:09:55,930 --> 00:09:57,760
take note of the lawyer.

107
00:09:57,880 --> 00:09:59,200
It's all fine.

108
00:10:00,040 --> 00:10:03,550
The error which is showing is I've not saved this

109
00:10:06,040 --> 00:10:07,000
solution file.

110
00:10:07,000 --> 00:10:07,890
Let me see if that.

111
00:10:07,890 --> 00:10:10,380
That's it.

112
00:10:10,380 --> 00:10:15,010
So again you can run in debug mode.

113
00:10:15,480 --> 00:10:17,490
It breaks here and we're getting student

114
00:10:23,560 --> 00:10:28,640
address on subjects instantiated it up at runtime.

115
00:10:29,470 --> 00:10:33,560
So I clearly repeat there are two options.

116
00:10:33,610 --> 00:10:34,480
One is

117
00:10:37,400 --> 00:10:43,770
Option one for class pro-white Inday.

118
00:10:44,270 --> 00:10:47,980
If you are not able to provide indirect because the class may be already there.

119
00:10:49,710 --> 00:10:54,690
You have to provide for the class indictable.

120
00:10:54,880 --> 00:10:57,530
That is the first the second step.

121
00:10:57,850 --> 00:11:01,690
We have to inject object into the component indirect object.

122
00:11:01,880 --> 00:11:07,980
We use reflective injector got resolved and carried with the list of providers.

123
00:11:08,290 --> 00:11:17,990
But to this also there is other data we don't have to explicitly do all this the internet is we use

124
00:11:18,150 --> 00:11:20,340
a Windows property.

125
00:11:20,540 --> 00:11:23,620
So either at the component level you can do this.

126
00:11:23,730 --> 00:11:32,480
I just copy this and as it is I'll paste it here in my component so we have a component in component.

127
00:11:32,480 --> 00:11:39,920
We will put one more propertied that this pro-white notes the components decorator has provided.

128
00:11:40,040 --> 00:11:46,330
So when you put providers you have the advantages you don't have to know what anything like this.

129
00:11:46,530 --> 00:11:55,470
Instead simply provide for the construct of the paramita.

130
00:11:55,780 --> 00:11:56,680
That's it.

131
00:11:56,710 --> 00:11:58,480
So when you do this automatically.

132
00:11:58,480 --> 00:12:04,100
Object of this class will be created for us and that we can get it.

133
00:12:04,660 --> 00:12:06,740
So we don't have to actually use this.

134
00:12:06,800 --> 00:12:10,510
It also what we are doing here.

135
00:12:12,470 --> 00:12:14,210
We can actually do it here.

136
00:12:15,340 --> 00:12:18,240
But we are not creating object of a subjects directly.

137
00:12:18,310 --> 00:12:27,960
So students should be sufficiently see under Program.

138
00:12:28,160 --> 00:12:31,850
It should break that we don't any at all.

139
00:12:31,890 --> 00:12:34,020
It just breaks that scope.

140
00:12:34,220 --> 00:12:37,040
I'm sure because we have not mentioned addressin subjects

141
00:12:39,830 --> 00:12:41,780
see no point of an atlas.

142
00:12:42,200 --> 00:12:44,960
So direct object of a different subject is not creative.

143
00:12:44,990 --> 00:12:54,020
So only we will provide address and subjects not on this

144
00:12:58,010 --> 00:13:04,500
and it should break here without any problem this thing will affect.

145
00:13:04,780 --> 00:13:15,660
So no need to explicitly use this the same value we can as it is give it in the decoded

146
00:13:18,630 --> 00:13:25,050
and we already learned earlier how we can alternate to really white floor brackets in decks that we

147
00:13:25,050 --> 00:13:40,640
can use use glass and how we can provide talk like this in and out of how we can put a white and use

148
00:13:40,640 --> 00:13:41,210
class

149
00:13:44,450 --> 00:13:53,150
as is Dopp mentioned Duc's index either give just the token like this all the syntax like this would

150
00:13:53,250 --> 00:14:01,420
are fine so long as the token is student can actually get the last name was an object of students who

151
00:14:01,600 --> 00:14:08,350
read the doggedness to object of this type will be created for us.

152
00:14:12,720 --> 00:14:24,720
So I should use the break point to run in debug mode and we should still get the output.

153
00:14:24,870 --> 00:14:29,230
And if you have a class inherited from student

154
00:14:38,170 --> 00:14:39,630
and child

155
00:14:44,090 --> 00:14:45,170
what we can do is

156
00:14:52,930 --> 00:14:59,690
well sort of extents.

157
00:14:59,760 --> 00:15:03,060
Suppose we have like this for instance of student

158
00:15:08,020 --> 00:15:15,610
he or I can mention students and so what we what we're going to get here is object of students and just

159
00:15:15,620 --> 00:15:29,590
to distinguish I figure one member of some test we do this bring some dummy on

160
00:15:32,460 --> 00:15:38,210
to life get on with the report they're going to create an object of

161
00:15:41,600 --> 00:15:49,910
expanders that it's actually the creating object of student time not students because this test is only

162
00:15:49,910 --> 00:15:53,410
there in student.

163
00:15:53,550 --> 00:15:54,700
That's very interesting.

164
00:15:57,810 --> 00:16:01,040
So what is prep for syntax is this one.

165
00:16:01,380 --> 00:16:05,320
So right now I have mentioned providers at the component level.

166
00:16:06,030 --> 00:16:15,630
But I can cut from here and I can mention providers at the more level I can go to at Mordieu and in

167
00:16:15,880 --> 00:16:24,990
model I can give providers so obviously I've to write here in book student

168
00:16:28,790 --> 00:16:29,980
student child

169
00:16:32,620 --> 00:16:36,420
address subjects.

170
00:16:36,420 --> 00:16:37,940
All these are and seem fine

171
00:16:42,000 --> 00:16:45,420
student and that's it.

172
00:16:45,420 --> 00:16:50,940
So I've cut from component and I've provided at the module level.

173
00:16:51,420 --> 00:16:53,140
Are we still going to get out.

174
00:16:53,190 --> 00:16:58,060
Yes we'll still get we still get the CMO

175
00:17:01,920 --> 00:17:03,330
same thing.

176
00:17:03,420 --> 00:17:13,840
So you have the choice of either mentioning at the module level or Mensing at components of an advantage

177
00:17:13,840 --> 00:17:16,930
of mentioning provider providers

178
00:17:19,570 --> 00:17:24,640
at the component level it is local to that component only.

179
00:17:24,910 --> 00:17:31,000
But if you give it at the module level we can make use of it in all the components so based on your

180
00:17:31,000 --> 00:17:38,350
school requirement you have to probably provide the provider if you want providers to be available in

181
00:17:38,410 --> 00:17:39,900
all the components of it.

182
00:17:39,940 --> 00:17:44,230
You give it in providers in any model decorator.

183
00:17:44,410 --> 00:17:49,480
But if you want it to be used only for that component get in the component.

184
00:17:50,110 --> 00:17:51,820
Very interesting.

185
00:17:51,830 --> 00:17:57,020
So I believe this gives you Pipitone And of course this will change.

186
00:17:57,110 --> 00:18:06,460
You're also I should mention subjects Dovi won't only instantiate student because student is dependent

187
00:18:06,460 --> 00:18:07,810
on a presence object.

188
00:18:07,810 --> 00:18:14,170
They should also be registered as provider and that is what is done using providers property in components

189
00:18:14,260 --> 00:18:16,810
or in DMORT.

190
00:18:16,980 --> 00:18:19,260
So I believe this gives you a syntax.

191
00:18:19,260 --> 00:18:28,680
Once again just to repeat quickly we have absolute all mentioning clause as in J.

192
00:18:28,990 --> 00:18:34,530
So when I'm creating object of student it is automatically going to in-depth address and subject in

193
00:18:36,310 --> 00:18:41,670
either do this or at the class level itself mentioned it as indictable.

194
00:18:42,220 --> 00:18:49,620
When you mention the class level indictable at the constructor level you don't have to do it.

195
00:18:49,650 --> 00:18:56,240
Do we know what we want to create the instance of the class mentioned there.

196
00:18:56,300 --> 00:19:00,510
We have to use reflective injector weasel and create.

197
00:19:01,070 --> 00:19:06,380
But this is not what we do in routine programming in routine programming.

198
00:19:06,380 --> 00:19:14,180
We mentioned the providers to providers property indeed decorator not decorator can be either components

199
00:19:14,180 --> 00:19:18,870
decorator or it can't be done anymore do liquidate.

200
00:19:18,980 --> 00:19:25,190
That means you want to register providers either at the component level or at the module level.

201
00:19:27,140 --> 00:19:32,620
Then finally one thing we have to understand is obscenely dependencies.

202
00:19:32,620 --> 00:19:34,640
And what is optional dependencies.

203
00:19:34,730 --> 00:19:37,430
We can import obstinant not in the constructor.

204
00:19:37,430 --> 00:19:41,200
You can provide optional a very good option on that one.

205
00:19:41,190 --> 00:19:48,440
It is not the object of the class in which we have this constructor can be directly created without

206
00:19:48,440 --> 00:19:54,540
specifying Loga but it created through index.

207
00:19:54,550 --> 00:19:59,080
That means provider is given logger is there in the provider in such case.

208
00:19:59,110 --> 00:20:06,310
Object of lager will be provided to us that means object of this class can be created with or without

209
00:20:06,310 --> 00:20:08,270
love for instance.

210
00:20:08,290 --> 00:20:11,860
So you are the decorator optional is very important.

211
00:20:14,100 --> 00:20:18,520
When using obstinant our code must be prepared for null value.

212
00:20:18,520 --> 00:20:26,200
This is important if this can be null if we don't register it logger's somewhere in the line.

213
00:20:26,200 --> 00:20:27,100
How do we do it.

214
00:20:27,100 --> 00:20:30,160
How do we actually reduce to two providers.

215
00:20:30,160 --> 00:20:37,610
The logger is not registered to provider because optionally spottier we are not going to get a Instead

216
00:20:37,610 --> 00:20:39,590
we will simply get nothing.

217
00:20:40,040 --> 00:20:47,600
The injector will set the value of longer to NULL in case the Logger class is not registered as the

218
00:20:47,600 --> 00:20:48,730
provider.

219
00:20:49,220 --> 00:20:55,290
So I believe this gives you a very clean and detailed understanding of what is dependency injection

220
00:20:55,700 --> 00:20:59,880
and what are the things required for implementing the same angle or framework.

221
00:21:00,170 --> 00:21:06,770
And we are going to use this particular concept in subsequent topics so that we can write services and

222
00:21:06,860 --> 00:21:10,180
inject them into the component thats all it this.

223
00:21:10,310 --> 00:21:10,960
Thank you.

