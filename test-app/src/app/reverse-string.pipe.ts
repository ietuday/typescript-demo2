import { Pipe, PipeTransform } from '@angular/core';
import { EmployeePipeExample } from './Employee-pipe-example';

@Pipe({
    name: 'reverse'
})
export class ReverseStringPipe implements PipeTransform {
    transform(value: any, start: any, end: any) {
        var text: string = value;
        var sliceStr: string; var subStr: string;
        if (start != null && end != null) {
            subStr = value.split("").slice(start, end).join("");
            sliceStr = value.split("").slice(start, end).reverse().join("");
            return text.replace(subStr, sliceStr);
        }
        else
            return text.split("").reverse().join("");
    }
}
