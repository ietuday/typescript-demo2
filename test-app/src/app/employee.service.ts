import { Injectable } from '@angular/core';
import { EmployeeServiceDemo } from './employee-service-demo';
import { LoggerService } from './logger.service';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private logger: LoggerService) {

   }

  getEmployees(): EmployeeServiceDemo[] {
    this.logger.log("Fetching employee...");
    let employees: EmployeeServiceDemo[] = [
      new EmployeeServiceDemo(1, "Raj Kumar", 11000.12345, new Date(2016, 0, 20)),
      new EmployeeServiceDemo(2, "Sudheer Reddy", 12000, new Date(2016, 0, 22)),
      new EmployeeServiceDemo(3, "Kiran Rao", 13000, new Date(1983, 12, 32)),
      new EmployeeServiceDemo(4, "Kishor Kumar", 14000, new Date(1983, 5, 3)),
      new EmployeeServiceDemo(5, "Usha Rani", 12000, new Date(1985, 5, 31)),
      new EmployeeServiceDemo(6, "Sai Kiran", 12300, new Date(1986, 6, 36)),
      new EmployeeServiceDemo(7, "Jacob John", 11000, new Date(1987, 7, 33)),
    ]
    return employees;
  }
}
