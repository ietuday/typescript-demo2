import { Directive, ElementRef, Renderer, HostBinding, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[ccCardHover]'
})

// To bind the input properties of the host element from directive we use HostBinding. 
// By using the @HostListener and @HostBinding decorators we can both listen to output events from our host element and also bind to input properties on our host element as well

export class CardHoverDirective {
  @HostBinding('class.card-outline-primary') private ishovering: boolean;

  // @Input('ccCardHover') config: Object = {
  //   querySelector: '.card-text'
  // };

  @Input('ccCardHover') config:Object;

  constructor(private el: ElementRef,
    private renderer: Renderer) {
    // renderer.setElementStyle(el.nativeElement, 'backgroundColor', 'gray');
  }

  @HostListener('mouseover') onMouseOver() {
    console.log("config",this.config);
    let part = this.el.nativeElement.querySelector(this.config['querySelector']);
    this.renderer.setElementStyle(part, 'display', 'block');
    this.ishovering = true;
  }

  @HostListener('mouseout') onMouseOut() {
    let part = this.el.nativeElement.querySelector(this.config['querySelector']);
    this.renderer.setElementStyle(part, 'display', 'none');
    this.ishovering = false;
  }
}