import { Directive, OnInit } from '@angular/core';
import { AbstractControl, ValidatorFn, Validator, FormControl, NG_VALIDATORS } from '@angular/forms';
import { UpperCasePipe } from '@angular/common';

function validateUpperCaseFactory(): ValidatorFn{
    return(ctrl: AbstractControl)=>{
        let isValid = ctrl.value.charAt(0).toUpperCase() === ctrl.value.charAt(0);
        if(isValid){
            return null
        }else{
            return{
                upper : {
                    valid: false
                }
            };
        }
    }
}

@Directive({
    selector:'[upper][ngModel]',
    providers:[
        {
            provide: NG_VALIDATORS,
            useExisting: UpperCasePipe,
            multi:true
        }
    ]
})

export class UpperCaseValidator implements Validator{
    validator:ValidatorFn;

    constructor(){
        this.validator = validateUpperCaseFactory();    
    }
    validate(ctrl: FormControl){
        return this.validator(ctrl);
    }
}