import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DependencyInjectionDemoComponent } from './dependency-injection-demo.component';

describe('DependencyInjectionDemoComponent', () => {
  let component: DependencyInjectionDemoComponent;
  let fixture: ComponentFixture<DependencyInjectionDemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DependencyInjectionDemoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DependencyInjectionDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
