import { Directive, ElementRef, Renderer } from '@angular/core';
@Directive({
    selector:'[showalert]'
})

export class ShowAlertDirective{

    constructor(private el:ElementRef, private renderer: Renderer){
        renderer.setElementStyle(el.nativeElement, "cursor", "pointer");
        // renderer.setElementStyle(el.nativeElement, "color", "blue");
        el.nativeElement.style.color = "blue";
        el.nativeElement.style.fontSize = "14pt";

        renderer.listen(el.nativeElement, "mouseover",function(){
            el.nativeElement.style.color = "red";
            el.nativeElement.style.fontSize = "24pt";

        })

        renderer.listen(el.nativeElement, "mouseout",function(){
            el.nativeElement.style.color = "blue";
            el.nativeElement.style.fontSize = "14pt";
        })

        renderer.listen(el.nativeElement, "click",function(){
                // alert('Hello from Directive');
                alert(el.nativeElement.innerText);

        })

    }
}