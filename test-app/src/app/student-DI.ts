// Method 1

// import { Inject } from '@angular/core';

// export class Address{

// }

// export class Subjects{

// }

// export class Student{
//     address: Address;
//     subjects: Subjects;

//     constructor(@Inject(Address) address:Address, @Inject(Subjects) subject:Subjects){
//         this.address = address;
//         this.subjects = subject;
//     }
// }

// Method 2

import { Injectable } from '@angular/core';

@Injectable()
export class Address{

}
@Injectable()
export class Subjects{

}
@Injectable()
export class Student{
    address: Address;
    subjects: Subjects;

    constructor(address:Address,subject:Subjects){
        this.address = address;
        this.subjects = subject;
    }
}

@Injectable()

export class StudentChild extends Student{
    test:string = "Dummy";
}