import { Component, OnInit, ViewChild } from '@angular/core';
import { EmployeeDetail } from '../employeeDetail';

@Component({
  selector: 'app-templete-driven-form',
  templateUrl: './templete-driven-form.component.html',
  styleUrls: ['./templete-driven-form.component.scss']
})
export class TempleteDrivenFormComponent implements OnInit {
  departments:Array<string> = ['Sales', 'Marketing', 'HR', 'Accounts'];
  emp:EmployeeDetail = new EmployeeDetail(1, "Udayaditya Singh", 10000,this.departments[2] );
  @ViewChild('empForm')empForm:any; 

  constructor() { }

  ngOnInit() {

  }

  get diagonistic(){
    return JSON.stringify(this.emp)
  }

  newEmployee(){
    //Get the Data of the form and operate such http call, or manupulation 
    //Reset the form
    console.log("Updated");
    this.emp = new EmployeeDetail(0,'',0,'');
    
  }

  // onSubmit(form:any){
  //   console.log(form.name.value);
  //   this.emp = new EmployeeDetail(0,'',0,'');
  //   form.reset();
  // }

  onSubmit(){
    console.log(this.empForm.controls.name.value);
    this.emp = new EmployeeDetail(0,'',0,'');
    this.empForm.reset();
  }

}
