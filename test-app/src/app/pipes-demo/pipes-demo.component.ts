import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipes-demo',
  templateUrl: './pipes-demo.component.html',
  styleUrls: ['./pipes-demo.component.scss']
})
export class PipesDemoComponent implements OnInit {
  name:string = 'Udayaditya Singh';
  dateOfBirth:Date = new Date(2000,10,17);
  numberofItems:number = 123456789.2587469;
  salary:number = 100000.2584;
  myPercentage:number = .9998;
  number:Array<number> = [1,2,3,4,5,6,7,8,9,10,11,23];
  obj:Object = {
    name:'Udayaditya Singh',
    Place: 'Pune',
    age:26,
    position: 'Software Developer'
  };

  object: Object = {foo: 'bar', baz: 'qux', nested: {xyz: 3, numbers: [1, 2, 3, 4, 5]}};
  arr: Array<string> = ['person1','person2','person3'];
  constructor() { }

  ngOnInit() {
  }

  addItem(value:string){
    this.arr= ['person1','person2','person3']
    this.arr.push(value);
    value = '';
  }
   
}
