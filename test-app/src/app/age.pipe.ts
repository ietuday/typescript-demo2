import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'age'
})
export class AgePipe implements PipeTransform{
    transform(value:Date, format:string):string {
        let todayOffset = new Date().getTime();
        let parDateOffset = value.getTime();
        let diff = todayOffset - parDateOffset;
        // (millisecond * second * minute * hour)
        let oneDay = 1000 * 60 * 60 * 24;
        if(format == null ||format.toLocaleLowerCase() == 'y'){
            return Math.floor(diff / (oneDay * 365)) + " " + "Years"; 
        }else if(format.toLocaleLowerCase() == 'm'){
            return Math.floor(diff / (oneDay * 31)) + " " + "Months"; 
        }else{
            return Math.floor(diff / (oneDay)) + " " + "Days"; 
        }
        
        // return years + " " + "Years";
    }

}
