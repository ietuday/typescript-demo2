import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DestructureArrayComponent } from './destructure-array.component';

describe('DestructureArrayComponent', () => {
  let component: DestructureArrayComponent;
  let fixture: ComponentFixture<DestructureArrayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DestructureArrayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DestructureArrayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
