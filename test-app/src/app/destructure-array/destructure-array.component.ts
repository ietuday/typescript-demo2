import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-destructure-array',
  templateUrl: './destructure-array.component.html',
  styleUrls: ['./destructure-array.component.scss']
})
export class DestructureArrayComponent implements OnInit {

  constructor() { }

  ngOnInit() {
let input = [1, 2]
//let first: number = input[0]
//let second: number = input[1]
let [first, second] = input
console.log (first + " " + second)

let [n1, n2,...rest] = [1, 2, 3, 4, 5]
console.log(n1)
console.log(rest)

let [m1] = [1,2,3,4,5]
console.log(m1)

let [, , m2, m3] = [1, 2, 3, 4, 5, 6]
console.log(m2 + " " + m3)

let [totalDate, year, month, day] = /^(\d\d\d\d)-(\d\d)-(\d\d)$/.exec('2017-07-1') || [0,0,0,0]
console.log(totalDate + " " + year + " " + month + " " + day)

let ob = {
    a: "foo",
    b: 12,
    c: "bar"
}

//let { a, b } = ob;
let { a:A, b:B } = ob;
console.log(A + " " + B)

let {a,...rest1} = ob
console.log(a + " " + rest1.b + " " + rest1.c)

let user = {
    department: "DP1",
    name: "SandeepSoni",
    favouriteCricketer: {
        first: {
            name: "Sachin",
            comment: "Excellent batsman"
        },
        second: {
            name: "Dhoni",
            comment: "Excellent Wicket Keeper"
        }
    },
    hobbies: ["Playing Cricket", "Playing Chess"]
};

let { favouriteCricketer: { first: First, second: Second } } = user;
console.log(First.name + " " + First.comment);        // Sachin
console.log(Second.name + " " + Second.comment);      // Dhoni

let { favouriteCricketer: { first: fav1, second: fav2 }, hobbies: [hob1, hob2] } = user;
console.log(fav1.name);        	// Sachin
console.log(fav2.name);      	// Dhoni
console.log(hob1);          		// Playing Cricket
console.log(hob2);          

type C = { a?: string, b?: number }
function foo(p1: C )
{
    let { a="", b=0 } = p1;
    console.log(a + " " + b)
}

//foo({ a: "A" });
//foo({ a: "A1", b: 1 });
//foo({})

function foo1({ a = "", b = 0 } = { a: "" }) {
    console.log(a + " " + b)
}
//foo1()
//foo1({})

let a1 = [1, 2, 3]
let a2 = [4, 5, 6]

let a12 = [0, ...a1, ...a2, 7]
console.log(a12)

let obs1 = { p1: "p1", p2: 10, p3: true };
//let obs2 = { ...obs1, p2: 100 };
let obs2 = { p2:100, ...obs1 };

console.log(obs2.p1 + " " + obs2.p2 + " " + obs2.p3)
  }

}
