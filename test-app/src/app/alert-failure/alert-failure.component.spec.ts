import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertFailureComponent } from './alert-failure.component';

describe('AlertFailureComponent', () => {
  let component: AlertFailureComponent;
  let fixture: ComponentFixture<AlertFailureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertFailureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertFailureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
