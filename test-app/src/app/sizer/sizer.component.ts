import { Component,Input,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-sizer',
  templateUrl: './sizer.component.html',
  styleUrls: ['./sizer.component.scss']
})
export class SizerComponent {
  @Input() size:number = 10;
  @Output() sizeChange = new EventEmitter<number>();

  constructor() { }

  resize(offset:number){
    this.size +=offset;
    if(this.size > 40){
      this.size = 40
    }else if(this.size < 8){
      this.size = 8;
    }
    this.sizeChange.emit(this.size);
  }
}
