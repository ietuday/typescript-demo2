import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Person } from '../Person';
@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.scss']
})
export class PersonComponent {
  @Input("fn") firstName: string;
  @Input() lastName: string;

  @Output("fnChange") firstNameChange = new EventEmitter<string>();
  @Output() lastNameChange = new EventEmitter<string>();

  changeFirstName(fn:string)
  {
      this.firstName = fn;
      this.firstNameChange.emit(fn);
  }

  changeLastName(ln :string)
  {
      this.lastName = ln;
      this.lastNameChange.emit(ln);
  }

  person: Person;
  constructor()
  {
       this.person = new Person(this.firstName, this.lastName);
  }
  changeToUppercase(value: string): string
  {
      return value.toUpperCase();
  }
  
//   @Input("fn") firstName:string = '';
//   @Input() lastName:string = '';
//   @Output("fnChange") firstNameChange = new EventEmitter<string>();
//   @Output() lastNameChange = new EventEmitter<string>();
//   person:Person;
  

//   constructor() {
//     this.person = new Person(this.firstName, this.lastName);
//    }

//   ngOnInit() {

//   }

//   modelChaged(value:any):void{
//     console.log("After Model Changed",value);
//   }

//   changeToUpper(value:string):string{
//     return value.toUpperCase();
//   }

//   changeFirstName(value:string){
//     console.log("changeFirstName : ",value);
//     this.firstName = value;
//     this.firstNameChange.emit(value);
//   }

//   changeLastName(value:string){
//     this.lastName = value;
//     console.log("changeLastName",value);
    
//     this.lastNameChange.emit(value);
//   }

  

}
