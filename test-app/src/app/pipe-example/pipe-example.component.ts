import { Component, OnInit } from '@angular/core';
import { EmployeePipeExample } from '../Employee-pipe-example';
@Component({
  selector: 'app-pipe-example',
  templateUrl: './pipe-example.component.html',
  styleUrls: ['./pipe-example.component.scss']
})
export class PipeExampleComponent implements OnInit {
  text:string = "Hello World";
  employees:EmployeePipeExample[] = [
    new EmployeePipeExample(1, "Raj Kumar", 11000.12345, new Date(1980, 5, 13)),
    new EmployeePipeExample(2, "Sudheer Reddy", 12000, new Date(1982, 15, 23)),
    new EmployeePipeExample(3, "Kiran Rao", 13000, new Date(1983, 25, 32)),
    new EmployeePipeExample(4, "Kishor Kumar", 14000, new Date(1983, 25, 3)),
    new EmployeePipeExample(5, "Usha Rani", 12000, new Date(1985, 25, 31)),
    new EmployeePipeExample(6, "Sai Kiran", 12300, new Date(1986, 26, 36)),
    new EmployeePipeExample(7, "Jacob John", 11000, new Date(1987, 27, 33)),
    new EmployeePipeExample(8, "Udayaditya Singh", 21000, new Date(1987, 27, 33)),
    new EmployeePipeExample(9, "Amit Singh", 15000, new Date(1987, 27, 33)),    

]
  constructor() { }

  ngOnInit() {
  }

}
