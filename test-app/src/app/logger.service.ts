import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {
  logs:Array<string> = [];

  constructor() { }

  log(message:string):void{
    this.logs.push(message);
    console.log("logs : ",this.logs);
  }

  printLog(){
    for (let i:number = 0; i < this.logs.length; i++) {
      const element = this.logs[i];
      console.log(element + '\n');
    }
  }
}
