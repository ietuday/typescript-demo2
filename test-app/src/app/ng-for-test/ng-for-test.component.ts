import { Component, OnInit } from '@angular/core';
import {Hero} from '../hero'
import {Heroes} from "../heroes";

@Component({
  selector: 'app-ng-for-test',
  templateUrl: './ng-for-test.component.html',
  styleUrls: ['./ng-for-test.component.scss']
})
export class NgForTestComponent implements OnInit {

  // heros:HEROES
  constructor() { }

  ngOnInit() {
  }

}
