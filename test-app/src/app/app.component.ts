import { Component, ReflectiveInjector } from '@angular/core';
import { Employee, EmployeeType } from './Employee';
import { PersonBasicInfo } from './PersonBasicInfo';
import { AlertSucessComponent } from './alert-sucess/alert-sucess.component';
import { AlertFailureComponent } from './alert-failure/alert-failure.component';
import { Student, Address, Subjects, StudentChild } from './student-DI';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [{ provide: Student, useClass: StudentChild }, Address, Subjects]
})
export class AppComponent {
  alert = AlertSucessComponent;
  myTime: Date = new Date();
  fontSize: number = 10;


  employee: Array<Employee> = [
    new Employee(1, "E1", 10000, EmployeeType.Daily),
    new Employee(2, "E2", 11000, EmployeeType.Contract),
    new Employee(3, "E3", 13000, EmployeeType.Permanent)
  ];

  selectedEmployee: Employee = this.employee[1];
  stud: Student;
  constructor(private student: Student, private router: Router) {
    // var injector = ReflectiveInjector.resolveAndCreate([Student, Address, Subjects]);
    // this.stud = injector.get(Student)
    // this.stud = student;
    console.log(this.stud);

  }

  //  Here instead of using automatic routing we can use manual routing using Router.Navigate.
  goHome() {
    this.router.navigate(['emp']);
  }

  evtNameHandler(studName: string) {
    console.log("Hello" + studName);

  }
  evtPersonalHandler(PersonBasicInfo: PersonBasicInfo) {
    console.log("person", PersonBasicInfo);

    console.log("Hello " + PersonBasicInfo.FName + " " + PersonBasicInfo.LName);
  }

  changeComponent() {
    if (this.alert == AlertFailureComponent)
      this.alert = AlertSucessComponent;
    else
      this.alert = AlertFailureComponent;
  }
}
