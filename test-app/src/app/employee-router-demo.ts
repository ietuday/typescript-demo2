import { DepartmentRouterDemo } from "./department-router-demo";

export class EmployeeRouterDemo {
    EmpId: number;
    EmpName: string;
    EmpSalary: number;
    Department: DepartmentRouterDemo;
}