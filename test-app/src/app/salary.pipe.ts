import { Pipe, PipeTransform } from '@angular/core';
import { EmployeePipeExample } from './Employee-pipe-example';

@Pipe({
    name: 'salary'
})
export class SalaryPipe implements PipeTransform {
    transform(allEmployees: EmployeePipeExample[], salaryGreaterThan: number, salaryLessThan:number): EmployeePipeExample[] {
        return allEmployees.filter(emp => emp.salary > salaryGreaterThan).filter(emp => emp.salary < salaryLessThan);
    }

}
