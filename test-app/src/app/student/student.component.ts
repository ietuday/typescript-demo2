import { Component, Input, Output, EventEmitter } from '@angular/core';
import { PersonBasicInfo } from '../PersonBasicInfo';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.scss']
})
export class StudentComponent {
  @Input() name: any;
  @Output('evtName') myNameEvent: EventEmitter<string> = new EventEmitter<string>();

  RaiseNameEvent(studName: string) {
    console.log("studName",studName);
    
    this.myNameEvent.emit(this.name);
  }

  @Output() evtPerson: EventEmitter<PersonBasicInfo> = new EventEmitter<PersonBasicInfo>();
  person = new PersonBasicInfo();

  RaisePersonEvent() {
    this.evtPerson.emit(this.person);
  }

}
