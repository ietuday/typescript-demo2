import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeRouterDemoComponent } from './employee-router-demo.component';

describe('EmployeeRouterDemoComponent', () => {
  let component: EmployeeRouterDemoComponent;
  let fixture: ComponentFixture<EmployeeRouterDemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeRouterDemoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeRouterDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
