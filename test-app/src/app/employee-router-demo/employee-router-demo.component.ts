import { Component, OnChanges, OnInit, OnDestroy } from '@angular/core';
import { EmployeeRouterDemo } from '../employee-router-demo';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-employee-router-demo',
  templateUrl: './employee-router-demo.component.html',
  styleUrls: ['./employee-router-demo.component.scss']
})
export class EmployeeRouterDemoComponent implements OnInit, OnDestroy {
  emps: EmployeeRouterDemo[];
  selectedEmployee:EmployeeRouterDemo = null;
  paramsSub;
  constructor(private route:ActivatedRoute) {
    var lstEmp: EmployeeRouterDemo[] = [
      { EmpId: 1, EmpName: "Phani", EmpSalary: 15000, Department: { DeptId: 1, DeptName: "D1" } },
      { EmpId: 2, EmpName: "Kranth", EmpSalary: 115000, Department: { DeptId: 2, DeptName: "D2" } },
      { EmpId: 3, EmpName: "Kranth2", EmpSalary: 115000, Department: { DeptId: 1, DeptName: "D1" } },
      { EmpId: 4, EmpName: "Kranth3", EmpSalary: 115000, Department: { DeptId: 2, DeptName: "D2" } }

    ];
    this.emps = lstEmp;
  }

  ngOnInit(){
    this.paramsSub = this.route.params.subscribe(params => {
      if (params["id"] != null)
      this.selectedEmployee = this.emps.filter(e => e.EmpId == params["id"])[0];
      });
  }

  ngOnDestroy(): void {
    this.paramsSub.unsubscribe();
  }
}
