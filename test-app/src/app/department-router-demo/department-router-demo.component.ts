import { Component } from '@angular/core';
import { DepartmentRouterDemo } from '../department-router-demo';

@Component({
  selector: 'app-department-router-demo',
  templateUrl: './department-router-demo.component.html',
  styleUrls: ['./department-router-demo.component.scss']
})
export class DepartmentRouterDemoComponent {
  depts: DepartmentRouterDemo[];
  constructor() {
    var lstDept: DepartmentRouterDemo[] = [
      { DeptId: 1, DeptName: "D1" },
      { DeptId: 2, DeptName: "D2" }
    ];
    this.depts = lstDept;
  }
}
