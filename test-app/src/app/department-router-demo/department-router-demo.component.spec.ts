import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentRouterDemoComponent } from './department-router-demo.component';

describe('DepartmentRouterDemoComponent', () => {
  let component: DepartmentRouterDemoComponent;
  let fixture: ComponentFixture<DepartmentRouterDemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartmentRouterDemoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentRouterDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
