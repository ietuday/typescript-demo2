import { Component, OnInit } from '@angular/core';
import { UpperCaseValidator } from '../upper.validator';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-custom-validator',
  templateUrl: './custom-validator.component.html',
  styleUrls: ['./custom-validator.component.scss']
})
export class CustomValidatorComponent implements OnInit {
  myForm:FormGroup;

  constructor(private fb:FormBuilder) {
    
   }

  ngOnInit() {
    // this.myForm = this.fb.group({
    //   'myname': new FormControl(["", [Validators.required, UpperCaseValidator]])
    // });  

    this.myForm = this.fb.group({
      firstName: ['Udayaditya',[Validators.required,UpperCaseValidator]]
    })
  }

}
