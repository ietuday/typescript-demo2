import { Component, OnInit } from '@angular/core';
import { EmployeeServiceDemo } from '../employee-service-demo';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-service-demo',
  templateUrl: './service-demo.component.html',
  styleUrls: ['./service-demo.component.scss']
})
export class ServiceDemoComponent implements OnInit {
  employees:EmployeeServiceDemo[];

  constructor(private employeeService: EmployeeService) {
    
   }

  ngOnInit() {
    this.employees = this.employeeService.getEmployees();
  }


}
