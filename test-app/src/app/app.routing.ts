import { NgModule } from '@angular/core';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeRouterDemoComponent } from './employee-router-demo/employee-router-demo.component';
import { DepartmentRouterDemoComponent } from './department-router-demo/department-router-demo.component'
import { FilenotfoundComponent } from './filenotfound/filenotfound.component';
import { HomeComponent } from './home/home.component';
// const routes: Routes = [
//     { path: '', redirectTo: 'home', pathMatch: 'full' },
//     { path: 'emp', component: EmployeeRouterDemoComponent },
//     { path: 'emp/:empId', component: EmployeeRouterDemoComponent },
//     { path: 'home', component: HomeComponent },
//     { path: 'dept', component: DepartmentRouterDemoComponent },
//     { path: '**', component: FilenotfoundComponent },
// ];

const routes: Routes = [
    {
        path: '', component: HomeComponent,
        children: [
            { path: '', redirectTo: 'emp', pathMatch: 'full' },
            { path: 'emp', component: EmployeeRouterDemoComponent },
            { path: 'emp/:id', component: EmployeeRouterDemoComponent },
            { path: 'dept', component: DepartmentRouterDemoComponent },
            { path: 'dept/:id', component: DepartmentRouterDemoComponent },
            { path: '**', component: FilenotfoundComponent }
        ]
    },
    { path: 'home', component: HomeComponent }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);

@NgModule({
    imports: [routing],
    exports: [RouterModule]
})
export class AppRoutingModule {

}