1
00:00:16,110 --> 00:00:23,250
Eager loading and lazy loading of more you know that we have understood that angular application can

2
00:00:23,250 --> 00:00:30,990
be divided into many more units wherein we are going to have one or model most often model and then

3
00:00:30,990 --> 00:00:35,420
there are often many features more we need to understand in this particular system.

4
00:00:35,430 --> 00:00:37,750
How are these modules loaded.

5
00:00:37,770 --> 00:00:39,840
So yes there are two ways of doing it.

6
00:00:39,840 --> 00:00:43,450
One is called eager eagerly and the other is lazy loading.

7
00:00:43,640 --> 00:00:50,310
And what is the basic difference between either loading and unloading at the time of startup when the

8
00:00:50,310 --> 00:00:58,260
application is bootstrapping and that time only by default angler is going to load all the more units.

9
00:00:59,070 --> 00:01:06,690
But no in terms of loading them in at the startup time we will want to load them on the on demand basis.

10
00:01:06,690 --> 00:01:13,680
That means only when they need it or what is the benefit of it is obviously because we are not supposed

11
00:01:13,680 --> 00:01:16,730
to know all the models at startup.

12
00:01:16,730 --> 00:01:20,120
We are going to have a decrease in the startup time.

13
00:01:20,220 --> 00:01:24,750
That means the obligation were on would load faster and start producing results.

14
00:01:24,810 --> 00:01:27,230
The user doesn't have to wait.

15
00:01:27,430 --> 00:01:33,480
But yes what is also important is lazy loading will load the model only.

16
00:01:33,480 --> 00:01:41,100
Precisely when you are going to navigate to it so that there will be loading and that time there is

17
00:01:41,100 --> 00:01:46,710
a time that because of something incorrect error would come dead not at the start of.

18
00:01:46,800 --> 00:01:54,180
Of course that is not a major issue but yes lazy loading is all about delaying the loading of a more

19
00:01:54,180 --> 00:02:01,020
fuel to a later point of time precisely when it is needed.

20
00:02:01,020 --> 00:02:03,350
In other words On-Demand loading.

21
00:02:03,720 --> 00:02:09,330
Now I would like to demonstrate this whole thing with the proper example and I would like to do everything

22
00:02:09,330 --> 00:02:15,240
from ground up so that this becomes another example for you to understand how modules are developed.

23
00:02:15,240 --> 00:02:23,990
So first I'm going to have two models and get on the app for I will be more use than just for namesake

24
00:02:24,470 --> 00:02:30,130
one will call it lazy more do and the other I'm going to call it ego mode.

25
00:02:30,300 --> 00:02:30,750
Just

26
00:02:33,940 --> 00:02:35,720
you go more do you.

27
00:02:36,170 --> 00:02:38,320
Or what is this modules are supposed to have.

28
00:02:38,410 --> 00:02:41,790
Yes every model is supposed to have become part of a model file.

29
00:02:41,800 --> 00:02:45,010
So this are being hit and same thing can be had.

30
00:02:45,040 --> 00:02:47,200
And of course I'll rename it.

31
00:02:47,260 --> 00:02:58,420
So this is nothing but Eagle dot com and this is Eagle or more do likewise here also I would like to

32
00:02:58,420 --> 00:03:05,040
have lazy dog compliant and lazy lot more.

33
00:03:05,140 --> 00:03:12,820
You know first let us program Eagle component in Eagle.

34
00:03:12,940 --> 00:03:14,210
I'm going to write real

35
00:03:17,190 --> 00:03:19,660
simple text.

36
00:03:19,840 --> 00:03:24,660
This is a good company.

37
00:03:26,490 --> 00:03:31,380
That's a lot like that only in Lizy component also and simply put the text.

38
00:03:31,380 --> 00:03:33,110
This is the complex

39
00:03:37,960 --> 00:03:43,930
so eager compering is putting the biggest ego component and this is a lazy component.

40
00:03:43,930 --> 00:03:46,850
I mean we just don't see that the component is loaded.

41
00:03:46,900 --> 00:03:49,520
Rest of the stuff is all going to the main thing.

42
00:03:49,990 --> 00:03:56,300
And then of course egal model is that you modem I just need to make one team.

43
00:03:56,470 --> 00:04:06,320
That is it is ego of it and obviously the component used by Eagle can produce either.

44
00:04:06,880 --> 00:04:10,030
The this is your ego or something similar.

45
00:04:10,100 --> 00:04:13,950
Lindsay also is incompetent.

46
00:04:15,160 --> 00:04:18,490
And it also of course this is

47
00:04:21,070 --> 00:04:21,720
lazy

48
00:04:26,070 --> 00:04:30,560
and this is ego.

49
00:04:30,940 --> 00:04:36,360
These two components we want to make use of them in the component

50
00:04:39,040 --> 00:04:44,240
model so here I'm going to mention the model names losing more

51
00:04:47,280 --> 00:04:49,000
ego more.

52
00:04:49,350 --> 00:04:53,100
So obviously these two names we like to mention here also right.

53
00:04:53,100 --> 00:04:54,630
Not everything is equal.

54
00:04:54,780 --> 00:05:02,140
I'm going to attend this memorial to actual implementation as Naisi little later.

55
00:05:13,470 --> 00:05:13,920
And

56
00:05:26,440 --> 00:05:27,350
that is it.

57
00:05:30,850 --> 00:05:31,510
So

58
00:05:34,520 --> 00:05:36,950
Lizzie what is that.

59
00:05:37,670 --> 00:05:38,220
Good morning.

60
00:05:38,250 --> 00:05:39,120
OK class names.

61
00:05:39,120 --> 00:05:41,750
I did not take it.

62
00:05:41,750 --> 00:05:43,750
Bootstrap is not required here.

63
00:05:46,330 --> 00:05:48,930
This is ego call it

64
00:05:52,460 --> 00:05:53,860
you got more you

65
00:06:00,870 --> 00:06:02,730
last names are not changed.

66
00:06:04,210 --> 00:06:05,470
Same thing here.

67
00:06:09,330 --> 00:06:09,790
The

68
00:06:17,120 --> 00:06:20,140
brilliant ones see that there are no competition in.

69
00:06:21,480 --> 00:06:28,790
Is you cannot find a competent This isn't lazy or lazy morning rather in Bootstrap

70
00:06:32,630 --> 00:06:34,530
like that also.

71
00:06:35,700 --> 00:06:37,600
Yeah global did

72
00:06:41,580 --> 00:06:42,100
of it.

73
00:06:42,170 --> 00:06:49,830
No compulsion to run it and see that we do not have any runtime errors also of it.

74
00:06:49,970 --> 00:06:59,680
No I blew up a component and here I would like to add the links so is the entire

75
00:07:02,710 --> 00:07:10,040
college doesn't roll out link it will do we'll call it the eagle

76
00:07:15,050 --> 00:07:16,710
you component.

77
00:07:25,800 --> 00:07:34,890
And then I have one more that is lazy lazy.

78
00:07:35,850 --> 00:07:54,730
And here I would like to display the 0 so so I'm going to use an old ugly.

79
00:07:55,060 --> 00:07:59,470
We have to provide the whole thing on the road component right.

80
00:07:59,540 --> 00:08:00,010
Right.

81
00:08:00,020 --> 00:08:02,740
Let us say in up

82
00:08:08,210 --> 00:08:11,850
app door building dord.

83
00:08:12,070 --> 00:08:13,110
Yes.

84
00:08:13,300 --> 00:08:19,280
So a certain standard basic things for us first we need to do some more.

85
00:08:19,790 --> 00:08:35,290
One is more new with a wide doors from a large slice called

86
00:08:42,190 --> 00:08:46,110
we need ropes around more.

87
00:08:46,220 --> 00:08:47,480
These are in older

88
00:08:52,190 --> 00:09:04,400
obviously after this I need to create the constant ropes of ropes and it is supposed to be on the inside

89
00:09:04,430 --> 00:09:04,710
out.

90
00:09:04,740 --> 00:09:14,470
We need an old object bop call for on that as it is ego.

91
00:09:15,130 --> 00:09:21,440
And I would like to map this to component eco component

92
00:09:27,040 --> 00:09:27,550
of

93
00:09:30,760 --> 00:09:34,670
give your lazy.

94
00:09:34,780 --> 00:09:35,240
This is

95
00:09:38,090 --> 00:09:40,650
so you or I need to know more.

96
00:09:40,700 --> 00:09:44,640
Both of them import.

97
00:09:45,830 --> 00:09:48,480
No I should not import rather this box.

98
00:09:48,480 --> 00:09:52,200
Also I prefer taking it from the child.

99
00:09:52,910 --> 00:09:54,900
So this is empty.

100
00:09:55,010 --> 00:10:07,110
This is empty but an image may create an export constant rolling with a more fuel with a wider equal

101
00:10:07,430 --> 00:10:11,230
or older more fuel rod for who.

102
00:10:11,240 --> 00:10:12,610
Because this is the model.

103
00:10:12,780 --> 00:10:17,630
And this time I'm going to get ropes.

104
00:10:18,220 --> 00:10:19,520
So let me copy this here.

105
00:10:19,540 --> 00:10:22,810
I'll cut and to the whole this whole thing.

106
00:10:22,810 --> 00:10:29,020
I'll copy for you good morning will have its own little thing.

107
00:10:29,410 --> 00:10:36,450
So I have an eagle or rotating door daz

108
00:10:41,610 --> 00:10:44,190
and only good pop and.

109
00:10:44,320 --> 00:10:46,730
Is not a lot anything else.

110
00:10:47,380 --> 00:10:49,890
Yes eagle can make you look the eagle component

111
00:11:07,030 --> 00:11:09,330
supposedly blogs last

112
00:11:17,290 --> 00:11:24,170
does rote thing which is exported here I will use it in eager Orting

113
00:11:26,980 --> 00:11:31,930
eager model which will use common words you and I wrote.

114
00:11:32,220 --> 00:11:35,280
Actually we don't need the common model Cliton because none of the

115
00:11:38,100 --> 00:11:40,470
better tools are being used anywhere.

116
00:11:41,850 --> 00:11:44,400
So import

117
00:11:46,530 --> 00:11:49,320
Roding from

118
00:11:52,020 --> 00:11:52,440
the

119
00:11:55,590 --> 00:12:09,220
like that same thing I do for lazy so eager old thing I'll create a copy paste it in the file name

120
00:12:12,790 --> 00:12:15,180
and place it on thing.

121
00:12:15,290 --> 00:12:18,300
This is.

122
00:12:18,320 --> 00:12:18,940
This is

123
00:12:23,740 --> 00:12:24,470
that's it.

124
00:12:32,850 --> 00:12:37,100
Let's see no end up roasting.

125
00:12:37,590 --> 00:12:40,000
We just don't need this.

126
00:12:40,620 --> 00:12:48,320
Rather what we'll do is we'll keep him keep up and when to sort it out.

127
00:12:48,340 --> 00:12:51,670
Talk to me.

128
00:12:51,720 --> 00:12:52,550
Let's say good

129
00:12:57,490 --> 00:12:58,430
Bob.

130
00:12:58,540 --> 00:12:59,740
Is it called

131
00:13:04,880 --> 00:13:06,350
that is only going a pretty good

132
00:13:10,460 --> 00:13:10,950
end here.

133
00:13:10,950 --> 00:13:12,100
Also we have rotating

134
00:13:21,740 --> 00:13:26,440
when I'm supposed to include in our Or do

135
00:13:43,500 --> 00:13:45,800
the.

136
00:13:46,080 --> 00:13:46,590
That's it.

137
00:13:46,600 --> 00:13:55,890
Now let us run this and see if we should not get any.

138
00:13:56,310 --> 00:14:02,820
So by default we go to the easy it's not working.

139
00:14:02,820 --> 00:14:12,800
Rather it is something that all took up if there are any errors made errors cannot muster the old lazy

140
00:14:13,710 --> 00:14:20,500
good or might don't.

141
00:14:20,660 --> 00:14:25,480
So where is lazy roping

142
00:14:28,270 --> 00:14:32,660
not here you're supposed to replace with would.

143
00:14:32,710 --> 00:14:33,580
That's a mistake.

144
00:14:33,620 --> 00:14:35,500
And same thing of course in ego.

145
00:14:35,720 --> 00:14:37,310
It should not be for sure.

146
00:14:37,320 --> 00:14:39,940
Before I end up each of the mornings

147
00:14:46,740 --> 00:14:49,010
still not working.

148
00:14:51,160 --> 00:14:51,890
Not much.

149
00:14:51,930 --> 00:14:54,400
Any Don't you often segment lazy

150
00:15:01,470 --> 00:15:04,420
Larry.

151
00:15:04,420 --> 00:15:09,120
What is the problem with lazy or being lazy.

152
00:15:09,120 --> 00:15:12,630
This road thing he'd been in is immoral.

153
00:15:12,940 --> 00:15:15,190
Oh this is the problem.

154
00:15:15,340 --> 00:15:16,750
You we want to get really sick.

155
00:15:20,370 --> 00:15:32,720
So in all losing it or dying from it all this is actually taken care of in Eagle

156
00:15:37,090 --> 00:15:38,340
Yeah.

157
00:15:38,930 --> 00:15:41,800
I've not got the same thing getting lazy.

158
00:15:41,930 --> 00:15:42,530
That's why the

159
00:15:45,480 --> 00:15:48,490
Naisi don't think it's just.

160
00:15:51,140 --> 00:15:54,840
That's a no excuse.

161
00:15:54,870 --> 00:16:04,610
This and we should be able to switch between the comparably component but right no only by the name.

162
00:16:04,610 --> 00:16:08,110
This is lazy but there are other ways either right.

163
00:16:08,130 --> 00:16:08,670
No.

164
00:16:08,990 --> 00:16:19,940
So what are we actually going to make a lazy component lazy for that we go to our probing over here.

165
00:16:19,940 --> 00:16:31,250
We're going to add one more but let's say that this policy is that while you're not supposed to you

166
00:16:32,350 --> 00:16:38,200
call up Lord Chiltern.

167
00:16:38,420 --> 00:16:44,000
The value in children is supposed to be at the bottom of the Lizy more Dhume.

168
00:16:44,400 --> 00:16:54,690
So Dr. slice is the more you slice lazy dog.

169
00:16:55,750 --> 00:17:02,780
That means despite an inductive fight we want to look older lazy more.

170
00:17:03,100 --> 00:17:04,440
That's how we have to get to

171
00:17:08,320 --> 00:17:11,580
know eager is going to last eagerly.

172
00:17:11,730 --> 00:17:19,900
But as Naisi more is going to load them on only when I click on the link Naisi it is going to be loaded

173
00:17:20,170 --> 00:17:21,960
otherwise it will not be loaded.

174
00:17:22,110 --> 00:17:24,760
First let's see if we are getting any different output.

175
00:17:28,800 --> 00:17:30,200
Lazy.

176
00:17:30,700 --> 00:17:31,970
There's no difference in the output

177
00:17:34,920 --> 00:17:40,400
but the main point is houseflies proved that the lazy model is not loaded.

178
00:17:40,410 --> 00:17:43,130
It's tough to see right now.

179
00:17:43,170 --> 00:17:51,650
If I go to upcourt you here we are mentioning Buthelezi more than an entire morning if Ballymore Lindsay

180
00:17:51,700 --> 00:17:56,770
my brother will be more vocal and we'll see that for eager muddle along.

181
00:17:56,780 --> 00:18:07,790
We are going to get at her and thought Naisi more than we are not going to get.

182
00:18:07,940 --> 00:18:15,050
It's not showing any at all but at the same time the other component you know cannot master Odigo

183
00:18:21,510 --> 00:18:22,590
whereas lazy can

184
00:18:26,870 --> 00:18:29,550
is trying to load the file.

185
00:18:29,590 --> 00:18:31,750
OK here there is something wrong.

186
00:18:32,280 --> 00:18:34,570
No closed source is important.

187
00:18:34,570 --> 00:18:40,780
Trying to solve something is immoral whereas our lazy Mordieu is in

188
00:18:49,360 --> 00:18:56,020
it isn't so I'm what I'll do is I'll give Sly's a sarcy slice

189
00:18:59,820 --> 00:19:04,770
all that you've got class even that is fine.

190
00:19:10,470 --> 00:19:11,590
No I'm

191
00:19:16,880 --> 00:19:18,860
lazy incompetent little

192
00:19:28,960 --> 00:19:29,740
ego.

193
00:19:29,840 --> 00:19:32,230
It cannot last.

194
00:19:32,450 --> 00:19:34,370
So I'm supposed to let you go here.

195
00:19:38,420 --> 00:19:43,810
But lazy I don't want to get out of this lazy do in order dynamically on the

196
00:19:48,720 --> 00:19:49,660
so next.

197
00:19:49,710 --> 00:19:54,700
Is no.

198
00:19:54,960 --> 00:20:02,430
This is a good goal in law to get Conley's and

199
00:20:05,290 --> 00:20:08,020
it's not getting error but it's not even giving the

200
00:20:11,590 --> 00:20:15,580
nurse a run once again and see what exactly the problem.

201
00:20:16,810 --> 00:20:21,720
Either loading is working fine lady lazy loading.

202
00:20:21,760 --> 00:20:28,700
Here we are getting cut to only so OK you see we're getting nothing.

203
00:20:28,840 --> 00:20:30,330
That means lazy loading.

204
00:20:30,330 --> 00:20:34,770
We have some issues here.

205
00:20:35,190 --> 00:20:39,550
Let us understand first in our Roding

206
00:20:45,040 --> 00:20:49,040
if MTV is a good and the good will take care of the ego loading fine.

207
00:20:49,040 --> 00:20:55,750
If it is lazy it's taking lazy more endlessly more.

208
00:20:55,770 --> 00:21:03,490
We have a rotating reference to lazy loading lazy or a mistake.

209
00:21:03,810 --> 00:21:05,120
We have already given.

210
00:21:05,270 --> 00:21:06,090
There.

211
00:21:06,570 --> 00:21:09,860
So here we are not supposed to give anything blank.

212
00:21:10,650 --> 00:21:11,230
No.

213
00:21:11,370 --> 00:21:12,950
You see this box.

214
00:21:17,870 --> 00:21:22,280
The company compliant very important.

215
00:21:22,280 --> 00:21:24,620
So basically this becomes a kind of change.

216
00:21:24,910 --> 00:21:33,420
So if I want I can have one more maybe for example Demel it can be mapped to a demo component and just

217
00:21:33,410 --> 00:21:34,520
play it quickly.

218
00:21:35,690 --> 00:21:39,090
The whole component is a component component.

219
00:21:39,100 --> 00:21:48,460
I will have to only add it to the game copy and I'll call this demo component

220
00:21:53,800 --> 00:21:55,780
and this is Demo component

221
00:21:58,840 --> 00:22:03,560
demo component should be used in the corresponding module.

222
00:22:07,170 --> 00:22:13,860
Allowed lazy component and demo component component in blue that has to be done

223
00:22:17,160 --> 00:22:19,960
from door slides.

224
00:22:20,070 --> 00:22:22,380
They were confident.

225
00:22:22,660 --> 00:22:24,740
Now how should they provide the link.

226
00:22:24,780 --> 00:22:31,220
I'll go to OP component up component road component in this.

227
00:22:31,260 --> 00:22:32,600
Let's have one more.

228
00:22:32,600 --> 00:22:37,380
There is a component but no good lazy slice them.

229
00:22:37,590 --> 00:22:40,380
The bot is glazes slice them.

230
00:22:40,980 --> 00:22:48,380
So lazy is going to therefore do the whole modem and induct more.

231
00:22:48,680 --> 00:22:53,540
We are having rotating all rotating register under a road thing is saying

232
00:23:00,490 --> 00:23:06,550
when it is empty it goes to the lazy component when it is to go to the component

233
00:23:15,050 --> 00:23:20,750
that said let's run and see if we are getting the output from the component

234
00:23:26,370 --> 00:23:30,620
components last lazy incompetent to see it.

235
00:23:31,020 --> 00:23:33,840
So like these parts have to be mentioned.

236
00:23:34,880 --> 00:23:44,930
Very importantly I'm able to say it is lazy loading it for the simple reason that end up more we do

237
00:23:44,930 --> 00:23:49,650
not have the reference of the morning.

238
00:23:49,770 --> 00:23:55,350
Good morning references that but anything more than reference is not so lazy modem is going to be precisely

239
00:23:55,350 --> 00:24:01,600
loaded only at runtime on needed only basis on demand basis.

240
00:24:01,710 --> 00:24:08,660
That's the only difference between ego model and lazy more lazy more do we have to specify the pop in

241
00:24:08,660 --> 00:24:10,010
the whole thing.

242
00:24:10,170 --> 00:24:17,240
In this particular fashion as you go model we just do the routine way we have done it.

243
00:24:17,610 --> 00:24:19,530
That's all with this.

244
00:24:19,710 --> 00:24:20,520
Thank you.

