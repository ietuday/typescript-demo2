1
00:00:16,660 --> 00:00:28,240
Messengered or child ropes What exactly is nested or see so we know that in the hoop component or anywhere

2
00:00:28,240 --> 00:00:35,110
where we have used a router or play tag the content of the component based on the part is going to be

3
00:00:35,110 --> 00:00:43,320
rendered but now we have a typical situation where the component is going to produce some old pull to

4
00:00:43,330 --> 00:00:49,560
our template and that it has its own outlet.

5
00:00:51,240 --> 00:00:53,570
So in this kind of situation.

6
00:00:53,640 --> 00:00:58,070
How can I ensure that the old is coming in the.

7
00:00:58,150 --> 00:01:02,750
Old older outlet and not in the older router.

8
00:01:03,690 --> 00:01:12,720
So here let's see first the same product that we had before we have your home component Home component

9
00:01:12,720 --> 00:01:15,720
right now it is displaying the basic text.

10
00:01:15,720 --> 00:01:19,740
I would like to replace this home component with something more.

11
00:01:19,740 --> 00:01:24,100
That is I would like to have let's say a little formatted output.

12
00:01:24,510 --> 00:01:25,210
So what are those.

13
00:01:25,260 --> 00:01:26,160
I'll go for one.

14
00:01:26,160 --> 00:01:29,180
David your class is equal to.

15
00:01:29,530 --> 00:01:32,320
Well some bootstrap

16
00:01:38,670 --> 00:01:44,810
them because it is multi line single called will not work to use

17
00:01:47,860 --> 00:01:51,300
the other.

18
00:01:51,750 --> 00:01:54,520
And yet I'll give you an

19
00:01:58,280 --> 00:01:59,780
go.

20
00:02:00,120 --> 00:02:04,350
This is just for reference.

21
00:02:05,420 --> 00:02:07,690
And I'm going to add one more

22
00:02:11,790 --> 00:02:15,170
important thing insert does do I'll get a.

23
00:02:15,300 --> 00:02:16,110
Oh Plett

24
00:02:25,210 --> 00:02:26,720
pro-drug.

25
00:02:27,430 --> 00:02:32,120
So no we are Okorocha that one we have it in

26
00:02:34,670 --> 00:02:39,280
my laptop team and so does the heading we're going to get.

27
00:02:39,330 --> 00:02:39,880
All right.

28
00:02:39,920 --> 00:02:45,630
When I click on it this rotary outlet is going to be used for display.

29
00:02:45,680 --> 00:02:49,150
So let me run this right now Latina's

30
00:02:57,580 --> 00:02:58,240
some kind of

31
00:03:01,570 --> 00:03:03,900
Dudack is not closed properly.

32
00:03:07,680 --> 00:03:08,470
Yes you're right.

33
00:03:08,530 --> 00:03:12,400
We have not closed headwater not.

34
00:03:12,470 --> 00:03:25,120
And once again the only thing I have made up is in the home I added text Gudu employs the text is gone

35
00:03:26,050 --> 00:03:30,460
home has this but employees doesn't have department doesn't have.

36
00:03:30,730 --> 00:03:37,810
And if something is not found even that doesn't have no my requirement is I want that section C right

37
00:03:37,830 --> 00:03:38,970
now what is happening.

38
00:03:38,980 --> 00:03:47,120
Employees Department all are using Oh not in this roter but Im sorry you are not in the home roador

39
00:03:47,140 --> 00:03:56,020
outlet but in my app application daughter there is a group on the route componentry help.

40
00:03:56,130 --> 00:04:02,180
There they are producing just tool so that this is

41
00:04:09,190 --> 00:04:09,850
outlet

42
00:04:18,730 --> 00:04:19,540
employees

43
00:04:24,000 --> 00:04:34,870
that text is not coming because of the casing run in debug mode and it works.

44
00:04:34,890 --> 00:04:39,450
This is our program at home.

45
00:04:39,560 --> 00:04:41,530
Here we have this.

46
00:04:41,650 --> 00:04:46,850
So my requirement is what the new open to come below that is here.

47
00:04:47,040 --> 00:04:51,490
Below is the employee that is should come below.

48
00:04:51,490 --> 00:04:52,070
Welcome.

49
00:04:52,070 --> 00:04:53,860
This is homepage.

50
00:04:53,930 --> 00:04:56,530
How are we going to get that.

51
00:04:56,540 --> 00:05:01,000
That means we want to make use of in-home this.

52
00:05:01,140 --> 00:05:05,760
This is what we want to make use of how to do that.

53
00:05:05,830 --> 00:05:08,400
Loading is what we will have Batanes now.

54
00:05:08,590 --> 00:05:16,490
So this olds table which we have and just copy come into this and create a new local people.

55
00:05:17,050 --> 00:05:25,130
And here I'll see if the pot is empty or maybe we want to make use of home component will end.

56
00:05:25,660 --> 00:05:32,950
But then in home component now I'm going to provide extra property one more property we are going to

57
00:05:32,950 --> 00:05:36,250
deploy that is tilted.

58
00:05:36,680 --> 00:05:45,050
No I'll give it to them and intill them I'll provide an area off the ropes and that area of roads Ulmo.

59
00:05:45,220 --> 00:05:48,710
Let's say specially employed only only then prosection unwinking

60
00:05:54,730 --> 00:05:55,470
said.

61
00:05:55,600 --> 00:06:00,210
So what I did know employee an employee with ID.

62
00:06:00,280 --> 00:06:06,790
I've made it and it tailed off all the rest Department is still a child of the room.

63
00:06:07,750 --> 00:06:16,320
So who is what this one with us home is this home component has this.

64
00:06:16,510 --> 00:06:20,820
So we'll see how the employee is going to come here.

65
00:06:28,440 --> 00:06:31,590
Welcome to the formal Department.

66
00:06:31,590 --> 00:06:37,660
It is still here only but when I click on employees bestowing it.

67
00:06:37,710 --> 00:06:39,970
OK sorry small mistake.

68
00:06:40,350 --> 00:06:42,920
This box should be empty.

69
00:06:43,230 --> 00:06:44,630
And this part is not required.

70
00:06:44,640 --> 00:06:45,240
I'll come in.

71
00:06:45,390 --> 00:06:47,510
I'll explain why.

72
00:06:48,110 --> 00:06:56,560
So default is not the whole component under the ENP is going to be split the.

73
00:07:01,590 --> 00:07:09,810
You see in places coming here which is under legal department will directive come in and put in place

74
00:07:09,910 --> 00:07:10,920
X and it's coming

75
00:07:13,650 --> 00:07:22,620
in place of this so what did you know see by default.

76
00:07:22,620 --> 00:07:28,190
MT is there for MT aspecific 10 Ben parties.

77
00:07:28,220 --> 00:07:36,230
MT We also know that we can use real data but remember with empty pot and children we cannot also use

78
00:07:36,260 --> 00:07:40,360
rhetoric that means either to confuse or children can be used.

79
00:07:40,780 --> 00:07:43,710
So when the pot is empty we are saying that

80
00:07:46,160 --> 00:07:57,590
slice MP will take me to MP component but if the component is going to appear under the rotor outlet

81
00:07:57,670 --> 00:08:06,170
of home component Department component fine not one component is going to render output under the Department

82
00:08:06,230 --> 00:08:09,930
under the older outlet of the room.

83
00:08:11,730 --> 00:08:13,050
Not under the table.

84
00:08:13,650 --> 00:08:19,770
So like this you can move the content from room to the child of a particular component.

85
00:08:19,830 --> 00:08:24,560
That's all of course if you want dept. also to be told this.

86
00:08:24,620 --> 00:08:27,520
Also inside.

87
00:08:27,740 --> 00:08:36,430
So now what will happen is department content also will show under home component.

88
00:08:36,680 --> 00:08:39,160
So this becomes consistent for everything

89
00:08:42,020 --> 00:08:45,180
hole is not found anything way when it's not found.

90
00:08:45,560 --> 00:08:54,000
This home refers to what you need to first understand that home refers to whole but is not there.

91
00:08:54,470 --> 00:08:55,720
So what we will do.

92
00:08:55,960 --> 00:08:57,790
We can actually create new but

93
00:09:10,340 --> 00:09:14,480
it is rare for us to hold on and

94
00:09:21,110 --> 00:09:25,960
doesn't.

95
00:09:26,340 --> 00:09:30,240
So by default we get the content from home component.

96
00:09:30,240 --> 00:09:32,210
This also gets the same thing.

97
00:09:32,220 --> 00:09:36,410
Does this motor thing.

98
00:09:36,620 --> 00:09:44,690
So this is how we know we have a child having its own router outlet we can redirect the output of a

99
00:09:44,690 --> 00:09:50,170
particular component to it by sending the road and adding children.

100
00:09:50,210 --> 00:09:51,920
Likewise if suppose by not phone.

101
00:09:51,920 --> 00:09:59,420
Also if you want to bring it under home component that also it.

102
00:09:59,500 --> 00:10:09,310
So if I click on some part that is not found even for that the header will be available see the requested

103
00:10:09,310 --> 00:10:10,500
file is not existing.

104
00:10:15,450 --> 00:10:25,770
So nesting off roads gives you the ability to use a router outlet inside the rotor outlet but for the

105
00:10:25,770 --> 00:10:31,060
content to be delivered inside the inner or outer outlet.

106
00:10:31,600 --> 00:10:38,840
You love use children in the know that all object to that children.

107
00:10:39,050 --> 00:10:39,940
That's all it does.

108
00:10:40,100 --> 00:10:40,530
Thank you.

