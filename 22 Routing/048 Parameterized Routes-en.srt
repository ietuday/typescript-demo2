1
00:00:16,580 --> 00:00:19,930
In this session I'm going to take more topics.

2
00:00:19,930 --> 00:00:28,270
One is refactoring the Roding configuration into or more do what we have done it was a configuration

3
00:00:28,390 --> 00:00:34,810
that was in app got a routing got to guess that I'm going to convert it properly being more do.

4
00:00:34,810 --> 00:00:36,820
That is one thing we are going to do.

5
00:00:37,120 --> 00:00:41,290
And the other thing we are going to look at is a rotating paramita.

6
00:00:41,320 --> 00:00:46,950
That means we can pass data from a link to component.

7
00:00:47,260 --> 00:00:52,670
So that component can do some kind of penetration and make that data visible in the view.

8
00:00:52,810 --> 00:00:58,710
In other words we can pass data from one view to other other views through politics.

9
00:00:58,780 --> 00:01:04,070
So first what is this role model you see right now in my project.

10
00:01:04,110 --> 00:01:11,620
I've got dot dot B.S. And here we are creating that Orting model with provider which we're getting using

11
00:01:11,620 --> 00:01:21,030
role model for road stable this whole object is what we have in our body that is all here.

12
00:01:21,330 --> 00:01:22,750
I want to replace that no.

13
00:01:24,050 --> 00:01:27,690
I will replace it with a problem more fuel here.

14
00:01:28,160 --> 00:01:31,250
That is I want to make this Indy more.

15
00:01:31,260 --> 00:01:40,640
So first thing I do is import more fuel from it.

16
00:01:42,350 --> 00:01:46,420
Anglo's less common than the first.

17
00:01:47,380 --> 00:01:53,760
Second thing in Denmark Bune study and learn more common core.

18
00:01:53,800 --> 00:01:56,320
Second thing I would like to do is NOT write right.

19
00:01:56,350 --> 00:01:59,890
Klaus export.

20
00:02:00,430 --> 00:02:02,970
I would like to say things.

21
00:02:05,040 --> 00:02:13,250
More you probably even call it that are providing more to you and I'm going to provide it.

22
00:02:15,020 --> 00:02:23,290
And the more you and your we will do some kind of export and import generally if we do something similar

23
00:02:23,290 --> 00:02:26,160
yet you see we do import right.

24
00:02:26,240 --> 00:02:30,760
Also we are not going to look import but what are we importing now.

25
00:02:30,830 --> 00:02:33,150
We would like to import broking.

26
00:02:33,410 --> 00:02:38,030
Either we want to take the whole thing here that is fine or else

27
00:02:42,040 --> 00:02:48,340
export class or as I would probably also want to write this.

28
00:02:48,340 --> 00:02:49,830
Even that is fine.

29
00:02:50,140 --> 00:02:56,240
They can handle any imports we have given older model for growth and so on.

30
00:02:56,240 --> 00:03:09,380
So and second thing is I would like to do some kind of export what is the export load more so it is

31
00:03:09,640 --> 00:03:11,460
very good I'm importing this.

32
00:03:11,590 --> 00:03:16,180
I don't have to explicitly import this automatically to export it.

33
00:03:16,640 --> 00:03:24,700
So approaching more you know you're just uploading more and it's appropriate and we are also approaching

34
00:03:24,760 --> 00:03:26,220
the.

35
00:03:26,280 --> 00:03:27,410
That's it.

36
00:03:27,590 --> 00:03:31,740
No other tinct does that from the rotating configuration.

37
00:03:31,840 --> 00:03:39,640
We know it is a prop or a rotating more Dhume that the only difference and this is a more better and

38
00:03:39,670 --> 00:03:41,570
standard way of doing it.

39
00:03:42,680 --> 00:03:44,820
And are we getting any different result.

40
00:03:44,840 --> 00:03:45,880
Let's run and see.

41
00:03:45,890 --> 00:03:51,650
There should not be any error and all the links which we created or use would work as it does not hold

42
00:03:52,190 --> 00:03:56,470
your potman one all fine perfect.

43
00:03:56,540 --> 00:03:58,160
So this is one part.

44
00:03:58,310 --> 00:04:03,910
The other thing which I would like to cover here is parametrized ropes.

45
00:04:04,140 --> 00:04:08,620
So right now my role model is having employees.

46
00:04:09,170 --> 00:04:20,540
Now what I would like to do is you know an employee component employee got Steimle I would like to add

47
00:04:21,080 --> 00:04:22,000
one more call.

48
00:04:22,020 --> 00:04:32,210
Let's say Didi's when I click on these details I would like to have details of that particular employee.

49
00:04:32,470 --> 00:04:39,110
So what I would like to know is here provide all ATAC but refer to them.

50
00:04:39,160 --> 00:04:49,980
I would like to roll them to Coulter link and the value of the Rotary link is supposed to be on getting

51
00:04:50,470 --> 00:04:51,960
basically a day off at 11.

52
00:04:51,990 --> 00:04:57,770
But the first part of me that I'll give is but generally don't darling we simply give up.

53
00:04:57,820 --> 00:05:05,050
But right now I would like to give part of the part I would like to pass some parameters which is nothing

54
00:05:05,050 --> 00:05:09,890
but current ENP ENP and that's it.

55
00:05:10,060 --> 00:05:12,930
So bought this sliced ENP that is insane.

56
00:05:13,160 --> 00:05:18,310
So constant on here I lose the M.B.A. the same.

57
00:05:18,320 --> 00:05:22,570
MP I'm going to parse it does extrapyramidal to this pop.

58
00:05:22,830 --> 00:05:26,960
So that's all I'm framing on Alcibiades.

59
00:05:27,240 --> 00:05:34,240
So it's going to be that next which when I click will take me to slice MP slice one slice piece last

60
00:05:34,310 --> 00:05:41,200
two slots lasting peace last three and so not on this one to see if that is correct.

61
00:05:42,050 --> 00:05:46,470
First I go to implies we haven't caught that.

62
00:05:46,470 --> 00:05:56,290
So basically the problem is it's D.M. file is not getting the fullest so I'd better run in debug mode.

63
00:06:04,000 --> 00:06:09,570
Employs lawyers it really does look at those details but that is important here.

64
00:06:09,580 --> 00:06:11,740
Look at your slides.

65
00:06:11,980 --> 00:06:18,890
This last one plus two plus treats class would like that we are getting in on this.

66
00:06:19,520 --> 00:06:21,500
The party is not existing.

67
00:06:21,500 --> 00:06:27,470
It's taking with my phone to the parties not existing What we have when these class employee but last

68
00:06:27,500 --> 00:06:33,330
in the last two is not the way we hope they will it does not have a mention of it.

69
00:06:33,770 --> 00:06:42,340
So I will not be sliced to Hobel specify simply put your column and some kind of variable say 80 or

70
00:06:42,330 --> 00:06:44,040
your colleagues in here.

71
00:06:45,110 --> 00:06:50,320
So not this MP 80 will go in to employ component.

72
00:06:50,450 --> 00:06:56,570
So now I'm going to make changes to the employee component so that we can read the value of that employer

73
00:06:56,660 --> 00:06:58,130
employee company.

74
00:06:58,700 --> 00:07:05,470
So for this we have to get first the reference to activity around so employer component

75
00:07:12,480 --> 00:07:15,600
employee component unposed.

76
00:07:15,770 --> 00:07:25,880
I think activated all the same thing we're going to get as injected object into the constructor.

77
00:07:26,200 --> 00:07:29,740
That we got activated around this activity.

78
00:07:29,840 --> 00:07:36,120
We can use it for getting the battery to do that.

79
00:07:36,810 --> 00:07:39,170
So here you see we are indeed.

80
00:07:39,250 --> 00:07:46,910
And so this code I'm going to right now I'll explain in Anandi in it.

81
00:07:47,170 --> 00:07:47,980
So first

82
00:07:51,130 --> 00:07:52,180
Andy and

83
00:07:59,150 --> 00:08:03,290
Leaman's on the

84
00:08:12,600 --> 00:08:16,860
pro-White and the

85
00:08:29,470 --> 00:08:35,010
misdeal and what is actually happening this rope which is activated.

86
00:08:35,120 --> 00:08:43,800
I got the reference here and that is going to give me Byrons biomes is an observable of de-value which

87
00:08:44,150 --> 00:08:52,460
as you can see that Bottoms is observable and inside that it is an observable of Kiraly bed.

88
00:08:52,850 --> 00:08:56,070
So on that observable we're calling subscribed.

89
00:08:56,600 --> 00:09:02,340
So those give a new pair one by one we're going to be in two bottoms off.

90
00:09:02,510 --> 00:09:03,780
No what is this key.

91
00:09:03,890 --> 00:09:12,310
This is supposed to be that imperially which we gave that as a main name this name which we have given

92
00:09:12,340 --> 00:09:20,410
in the pop this one.

93
00:09:20,910 --> 00:09:28,210
So if bottoms of M.B.A. does not equal we would like to send the selected employee.

94
00:09:28,350 --> 00:09:35,450
That means we should have no self employed by any form.

95
00:09:35,580 --> 00:09:46,350
Of course this type employee or let's say we can make it does not.

96
00:09:46,360 --> 00:09:48,120
And also I would like to have

97
00:09:50,890 --> 00:09:51,720
bottoms.

98
00:09:51,760 --> 00:09:55,490
So basically we do the subscription.

99
00:09:55,500 --> 00:10:03,850
So we simply give any one of that is here we are subscribing it and later we will know the subscription

100
00:10:03,850 --> 00:10:09,470
offer for which I'll handle on indeed destroying.

101
00:10:09,680 --> 00:10:18,990
So you're not on the fly with implements on this blog.

102
00:10:19,760 --> 00:10:21,440
So indeed on this point

103
00:10:24,500 --> 00:10:42,090
plan in India on this flight to the medaled I'll say this does not items on unsubscribe.

104
00:10:42,370 --> 00:10:47,890
And here we are subscribing your we are unsubscribing now.

105
00:10:47,910 --> 00:10:49,280
What is the issue.

106
00:10:49,280 --> 00:10:50,320
What is the point.

107
00:10:50,440 --> 00:10:54,660
Somewhere in my view I should display selected employee.

108
00:10:55,000 --> 00:11:06,310
So here the corresponding view is employed at the moment imploded Stephen I could under the table maybe

109
00:11:06,310 --> 00:11:17,540
some heads up and then I'm hitting and employee meetings

110
00:11:20,200 --> 00:11:25,500
no we should say need him.

111
00:11:25,630 --> 00:11:28,550
So how do we get him to be named.

112
00:11:28,870 --> 00:11:44,640
So let the employee door EAB name likewise you can probably put other Didion's also name salary ID so

113
00:11:44,670 --> 00:11:47,210
you can dig the name salary

114
00:11:52,530 --> 00:11:54,680
cylinder employee door.

115
00:11:55,820 --> 00:12:03,070
I mean selling something you can probably do with the department also.

116
00:12:03,240 --> 00:12:05,010
So let me show you old first

117
00:12:08,060 --> 00:12:14,930
undeliverable terms that we are going to get at the mall when I go to employees.

118
00:12:15,150 --> 00:12:18,580
But we are not getting any done on this

119
00:12:21,570 --> 00:12:22,520
lot coming.

120
00:12:22,780 --> 00:12:24,660
So something should have gone wrong.

121
00:12:27,390 --> 00:12:27,990
OK.

122
00:12:28,160 --> 00:12:30,860
I forgot the main call in employee component

123
00:12:34,640 --> 00:12:36,660
your penetration is done.

124
00:12:36,710 --> 00:12:38,120
No problem.

125
00:12:38,330 --> 00:12:45,730
You can actually put a break point here and see a selected employee selected employee is correct or

126
00:12:46,010 --> 00:12:55,390
used in selected employee or selected employee errors here.

127
00:12:56,820 --> 00:12:57,330
Is it called

128
00:13:02,020 --> 00:13:09,610
you know ID is equal to ENP not only that but you're not on it.

129
00:13:16,230 --> 00:13:25,640
Do employees details fantastic it's working.

130
00:13:25,710 --> 00:13:27,430
So how are we getting this.

131
00:13:27,750 --> 00:13:33,460
No let me explain once again what I have done first year here.

132
00:13:33,630 --> 00:13:39,930
I have created sort of Norty of basically that in an probing and operating.

133
00:13:39,930 --> 00:13:47,970
I created a new product in the new pop arena we had only employee mapping to employee component but

134
00:13:48,160 --> 00:13:48,710
this.

135
00:13:48,750 --> 00:13:52,610
Also mapping to employee component on the.

136
00:13:53,100 --> 00:13:58,110
So here I have that extra up paramita MP mph.

137
00:13:58,770 --> 00:14:07,700
Remember if needed you can also pass one more extra paramita maybe something like call an BPT and let

138
00:14:07,750 --> 00:14:10,550
us say for whatever reason I would like to.

139
00:14:10,980 --> 00:14:12,710
I can do that also.

140
00:14:13,170 --> 00:14:23,690
But then in my employee component here I'm supposed to have one more value but there is only one part

141
00:14:23,690 --> 00:14:24,080
of it.

142
00:14:24,090 --> 00:14:31,040
So that link can be mentioned like this but when we have multiple baramin done we mention it in such

143
00:14:31,050 --> 00:14:31,870
case.

144
00:14:32,100 --> 00:14:41,270
Let me just controls he can talk to me and I'll probably come in off or do it for the time being sends

145
00:14:41,370 --> 00:14:42,720
Casey here.

146
00:14:42,950 --> 00:14:52,030
We are supposed to provide Oh object the value of Jambi 80 is equal to him.

147
00:14:52,120 --> 00:15:03,890
In the Camargue the value of DVDA he is equal to do we have impede or do we use department or department

148
00:15:04,370 --> 00:15:10,380
like this we will have to give so not Bill but it is going to be bossed

149
00:15:13,560 --> 00:15:24,060
Let's run this and see first darling no has burn parameters when I go to employ the recall details.

150
00:15:25,820 --> 00:15:29,560
Oh something is wrong.

151
00:15:29,780 --> 00:15:38,540
Up in eighty one it's working but karma is coming.

152
00:15:38,540 --> 00:15:42,920
I want to live in a little different format my smoking.

153
00:15:43,160 --> 00:15:45,040
But we wanted to do different forms.

154
00:15:45,050 --> 00:15:53,510
We wanted to slice these last two like that we wanted so we want multiple Parminter rather than passing

155
00:15:53,510 --> 00:15:55,120
it as a single object.

156
00:15:55,130 --> 00:16:01,680
Right now this is being passed as an object which has MPAC and department ideas to properties.

157
00:16:01,790 --> 00:16:07,320
I don't want to pass it as an object not argue simply.

158
00:16:07,560 --> 00:16:18,960
I would replace this with the second part of that is we don't you know be 80 will be or department

159
00:16:21,410 --> 00:16:23,060
door DVD

160
00:16:25,570 --> 00:16:33,040
so we can give a number of parameters like this and each one of them will come and said in this particular

161
00:16:33,040 --> 00:16:33,570
location

162
00:16:35,880 --> 00:16:43,360
in a row being yes he had like this it can go next you know

163
00:16:48,150 --> 00:16:56,610
department pick him slice one plus one let's talk to him and let me tell you.

164
00:16:57,950 --> 00:17:02,570
Sorts of toys in which format you want that you are in that particular format.

165
00:17:02,580 --> 00:17:09,560
We love to provide the detail in employee component.

166
00:17:09,610 --> 00:17:17,020
You have to be clear about this but same thing is all mentioned in the hand and you see employees lies.

167
00:17:17,030 --> 00:17:20,740
The department also can do the same thing.

168
00:17:20,840 --> 00:17:23,300
You have to use activated road.

169
00:17:23,450 --> 00:17:28,310
We get de-activated or does paramita by default selected employees.

170
00:17:29,140 --> 00:17:36,880
We have employee details and in on it we are doing some kind of initialization.

171
00:17:36,970 --> 00:17:45,520
Red bottoms is going to get me this dog robot Bottoms is going to get me an observable of P-value bed

172
00:17:46,680 --> 00:17:52,020
based on the key which is supposed to be irony as mentioned in the book.

173
00:17:53,120 --> 00:17:59,450
We are going to get the value if there is a record of that particular position.

174
00:17:59,520 --> 00:18:06,550
They are getting selected employee on filtering and once we have selected employee selected employee

175
00:18:06,550 --> 00:18:09,280
that is we are trying to render all here.

176
00:18:09,800 --> 00:18:14,480
Yes if you want we can put this conditionings top like your

177
00:18:17,560 --> 00:18:21,890
all in.

178
00:18:22,570 --> 00:18:29,110
So if there is no selected employee that whole section will be like first time we will not have any

179
00:18:29,110 --> 00:18:32,630
Seliger employee so will hide the whole section.

180
00:18:32,720 --> 00:18:35,970
But here when they click when you get to select an employee it takes

181
00:18:40,130 --> 00:18:41,910
in the selected employee.

182
00:18:41,930 --> 00:18:46,110
It's not equal.

183
00:18:46,450 --> 00:18:50,620
And of course how do we provide the link.

184
00:18:50,680 --> 00:18:59,140
There are two ways we can simply impede or the MPAA ECOMOG or department or department if you have multiple

185
00:18:59,140 --> 00:19:07,290
parameters keep providing comments updated list or if you want to pass a single object like this from

186
00:19:07,290 --> 00:19:09,500
door to door so we can delete them.

187
00:19:09,540 --> 00:19:11,230
Either way it is interesting.

188
00:19:13,030 --> 00:19:19,610
So that's all about how we can possibly talk to the Roebling which can be used in the component for

189
00:19:19,610 --> 00:19:20,660
filtration.

190
00:19:20,740 --> 00:19:21,170
Thank you.

