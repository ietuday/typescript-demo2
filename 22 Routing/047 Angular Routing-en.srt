1
00:00:16,530 --> 00:00:25,470
Angular roping a lot of any common features of angular is developing single piece application or what

2
00:00:25,470 --> 00:00:32,780
is a single page application from what we are going to get a response of only one request.

3
00:00:32,850 --> 00:00:34,530
Let's say one Babet.

4
00:00:34,860 --> 00:00:42,260
And in the same bed we are not going to display the views of different components.

5
00:00:42,360 --> 00:00:46,590
That means we are not going to make our trips to the server.

6
00:00:47,320 --> 00:00:53,220
We are simply tending from one view to another you and on the same page.

7
00:00:53,250 --> 00:00:55,530
The content will be displayed.

8
00:00:55,830 --> 00:00:57,190
How can we achieve this.

9
00:00:57,300 --> 00:01:06,430
Precisely that is what we are going to do using ropin and pro-white angular slash Rohter library.

10
00:01:06,450 --> 00:01:15,060
This is important roter library is used for enabling routing in our application routing is used to navigate

11
00:01:15,060 --> 00:01:17,430
from one view to another.

12
00:01:17,670 --> 00:01:23,250
When a user performs and it does not work and with that task maybe a click of a button.

13
00:01:23,900 --> 00:01:25,910
Maybe because a hyperlink.

14
00:01:25,980 --> 00:01:27,830
Or it can be any other cause.

15
00:01:28,760 --> 00:01:35,990
So where we want to simply jump from one view to another view and obviously both the views may belong

16
00:01:36,080 --> 00:01:38,650
to two different components.

17
00:01:38,690 --> 00:01:45,770
Yes it is also possible that at the time of jumping from one view to another view that means actually

18
00:01:45,770 --> 00:01:48,860
speaking it is one component or another component.

19
00:01:48,870 --> 00:01:55,850
We passed some optional parameters so that in the other view some kind of filtration can be done and

20
00:01:55,940 --> 00:02:01,890
or can be displayed so that we will see practically it later.

21
00:02:02,150 --> 00:02:08,860
But what is very important here is we have a road link router link active router outlet.

22
00:02:08,880 --> 00:02:16,170
These are the three directives which are going to be used in and galop to our older model Pakki.

23
00:02:16,460 --> 00:02:22,890
So yes in our end the more do we will have to import a router module packet.

24
00:02:23,090 --> 00:02:25,170
So this is how the whole thing works.

25
00:02:25,350 --> 00:02:27,590
Just broad overview here.

26
00:02:27,710 --> 00:02:31,440
Example we will see a little later version of view.

27
00:02:31,470 --> 00:02:33,920
Anyway we are going to provide it.

28
00:02:35,320 --> 00:02:42,540
It that generally has had to laugh but here we are going to provide a router link and give some back.

29
00:02:43,020 --> 00:02:49,310
And this part is supposed to be mapped to a particular component in the road table which we are going

30
00:02:49,310 --> 00:02:52,040
to create somewhere later.

31
00:02:52,180 --> 00:02:59,660
Also there is a round table created and registered with the group Mordieu so that roundtable will have

32
00:02:59,870 --> 00:03:07,110
bought my component so when I click on this link this is the display text of that link.

33
00:03:07,280 --> 00:03:13,240
When I click on this link this box corresponding component is going to be loaded.

34
00:03:14,090 --> 00:03:20,410
It's his Steimle template will be executed with the properties being taken from the component and some

35
00:03:20,410 --> 00:03:22,940
internal part is generated.

36
00:03:22,940 --> 00:03:29,540
What point is it is that it's Steimle or displayed for displaying that extremal output within the current

37
00:03:29,540 --> 00:03:30,240
view.

38
00:03:30,290 --> 00:03:34,380
We are supposed to provide a router outlet.

39
00:03:34,610 --> 00:03:40,310
This is the direct deal displays the inner content of the corresponding closed component template in

40
00:03:40,310 --> 00:03:41,500
the current.

41
00:03:42,110 --> 00:03:47,990
So whatever component we have its template is going to be displayed here.

42
00:03:49,440 --> 00:03:53,360
By clicking on it or maybe a button.

43
00:03:53,590 --> 00:03:57,170
Remember both of them also help us.

44
00:03:57,180 --> 00:03:59,240
I wrote the link active.

45
00:03:59,310 --> 00:04:05,940
Basically when I take the cursor over the hyperlink what should be the time of that hyperlink that we

46
00:04:05,940 --> 00:04:07,700
can control over here.

47
00:04:08,280 --> 00:04:13,990
And here you see this or this is how we are going to create old thing more rotes.

48
00:04:14,040 --> 00:04:21,360
So here we are creating loads which is a collection of objects and each object has a part and component

49
00:04:21,540 --> 00:04:22,870
part and component.

50
00:04:23,070 --> 00:04:28,710
So first proteasome up the first component second road map the second component Toyota's my component

51
00:04:29,610 --> 00:04:36,980
and these are the values which we are actually supposed to use as part in ATAC and hotlink this one

52
00:04:37,490 --> 00:04:40,280
should be either post-road second row or terminal.

53
00:04:40,400 --> 00:04:46,550
Based on what we provided the corresponding component dimply output is going to be displayed in the

54
00:04:46,550 --> 00:04:49,650
location where we have roter outlet.

55
00:04:50,030 --> 00:04:57,980
But interestingly this is just an old table created wrote table has to be registered with roter model

56
00:04:58,700 --> 00:05:06,620
and the older model we are going to get is model with providers routing and this routing has to be registered

57
00:05:06,860 --> 00:05:08,190
with the root boarding.

58
00:05:08,690 --> 00:05:15,890
So we have got a proton model for food that contains all the directives given the road and the road

59
00:05:15,890 --> 00:05:17,500
service itself.

60
00:05:18,140 --> 00:05:20,020
But if you don't want the roads with.

61
00:05:20,150 --> 00:05:25,790
And just if you want the directive under given ropes then you are supposed to make use of road or more

62
00:05:26,330 --> 00:05:27,650
for side.

63
00:05:28,190 --> 00:05:32,660
Yes practically we will see where we can make use of for route and fortell later.

64
00:05:33,200 --> 00:05:36,310
But let us first begin with a simple example.

65
00:05:36,380 --> 00:05:39,300
I'm going to create lot of classes here.

66
00:05:39,590 --> 00:05:42,740
So let's say this is department already ideas I'll do copy and paste.

67
00:05:42,770 --> 00:05:50,820
In this particular example some are adding a new type script department.

68
00:05:51,040 --> 00:05:55,530
Has just department department department name.

69
00:05:55,530 --> 00:06:01,390
That's it then employee employee the s.

70
00:06:01,660 --> 00:06:06,530
So I get now typescript employed.

71
00:06:06,550 --> 00:06:09,380
Or does this deal to.

72
00:06:10,000 --> 00:06:11,720
Because you are using department here.

73
00:06:11,750 --> 00:06:14,470
Yes you should import

74
00:06:16,920 --> 00:06:24,360
department from God's last department.

75
00:06:24,410 --> 00:06:28,360
This would give error because we are using department here it right.

76
00:06:29,030 --> 00:06:36,290
Then just for your reference I'll copy and paste it here so that you don't miss it.

77
00:06:36,290 --> 00:06:40,130
Then I want to know the component that is employee component.

78
00:06:40,130 --> 00:06:49,700
I would like to create employee component a new typescript fired employee not competent or B because

79
00:06:49,780 --> 00:06:51,820
it's a pretty straightforward.

80
00:06:51,840 --> 00:07:02,270
I have employed employees made up of all these things so I have a constructor which is creating a list

81
00:07:02,270 --> 00:07:11,730
of employees for granted some salary corresponding Department object is also created this department

82
00:07:11,730 --> 00:07:17,110
is the property Department ID and department mean.

83
00:07:17,720 --> 00:07:27,570
I say also I intend to be one and we do so have two employees left that may be for them or I may create

84
00:07:27,570 --> 00:07:43,670
a few more three for trade for maybe something Department will change one the one and so on.

85
00:07:44,950 --> 00:07:50,050
Every time we are creating a new report which is not a good practice somewhere we should have an area

86
00:07:50,050 --> 00:07:52,990
of department and from that area we should provide the reference.

87
00:07:52,990 --> 00:07:57,570
Actually But I know we can ignore it.

88
00:07:57,570 --> 00:07:58,180
Gomo

89
00:08:01,440 --> 00:08:05,430
so constructed is initialising MP as well.

90
00:08:05,470 --> 00:08:10,340
MP s I would like to use it in employee

91
00:08:13,680 --> 00:08:15,580
don't hate Steimle fine.

92
00:08:15,760 --> 00:08:18,390
So I'm going to have not employed or team and

93
00:08:25,350 --> 00:08:29,130
employed and still my employed got that statement.

94
00:08:29,180 --> 00:08:39,400
We would have simple rendering table where we are and then all the employee that is in the is used.

95
00:08:39,400 --> 00:08:41,460
We are getting one employee at a time.

96
00:08:41,460 --> 00:08:45,000
Name of the employee Department of the employee is what we are in.

97
00:08:45,330 --> 00:08:46,790
Maybe we can render the

98
00:08:55,490 --> 00:08:57,370
employee employee name in place.

99
00:08:57,400 --> 00:08:59,110
And that also we can do

100
00:09:09,290 --> 00:09:16,870
so my employer component is very good and it is also producing some kind of say we don't have a department

101
00:09:16,870 --> 00:09:18,000
component.

102
00:09:20,860 --> 00:09:24,480
So a new that is going to play

103
00:09:27,070 --> 00:09:38,950
Department dot com and dot ideas and here we are getting a list of all departments or department component

104
00:09:47,510 --> 00:09:52,940
that it and then up to a department his team will find

105
00:10:00,950 --> 00:10:01,160
Glenn

106
00:10:06,350 --> 00:10:15,800
Glick heads demon department of its team and I'm going to list all the departments pretty simple so

107
00:10:15,800 --> 00:10:17,300
we have two components in one place.

108
00:10:17,300 --> 00:10:22,100
We are displaying all the employees in the other place we are displaying all the departments we have

109
00:10:22,100 --> 00:10:23,140
department ID

110
00:10:28,070 --> 00:10:31,420
so looks conformant Department Glaus Department ID

111
00:10:34,440 --> 00:10:41,020
I guess Bill Gates idea what little we have so.

112
00:10:43,540 --> 00:10:53,420
Law the basic skeleton is really not what you have to do is be noble Abdur comporting daz and make

113
00:10:53,420 --> 00:10:54,780
some small changes.

114
00:10:55,630 --> 00:11:06,060
Most important things we are trying to inject here roador component so I'm going to add this construct

115
00:11:06,060 --> 00:11:11,290
up to get it over the company and went in.

116
00:11:11,710 --> 00:11:16,890
I've got Cumberland.

117
00:11:17,160 --> 00:11:19,150
I would like to have it Steimle.

118
00:11:19,220 --> 00:11:23,400
My daughter TGM it used as a template.

119
00:11:23,430 --> 00:11:24,150
You are in

120
00:11:33,290 --> 00:11:34,600
book.

121
00:11:38,350 --> 00:11:39,610
Despite abductor's.

122
00:11:39,620 --> 00:11:43,070
You also are up for

123
00:11:48,610 --> 00:11:58,870
abducted stimulus supposed to provide but older so that I have to add on top from that the component

124
00:12:00,230 --> 00:12:08,470
is also here only women pre-work wouldn't matter for manual navigation.

125
00:12:08,490 --> 00:12:09,610
That said there is one matter.

126
00:12:09,610 --> 00:12:11,630
Go home and go home.

127
00:12:11,650 --> 00:12:17,120
It's going to take us to something like IOM you know what is M-B somewhere later.

128
00:12:17,140 --> 00:12:24,310
I'm going to create an old table in the table is going to be created as a pop which will refer to a

129
00:12:24,310 --> 00:12:26,700
corresponding component.

130
00:12:27,040 --> 00:12:30,660
So very what I'm going to call go there.

131
00:12:30,700 --> 00:12:33,970
I want to get the content from employee component.

132
00:12:34,230 --> 00:12:39,680
The template is going to be displayed now.

133
00:12:40,780 --> 00:12:46,920
Next is TGM and this is the most important thing in and I table.

134
00:12:49,910 --> 00:13:00,820
We have got to do some management and hear what is important is the road links which we are doing.

135
00:13:01,210 --> 00:13:04,210
Go home is like employees.

136
00:13:04,300 --> 00:13:08,930
It's linked to EMT but department is linked to department.

137
00:13:08,930 --> 00:13:17,130
But this again is departments who can actually do anything else also.

138
00:13:17,220 --> 00:13:20,960
But this is important when we click this matter is going to execute.

139
00:13:21,380 --> 00:13:25,810
And this is in complete complete is this.

140
00:13:25,850 --> 00:13:31,320
So companies go home is going to execute going will let us navigate to empty.

141
00:13:31,470 --> 00:13:38,800
But where is the output of the component or the Navigant better going to be displayed in this location.

142
00:13:38,820 --> 00:13:45,210
So here we feel we are creating him in the when we click on the menu we're going to get the output displayed

143
00:13:45,240 --> 00:13:47,010
on the same page.

144
00:13:47,340 --> 00:13:50,780
And that is why it is rare for a single page application.

145
00:13:50,820 --> 00:14:00,880
And finally you have to make up being component basically road component is what we have to Agnel.

146
00:14:01,140 --> 00:14:09,490
So go to the project in your script file undercoating Yes and then a lot of initial conditions are required.

147
00:14:09,900 --> 00:14:11,540
Everything at one place.

148
00:14:11,670 --> 00:14:16,980
So this is the most important part of it where we are actually trying to create a baby.

149
00:14:17,260 --> 00:14:26,430
This road maps though bought to the corresponding component as a result is that our first here actually

150
00:14:26,430 --> 00:14:34,140
we are supposed to put CTS and we haven't yet written this component file not for component.

151
00:14:34,350 --> 00:14:35,580
Well let's not worry about that.

152
00:14:35,610 --> 00:14:38,860
I'll come back to this and discuss it like not let us commented.

153
00:14:39,090 --> 00:14:48,590
So I have it rotes Eddie there I have got one another and one more wrote for us.

154
00:14:48,590 --> 00:14:50,100
Understand this too.

155
00:14:50,570 --> 00:14:59,110
We are creating a potholders me when used with road link of employee component will be displayed.

156
00:14:59,300 --> 00:15:01,220
We have a potholders department.

157
00:15:01,450 --> 00:15:07,420
When used to it Roebling output of department component reproducibly.

158
00:15:07,820 --> 00:15:10,480
And of course if you want to control write this also.

159
00:15:10,680 --> 00:15:12,530
Well no not the park matters.

160
00:15:12,560 --> 00:15:15,710
Just double stuff basically means not the part matches

161
00:15:18,500 --> 00:15:24,460
of file not found component can be displayed directly why not let us add this also.

162
00:15:25,140 --> 00:15:32,120
I had a time to find file following component.

163
00:15:33,190 --> 00:15:34,970
Got this.

164
00:15:35,690 --> 00:15:37,430
And here we just want to write

165
00:15:40,880 --> 00:15:42,280
on a date.

166
00:15:44,800 --> 00:15:51,700
Let's say some skeleton code from here and paste here and here I just want to get a template

167
00:15:55,110 --> 00:16:06,540
on some text that said that it was dead file not existing.

168
00:16:07,400 --> 00:16:09,500
And let's start with this.

169
00:16:09,510 --> 00:16:20,100
That it a simple file not gamboling file not file component and does not compute.

170
00:16:20,280 --> 00:16:33,290
No component here is supposed to be imports from daughter lies not fall in.

171
00:16:33,810 --> 00:16:42,320
So using this arose object we are doing get older but older more do a lot for roads and we call our

172
00:16:42,330 --> 00:16:44,080
own property here.

173
00:16:44,470 --> 00:16:47,490
This eroding property is what I would like to use it.

174
00:16:47,520 --> 00:16:52,700
So we are exporting this actually this whole thing is what I'm going to actually use it with.

175
00:16:52,780 --> 00:16:56,200
Indeed more than you know

176
00:17:03,440 --> 00:17:06,260
we have this.

177
00:17:06,380 --> 00:17:08,350
I've got more details on this.

178
00:17:08,360 --> 00:17:09,930
So go look I've got more.

179
00:17:09,980 --> 00:17:10,440
Yes

180
00:17:13,780 --> 00:17:15,170
and you're on top.

181
00:17:15,170 --> 00:17:20,870
We will have to first import roasting from roasting more than we have exploded.

182
00:17:20,990 --> 00:17:32,710
So here we are import roasting from door slots are opening and that roasting is what you're actually

183
00:17:32,710 --> 00:17:38,360
supposed to import at that St..

184
00:17:38,840 --> 00:17:40,270
This is what precisely is done.

185
00:17:40,340 --> 00:17:46,970
We should do this import and this of course we are not making use of multiple components that is employee

186
00:17:46,970 --> 00:17:49,960
component Department component 5.1 component.

187
00:17:50,030 --> 00:17:54,990
All this I'll include here I'll refer to them here.

188
00:17:56,040 --> 00:18:07,390
Employees company and the Barkman company and a lot of phone companies and and that's it.

189
00:18:07,700 --> 00:18:09,410
So again I come back to this.

190
00:18:09,560 --> 00:18:09,930
Right.

191
00:18:09,950 --> 00:18:13,270
Let me first demonstrate these two things please.

192
00:18:13,340 --> 00:18:20,620
So when I run this what are we going to see the content from a component which is content from my app.

193
00:18:21,050 --> 00:18:26,410
It's Jemal which is this navigation button and by default we have the yes.

194
00:18:26,810 --> 00:18:29,000
So I can actually replace this with header.

195
00:18:29,180 --> 00:18:34,320
So we get a rule that we this no.

196
00:18:34,600 --> 00:18:37,520
5 right.

197
00:18:37,530 --> 00:18:43,150
No I'm going to use this to only your MP and department.

198
00:18:43,280 --> 00:18:47,880
So we have go a whole lot to be was no employees.

199
00:18:47,890 --> 00:18:48,680
That is the default.

200
00:18:48,680 --> 00:18:55,700
Actually we got the employee days department I'm getting that department didn't you see what is interesting

201
00:18:55,700 --> 00:19:03,420
is the block is getting things on so that you are here slash Department slash employee that is very

202
00:19:03,420 --> 00:19:04,160
interesting.

203
00:19:05,720 --> 00:19:09,070
Now we have other things also to look at.

204
00:19:09,080 --> 00:19:12,700
One is the goal of we're all doubling.

205
00:19:13,220 --> 00:19:19,520
We are making it all to the metod know that Wantage of this is in the mid term.

206
00:19:20,030 --> 00:19:21,320
Where is this matter.

207
00:19:21,680 --> 00:19:28,520
This matter obviously is in the iPod you know noting mode

208
00:19:35,870 --> 00:19:36,720
setting.

209
00:19:37,120 --> 00:19:41,290
This is the template associated with app component.

210
00:19:41,410 --> 00:19:47,050
So it should be a component of component us go through that program.

211
00:19:47,190 --> 00:19:50,160
We are navigating to employ me.

212
00:19:50,290 --> 00:19:57,040
That means if I look on department I'm going to get employee on to this and this are saying in one case

213
00:19:57,040 --> 00:19:59,750
we are used we have used link

214
00:20:04,260 --> 00:20:05,500
low

215
00:20:08,180 --> 00:20:09,680
routing roping Roding

216
00:20:12,660 --> 00:20:15,480
in one case we have those link.

217
00:20:16,000 --> 00:20:25,800
We and in the other case we have used the method so that programmatically navigating here that the only

218
00:20:25,800 --> 00:20:31,710
difference in this DVD and if I don't know it will take me to the department

219
00:20:34,630 --> 00:20:36,040
UKIP Department

220
00:20:38,590 --> 00:20:44,240
this and these are seem no more difference just two ways of doing the same thing.

221
00:20:44,390 --> 00:20:46,710
Nobody can go home.

222
00:20:46,750 --> 00:20:49,890
I would like to have something like one component.

223
00:20:50,230 --> 00:20:53,150
So let's say I know a new component.

224
00:20:55,000 --> 00:21:00,030
On daughter component God.

225
00:21:00,970 --> 00:21:04,500
And the my component.

226
00:21:04,620 --> 00:21:09,740
I'll take the skeleton instead and I'll just say complete

227
00:21:12,940 --> 00:21:13,800
some text.

228
00:21:13,800 --> 00:21:17,820
This is my own page and this.

229
00:21:17,820 --> 00:21:20,260
I'm calling it that all gone.

230
00:21:20,520 --> 00:21:27,590
I don't want to go back I don't want anything that's gone on so long if I want to navigate this.

231
00:21:27,600 --> 00:21:29,080
Can I right here.

232
00:21:30,820 --> 00:21:40,060
Home component actually like an pro-white in Newport.

233
00:21:40,170 --> 00:21:42,150
Of course it does.

234
00:21:42,480 --> 00:21:44,110
And this is home gamboling.

235
00:21:46,640 --> 00:21:56,400
So either home or empty if I want to use same thing for both of them here that I do I'll give home.

236
00:21:56,900 --> 00:22:00,420
That's very important when pot is empty.

237
00:22:00,780 --> 00:22:02,880
Then read at.

238
00:22:03,280 --> 00:22:04,720
And obviously here.

239
00:22:06,290 --> 00:22:06,540
All

240
00:22:13,830 --> 00:22:15,460
very very very important.

241
00:22:15,780 --> 00:22:24,770
Whenever we want to navigate to one of the existing BOP we simply use that to do rather than specifying

242
00:22:24,770 --> 00:22:26,390
the component.

243
00:22:26,750 --> 00:22:33,310
But what is important is you're if you're giving empty compulsory you should give part of my work to

244
00:22:33,350 --> 00:22:34,560
food.

245
00:22:34,670 --> 00:22:37,390
Otherwise the ultimate value is partial.

246
00:22:37,670 --> 00:22:42,690
So even partially if it is matching that needs it will every word magic to everybody.

247
00:22:42,750 --> 00:22:48,680
The get go home only which is not a requirement only when the parties empty beer is empty.

248
00:22:48,680 --> 00:22:52,790
In my app I've given here empty Roebling is empty.

249
00:22:53,420 --> 00:22:54,920
So now let us run this.

250
00:22:54,920 --> 00:22:56,540
Actually we can give space also

251
00:22:59,300 --> 00:23:00,180
no effect on

252
00:23:03,240 --> 00:23:08,180
it takes me to something went wrong.

253
00:23:08,840 --> 00:23:11,630
Yeah homegrown and I will not do more Bewdley

254
00:23:20,320 --> 00:23:21,430
or component

255
00:23:24,030 --> 00:23:25,090
of.

256
00:23:25,430 --> 00:23:28,590
All Gauntlett

257
00:23:36,520 --> 00:23:37,460
go home.

258
00:23:37,870 --> 00:23:44,270
This is the home page employs department I'll can go home again because this is my

259
00:23:47,530 --> 00:23:55,730
and of course go home is my work as you can see here go home is actually my darling Emily can within

260
00:23:55,770 --> 00:24:00,310
the home yes even that the same thing no difference whether it's home or

261
00:24:05,190 --> 00:24:07,050
same difference.

262
00:24:07,360 --> 00:24:15,470
So actually just for making it complete this also college does home

263
00:24:18,730 --> 00:24:27,760
and yet go home after reading my component and then this book on that looks completed.

264
00:24:28,630 --> 00:24:29,980
So there those

265
00:24:33,260 --> 00:24:36,980
go home to all employees.

266
00:24:40,180 --> 00:24:43,050
So all these are bought you have to do for preventing it.

267
00:24:43,110 --> 00:24:48,040
And then let me summarize what all you have done extra for a roundtable.

268
00:24:48,160 --> 00:24:56,230
First we have to create a Steimle file in which either ATAC or button tag is supposed to have a road

269
00:24:56,350 --> 00:24:57,290
link.

270
00:24:57,790 --> 00:25:02,920
If you don't want to have road link you should have it function and in the function you have to take

271
00:25:02,920 --> 00:25:07,180
care of using the router got navigate in all the cases.

272
00:25:07,190 --> 00:25:09,790
But we are supposed to provide is the pop.

273
00:25:09,850 --> 00:25:11,170
These are all about.

274
00:25:11,350 --> 00:25:17,980
But these pots are supposed to be mapped to the corresponding component whose old pope is going to be

275
00:25:17,980 --> 00:25:20,800
displayed in this location.

276
00:25:20,900 --> 00:25:27,840
Now where is this component and pop mapping done for that we are a separate voting file.

277
00:25:28,390 --> 00:25:39,060
And here we are doing the mapping.

278
00:25:39,150 --> 00:25:41,060
I do not yet demonstrate start stop.

279
00:25:41,070 --> 00:25:42,700
Let me do that also.

280
00:25:42,720 --> 00:25:44,360
So let's say No I go here.

281
00:25:46,050 --> 00:25:53,130
And something tunc and we know that the junk is not there.

282
00:25:58,450 --> 00:26:01,770
No failing that not.

283
00:26:05,220 --> 00:26:07,300
Because that file is not existing.

284
00:26:09,900 --> 00:26:17,500
So if you are is not matching automatically it will take me to start stop back and start start is mapped

285
00:26:17,490 --> 00:26:24,660
to what starstuff is but to follow up on it which is one component only and you can display more complicated

286
00:26:24,660 --> 00:26:26,700
content in case you want them.

287
00:26:27,210 --> 00:26:34,980
And most important using this roundtable which we created here we have to use a role model for road

288
00:26:35,520 --> 00:26:41,520
and export or routing object model model with providers.

289
00:26:41,550 --> 00:26:49,800
Once we have this routing in model as we are going to do import and mentioned the same in import section

290
00:26:49,800 --> 00:26:50,370
here.

291
00:26:51,420 --> 00:26:54,840
That means we are doing it angular more import.

292
00:26:55,350 --> 00:27:00,330
This is typescript import but this is angular import.

293
00:27:00,660 --> 00:27:05,040
And at the same time all the components which we are using in our project we have to mention it all

294
00:27:05,050 --> 00:27:15,960
here and that's it that's all about routing more about the routing table keeps increasing more Bhatt's

295
00:27:15,960 --> 00:27:26,090
can be defined here and vary what we want to take of this linking in the next chapter I'm going to show

296
00:27:26,360 --> 00:27:31,010
how we can possibly top from one view to another view through barometers.

297
00:27:31,130 --> 00:27:32,570
But for now that's all.

298
00:27:32,600 --> 00:27:33,190
Thank you.

